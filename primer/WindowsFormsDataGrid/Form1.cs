﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace WindowsFormsDataGrid
{
    public partial class Form1 : Form
    {
        // List<DataItem> bList bList;
        public Form1()
        {
            
            InitializeComponent();
        }

        private void toolStripButtonLoad_Click(object sender, EventArgs e)
        {
            this.bindingSourceDataList.DataSource = this.Load();
        }
        private List<DataItem> Load()
        {
            List<DataItem> bList = new List<DataItem>();
            //DataItem d = DataItem.Create(1, new DateTime(DateTime.Now.Ticks), "Name1", false);
            bList.Add(DataItem.Create(1, new DateTime(DateTime.Now.Ticks), "Name1", false));
            bList.Add(DataItem.Create(2, DateTime.Now, "Name2", false));
            bList.Add(DataItem.Create(3, DateTime.Now, "Name3", false));
            bList.Add(DataItem.Create(4, DateTime.Now, "Name4", false));
            bList.Add(DataItem.Create(5, DateTime.Now, "Name5", false));
            bList.Add(DataItem.Create(6, DateTime.Now, "Name6", false));
            bList.Add(DataItem.Create(7, DateTime.Now, "Name7", false));
            bList.Add(DataItem.Create(8, DateTime.Now, "Name8", false));
            bList.Add(DataItem.Create(9, DateTime.Now, "Name9", false));
            bList.Add(DataItem.Create(10, DateTime.Now, "Name10", false));
            bList.Add(DataItem.Create(11, DateTime.Now, "Name11", false));
            return bList;
        }

        
        
    }
}
