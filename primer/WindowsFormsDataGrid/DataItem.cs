﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Text;

namespace WindowsFormsDataGrid
{
    public class DataItem:INotifyPropertyChanged
    {
        private int id;
        //Если не присваивать дефолтный параметор то бедет показвать 01.01.0001 
        private DateTime date=DateTime.Now;
        private string name;
        private bool isColor;

        [DisplayName("ID")]
        public int Id
        {
            get
            {
                return this.id;
            }
            set
            {
                this.id = value;
                this.OnPropertyChanged("Id");
            }
        }
        [DisplayName("Дата")]
        public DateTime Date
        {
            get
            {
                return this.date;
            }
            set
            {
                this.date= value;
                this.OnPropertyChanged("Date");
            }
        }
        [DisplayName("Название")]
        public string Name
        {
            get
            {
                return this.name;
            }
            set
            {
                this.name= value;
                this.OnPropertyChanged("Name");
            }
        }
        [DisplayName("Изменить цвет")]
        public bool IsColor
        {
            get
            {
                return this.isColor;
            }
            set
            {
                this.isColor = value;
                this.OnPropertyChanged("IsColor");
            }
        }
        public static DataItem Create(int id, DateTime date, string name, bool isColor)
        {
            DataItem di = new DataItem();
            di.id = id;
            di.date = date;
            di.name = name;
            di.isColor = isColor;
            return di;
        }

        #region Члены INotifyPropertyChanged

        public event PropertyChangedEventHandler PropertyChanged;
        private void OnPropertyChanged(String propertyName)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
            }
        }

        #endregion
    }
}
