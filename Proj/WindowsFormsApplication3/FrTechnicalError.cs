﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace PortalRosreestr
{
    public partial class FrTechnicalError : Form
    {
        public FrTechnicalError()
        {
            InitializeComponent();
            this.txt_kadastr.Text = "";
            this.txt_docs.Text = "";
            this.txt_cadNumber.Text = "";
        }

        private void textBox2_TextChanged(object sender, EventArgs e)
        {

        }

        private void txt_kadastr_TextChanged(object sender, EventArgs e)
        {

        }

        private void radioButton4_CheckedChanged(object sender, EventArgs e)
        {

        }

        private void FrTechnicalError_Shown(object sender, EventArgs e)
        {
            this.txt_kadastr.Text = "";
            this.txt_docs.Text = "";
            this.txt_cadNumber.Text = "";
        }

        private void FrTechnicalError_Load(object sender, EventArgs e)
        {
            try
            {
                
                var itemsRe = FrPortalRosreestr.portalRosreestr.ReadXsd(Application.StartupPath + @"\\DATA\\dtypeTypeError.xsd");
                this.cbb_type_error.Sorted = false;
                this.cbb_type_error.DataSource = itemsRe;
                this.cbb_type_error.ValueMember = "Key";
                this.cbb_type_error.DisplayMember = "Value";                
            }
            catch (Exception ex)
            {
                ThreadExceptionHandler.ShowEventDialog("", ex);
            }
        }


        private void ok_Click(object sender, EventArgs e)
        {

        }
    }
}
