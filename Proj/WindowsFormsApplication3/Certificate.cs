﻿using Ionic.Zip;
using SQLiteHelperTestApp;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

using System.Security.Cryptography.Pkcs;

using System.Text;
using System.Windows.Forms;
using System.Security.Cryptography.X509Certificates;
// Дополнительные пространства имен
using System.Security.Cryptography;
using CAPICOM;
using System.Collections;
using System.Runtime.InteropServices;

namespace PortalRosreestr
{
    class Certificate
    {
        

        public static X509Certificate2 cert2 = null;
        public static ICertificate cert2I = null;
        public static ICertificates2 cel = null;
        public static string SubjName;


        /// <summary>
        /// Показ списка сертификатов
        /// </summary>
        /// <returns></returns>
        public static X509Certificate2 GetCertX509()
        {
            try
            {
 /*               X509Store store = new X509Store("My", StoreLocation.CurrentUser);
                store.Open(OpenFlags.ReadOnly | OpenFlags.OpenExistingOnly);
                X509Certificate2Collection collection = (X509Certificate2Collection)store.Certificates;
                X509Certificate2Collection fcollection = (X509Certificate2Collection)collection.Find(X509FindType.FindByTimeValid, DateTime.Now, false);


                X509Certificate2Collection scollection = X509Certificate2UI.SelectFromCollection(fcollection, "Список сертификатов!", "Выберите сертификат кадастрового инженера!", X509SelectionFlag.SingleSelection);

                if (scollection.Count > 0) return scollection[0];
                else return null;

   */
                X509Store store = new X509Store("My", StoreLocation.CurrentUser);
                store.Open(OpenFlags.ReadOnly | OpenFlags.OpenExistingOnly);
                X509Certificate2Collection collection = (X509Certificate2Collection)store.Certificates;
                X509Certificate2Collection fcollection = (X509Certificate2Collection)collection.Find(X509FindType.FindBySerialNumber, cert2I.SerialNumber, false);

                //X509Certificate2Collection scollection = X509Certificate2UI.SelectFromCollection(fcollection, "Список сертификатов!", "Выберите сертификат кадастрового инженера!", X509SelectionFlag.SingleSelection);
                if (fcollection.Count > 0) return fcollection[0];
                else return null;
                
               /* IStore store = new CAPICOM.Store();
                store.Open(CAPICOM.CAPICOM_STORE_LOCATION.CAPICOM_CURRENT_USER_STORE, "MY", CAPICOM.CAPICOM_STORE_OPEN_MODE.CAPICOM_STORE_OPEN_READ_ONLY | CAPICOM.CAPICOM_STORE_OPEN_MODE.CAPICOM_STORE_OPEN_EXISTING_ONLY);
                ICertificates2 certificates = (ICertificates2)store.Certificates;

                certificates = certificates.Find(CAPICOM_CERTIFICATE_FIND_TYPE.CAPICOM_CERTIFICATE_FIND_KEY_USAGE, CAPICOM_KEY_USAGE.CAPICOM_DIGITAL_SIGNATURE_KEY_USAGE, true);
                if (certificates.Count > 0)
                {
                    certificates = certificates.Select();
                };
                if (certificates.Count > 0)
                {
                    return certificates[1] as ICertificate;
                }
                else return null;
                */
               
            }
            catch
            {
                return null;
            }
        }

        public static ICertificate GetCert()
        {
            try
            {
                /*  X509Store store = new X509Store("My", StoreLocation.CurrentUser);
                  store.Open(OpenFlags.ReadOnly | OpenFlags.OpenExistingOnly);
                  X509Certificate2Collection collection = (X509Certificate2Collection)store.Certificates;
                  X509Certificate2Collection fcollection = (X509Certificate2Collection)collection.Find(X509FindType.FindByTimeValid, DateTime.Now, false);


                  X509Certificate2Collection scollection = X509Certificate2UI.SelectFromCollection(fcollection, "Список сертификатов!", "Выберите сертификат кадастрового инженера!", X509SelectionFlag.SingleSelection);

                  if (scollection.Count > 0) return scollection[0];
                  else return null;
                 */
                IStore store = new CAPICOM.Store();
                store.Open(CAPICOM.CAPICOM_STORE_LOCATION.CAPICOM_CURRENT_USER_STORE, "MY", CAPICOM.CAPICOM_STORE_OPEN_MODE.CAPICOM_STORE_OPEN_READ_ONLY | CAPICOM.CAPICOM_STORE_OPEN_MODE.CAPICOM_STORE_OPEN_EXISTING_ONLY);
                ICertificates2 certificates = (ICertificates2)store.Certificates;

                certificates = certificates.Find(CAPICOM_CERTIFICATE_FIND_TYPE.CAPICOM_CERTIFICATE_FIND_KEY_USAGE, CAPICOM_KEY_USAGE.CAPICOM_DIGITAL_SIGNATURE_KEY_USAGE, true);
                if (certificates.Count > 0)
                {
                    certificates = certificates.Select();
                    //certificates.Select();
                 
                };
                if (certificates.Count > 0)
                {
                    cert2I = certificates[1] as ICertificate;
                    cel = certificates;
                    Certificate.cert2 =  GetCertX509();
                    SubjName = cert2.SerialNumber;
                    return certificates[1] as ICertificate;
                 
                    
                }
                else return null;


            }
            catch
            {
                return null;
            }
        }

        public static string SignFromText(byte[] plaintext, bool bDetached, Encoding encodingType) 
        {
            CAPICOM.SignedData signedData = 
                new CAPICOM.SignedDataClass();
            CAPICOM.Utilities u = new CAPICOM.UtilitiesClass();
            signedData.Content = u.ByteArrayToBinaryString(plaintext);
            CAPICOM.Signer signer = new CAPICOM.Signer();
            CAPICOM.Certificate ClientCert = new CAPICOM.Certificate();
            try
            {
                CAPICOM.Store MyStore = new CAPICOM.StoreClass();
                MyStore.Open(CAPICOM.CAPICOM_STORE_LOCATION.CAPICOM_CURRENT_USER_STORE,"My",CAPICOM.CAPICOM_STORE_OPEN_MODE.CAPICOM_STORE_OPEN_READ_ONLY);
                foreach(CAPICOM.Certificate c in ((CAPICOM.ICertificates2)MyStore.Certificates))
                {
                    if(c.SerialNumber== cert2.SerialNumber)
                        ClientCert = c;
                }
            }
            catch(Exception ex)
            {
                MessageBox.Show("Ошибка : "+ex.Message );
            }

            signer.Certificate = ClientCert;
            string _signedContent = signedData.Sign(signer, bDetached, 
                CAPICOM.CAPICOM_ENCODING_TYPE.CAPICOM_ENCODE_BASE64);
            return _signedContent; 
        }
      
        /// <summary>
        /// Подписание содержимого указанным сертификатом
        /// </summary>
        /// <param name="certificate">Сертификат, с которым связан ключ для подписания.</param>
        /// <param name="data">Подписываемое содержимое.</param>
        /// <param name="detached">Признак отсоединённой подписи.</param>
        /// <returns>Подпись.</returns>
        /// 
        public static byte[] HashAndSignBytes(Stream DataStream, RSAParameters Key)
        {
            try
            {
                // Reset the current position in the stream to 
                // the beginning of the stream (0). RSACryptoServiceProvider
                // can't verify the data unless the the stream position
                // is set to the starting position of the data.
                DataStream.Position = 0;

                // Create a new instance of RSACryptoServiceProvider using the 
                // key from RSAParameters.  
                RSACryptoServiceProvider RSAalg = new RSACryptoServiceProvider();

                RSAalg.ImportParameters(Key);

                // Hash and sign the data. Pass a new instance of SHA1CryptoServiceProvider
                // to specify the use of SHA1 for hashing.
                return RSAalg.SignData(DataStream, new SHA1CryptoServiceProvider());
            }
            catch (CryptographicException e)
            {
                Console.WriteLine(e.Message);

                return null;
            }
        }

       // [DllImport("Crypt32.dll", EntryPoint = "CryptSignMessage")]
        public static byte[] StreamToByteArray(string fileName)
        {
            byte[] total_stream = new byte[0];
            using (Stream input = File.Open(fileName, FileMode.Open, FileAccess.Read))
            {
                byte[] stream_array = new byte[0];
                // Setup whatever read size you want (small here for testing)
                byte[] buffer = new byte[32];// * 1024];
                int read = 0;

                while ((read = input.Read(buffer, 0, buffer.Length)) > 0)
                {
                    stream_array = new byte[total_stream.Length + read];
                    total_stream.CopyTo(stream_array, 0);
                    Array.Copy(buffer, 0, stream_array, total_stream.Length, read);
                    total_stream = stream_array;
                }
            }
            return total_stream;
        }
        private static  bool CryptSignMessage(string path )
        {

            
            byte[] msgBytes;
            SignedCms signedCms;
            CmsSigner cmsSigner;
            byte[] encodedMsg;
            SignerClass signer;
            ICertificate2 c;
            try
            {

                X509Store xs = new X509Store(StoreName.My, StoreLocation.CurrentUser);
                xs.Open(OpenFlags.ReadOnly);

                RSACryptoServiceProvider rsa = new RSACryptoServiceProvider();
                X509Certificate2Collection cers = xs.Certificates.Find(X509FindType.FindBySerialNumber, SubjName, false);
                X509Certificate2 cer = cers[0];


                msgBytes = System.IO.File.ReadAllBytes(path);

                ThreadExceptionHandler.logger.Info("content " + path);
                ContentInfo contentInfo = new ContentInfo(msgBytes);
                ThreadExceptionHandler.logger.Info("signedCms " + msgBytes.ToString());

                signedCms = new SignedCms(contentInfo, true);

                ThreadExceptionHandler.logger.Info("CmsSigner ");
                //var cmsSigner = new CmsSigner(cert2);
                cmsSigner = new CmsSigner(SubjectIdentifierType.IssuerAndSerialNumber, cer);


                if (cer == null)
                    throw new Exception("Certificate not found.");
                ThreadExceptionHandler.logger.Info("ComputeSignature " + cer.SerialNumber);
                try
                {
                    
                }
                catch (Exception e)
                {
                    MessageBox.Show("Ошибка при подписывании пакета " + e.ToString() + " " + e.Message);
                    ThreadExceptionHandler.logger.Info("Error " + e.Message);
                }
                ThreadExceptionHandler.logger.Info("encodedMsg ");
                encodedMsg = signedCms.Encode();
                ThreadExceptionHandler.logger.Info("WriteAllBytes " + path);
                System.IO.File.WriteAllBytes(path + ".sig", encodedMsg);


            }
            catch (Exception e)
            {
                ThreadExceptionHandler.logger.Info("Error " + e.Message);
            }
            finally
            {
                msgBytes = null;
                signedCms = null;
                cmsSigner = null;
                encodedMsg = null;
                signer = null;
            }
            return true;
        }
        

        public static void Sign(string path)        
        {
            
            byte[] msgBytes;
            SignedCms signedCms;
            CmsSigner cmsSigner;
            byte[] encodedMsg;
            SignerClass signer;
            ICertificate2 c;
            try
            {

               /*
                var signeddata = new CAPICOM.SignedData();
              //  FileStream file = File.Open(tbSourceFile.Text, FileMode.Open);
                byte[] Content = System.IO.File.ReadAllBytes(path);

                //file.Read(Content, 0, (int)file.Length);
                //file.Close();

                StringWriter sw = new StringWriter();
                sw.Write(Content);

                signeddata.Content = sw.ToString();


                IStore store = new CAPICOM.Store();
                store.Open(CAPICOM.CAPICOM_STORE_LOCATION.CAPICOM_CURRENT_USER_STORE, "MY", CAPICOM.CAPICOM_STORE_OPEN_MODE.CAPICOM_STORE_OPEN_READ_ONLY | CAPICOM.CAPICOM_STORE_OPEN_MODE.CAPICOM_STORE_OPEN_EXISTING_ONLY);
                ICertificates2 certificates = (ICertificates2)store.Certificates;

                bool ok = false;
                signer = new SignerClass();
                ICertificates2 certificates1 = (ICertificates2)store.Certificates;
                IEnumerator certs = certificates.GetEnumerator();
                while (certs.MoveNext())
                {
                    c = (ICertificate2)certs.Current;
                    if (c.SerialNumber.Trim().ToUpper() == SubjName.Trim().ToUpper()) //&&
                    //c.Thumbprint == "870279C8C40F6D4A68F48EA83CF8F1A83C0C4654" &&
                    //c.SubjectName == "CN=Joe Smith")
                    {
                        signer.Certificate = c;
                        ok = true;
                        break;
                    }
                }
                
               // certificates = certificates.Find(CAPICOM_CERTIFICATE_FIND_TYPE.CAPICOM_CERTIFICATE_FIND_SHA1_HASH,SubjName, true);

                //if (certificates.Count > 0)
                //{
                  //  certificates =  certificates.Find(CAPICOM_CERTIFICATE_FIND_TYPE.CAPICOM_CERTIFICATE_FIND_SHA1_HASH,SubjName, false);
                //}
               // if (certificates.Count > 0)
               // {
                  //  ISigner2 signers = new CAPICOM.Signer();
               //     signers.Certificate = certificates[1]  as ICertificate;
                if (ok)
                {
                    string res = signeddata.Sign(signer, true, CAPICOM_ENCODING_TYPE.CAPICOM_ENCODE_BASE64);
                    // save the signature into a detached signature file                    
                    byte[] bytes = new byte[res.Length * sizeof(char)];
                    System.Buffer.BlockCopy(res.ToCharArray(), 0, bytes, 0, bytes.Length);
                    ThreadExceptionHandler.logger.Info("Подпись файла   " + SubjName.ToString()+" "+path);
                    System.IO.File.WriteAllBytes(path + ".sig", bytes);
                }
                else
                {
                    ThreadExceptionHandler.logger.Error("Подпись файла не прошла  " + SubjName);
                }
                */
               // }

                /*if (cert2I != null)
                { 
                    ISigner2 signers = new CAPICOM.Signer();
                    signers.Certificate = cert2I; 
                    
                    string res = signeddata.Sign(signers, false, CAPICOM_ENCODING_TYPE.CAPICOM_ENCODE_BINARY);
                    byte[] bytes = new byte[res.Length * sizeof(char)];
                    System.Buffer.BlockCopy(res.ToCharArray(), 0, bytes, 0, bytes.Length);
                    System.IO.File.WriteAllBytes(path + ".sig", bytes);
                    }
                */

              /* ThreadExceptionHandler.logger.Info("read  " + path);
                msgBytes = System.IO.File.ReadAllBytes(path);
                //byte[] asciiBytes = System.IO.File.ReadAllBytes(path);
                byte[] dataBytes = msgBytes;

                // Create a buffer for the memory stream.
                byte[] buffer = new byte[dataBytes.Length];

                // Create a MemoryStream.
                MemoryStream mStream = new MemoryStream(buffer);

                // Write the bytes to the stream and flush it.
                mStream.Write(dataBytes, 0, dataBytes.Length);

                mStream.Flush();

                RSACryptoServiceProvider rsa;
                if (cert2.HasPrivateKey)
                    rsa = (RSACryptoServiceProvider)cert2.PrivateKey ;
                else
                    rsa = (RSACryptoServiceProvider)cert2.PublicKey.Key;
              //  RSACryptoServiceProvider rsaCert =  cert2.PublicKey.Key as RSACryptoServiceProvider;// GetCertificateWithPrivateKeyFromSomewhere(); // this method is yours
                RSAParameters Key = rsa.ExportParameters(true);
                byte[] signedData = HashAndSignBytes(mStream, Key);

               // byte[] res = rsaCert.SignData(msgBytes, CryptoConfig.MapNameToOID("SHA512"));//rsaCert.SignData SignData(msgBytes, new SHA1CryptoServiceProvider());
                System.IO.File.WriteAllBytes(path + ".sig", signedData);
              //  string SignedMsg = SignFromText(asciiBytes, false, Encoding.ASCII);*/



                X509Store xs = new X509Store(StoreName.My, StoreLocation.CurrentUser);
                xs.Open(OpenFlags.ReadOnly);

                RSACryptoServiceProvider rsa = new RSACryptoServiceProvider();
                X509Certificate2Collection cers = xs.Certificates.Find(X509FindType.FindBySerialNumber, SubjName, false);
                X509Certificate2 cer = cers[0];

                
                msgBytes = System.IO.File.ReadAllBytes(path);
                
                ThreadExceptionHandler.logger.Info("content " + path);
                ContentInfo contentInfo = new ContentInfo(msgBytes);
                ThreadExceptionHandler.logger.Info("signedCms " + msgBytes.ToString());
                
                signedCms = new SignedCms(contentInfo, true);

                ThreadExceptionHandler.logger.Info("CmsSigner ");
                //var cmsSigner = new CmsSigner(cert2);
                cmsSigner = new CmsSigner(SubjectIdentifierType.IssuerAndSerialNumber, cer);

               
                if (cer == null)
                    throw new Exception("Certificate not found.");
                ThreadExceptionHandler.logger.Info("ComputeSignature " + cer.SerialNumber);
                try
                {
                    signedCms.ComputeSignature(cmsSigner, true);
                }
                catch (Exception e)
                {
                    MessageBox.Show("Ошибка при подписывании пакета " + e.ToString() + " " + e.Message);
                    ThreadExceptionHandler.logger.Info("Error " + e.Message);
                }
                ThreadExceptionHandler.logger.Info("encodedMsg ");
                encodedMsg = signedCms.Encode();
                ThreadExceptionHandler.logger.Info("WriteAllBytes " + path);
                System.IO.File.WriteAllBytes(path + ".sig", encodedMsg);
              
              
            }
            catch (Exception e)
            {
                ThreadExceptionHandler.logger.Info("Error " + e.Message);
            }
            finally
            {
                msgBytes = null; 
                signedCms = null;
                cmsSigner = null;
                encodedMsg = null;
                signer = null;
            }
        }

        /// <summary>
        ///  Подпись полученного заархивированного пакета REQ
        /// </summary>
        /// <param name="path">Путь к архиву REQ.zip</param>
        public static void SignGUOKS(string path)
        {
            try
            {
                string[] files = Directory.GetFiles(path, "*", SearchOption.AllDirectories).Where(s => (Path.GetExtension(s) == ".zip")).ToArray();
                if (files.Length == 0) return;
                ZipFile zip = new ZipFile(files[0]);
              //  zip.AlternateEncodingUsage = ZipOption.Always;
              //  zip.AlternateEncoding = Encoding.GetEncoding((866));
                string pathExt = path + Path.GetFileNameWithoutExtension(files[0]);
                zip.ExtractAll(pathExt);
                zip.Dispose();
                File.Delete(files[0]);
                Signs(pathExt);
                zip = new ZipFile(files[0]);
              //  zip.AlternateEncodingUsage = ZipOption.Always;
               // zip.AlternateEncoding = Encoding.GetEncoding((866));
                zip.AddDirectory(pathExt);
                zip.Save();
                zip.Dispose();
                Directory.Delete(pathExt, true);
            }
            catch (Exception ex)
            {
                MessageBox.Show("Ошибка при подписывании пакета req " + ex.ToString() + " " + ex.Message);
            }
        }

        /// <summary>
        /// Подпись группы файлов
        /// </summary>
        /// <param name="path">Путь к директории файлов</param>
        public static void Signs(string path)
        {
            ThreadExceptionHandler.logger.Info("подпись фалов");
            try
            {
                if ((path == "") || (path == null)) return;
                foreach (var item in Directory.GetFiles(path, "*", SearchOption.AllDirectories).Where(s =>
                    ((Path.GetExtension(s) != ".zip") ||
                     ((Path.GetExtension(s) == ".zip") &&(Path.GetFileNameWithoutExtension(s).ToLower().IndexOf("SchemaParcels".ToLower()) != -1)))
                    && (Path.GetExtension(s) != ".sig")
                    && (Path.GetDirectoryName(s).IndexOf("dov") == -1)).ToArray())
                {
                    // if (Path.GetDirectoryName(item).IndexOf("dov")== -1)S
                    ThreadExceptionHandler.logger.Info("подпись фалов1 " + item);
                    Sign(item);
                    ThreadExceptionHandler.logger.Info("подпись фалов2 " + item);
                }
            }
            catch (Exception e)
            {
                ThreadExceptionHandler.logger.Error(e.Message);
            }
        }


    }

    
       
}
