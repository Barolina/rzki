﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace PortalRosreestr
{
    class Consts
    {
        public const string xsltConversionDopReq = "DOP_ST_REQ.xsl";
        public const string xsltConversionDopReqPDF = "DOP_PDF.xsl";
        public const string xsltGeneral = "STD_to_REQ.xsl";
        public const string xsltFixError = "STD_to_REQ_FIX_Error.xsl";
        public const string xsltTechnicalError = "TechicalError.xsl";

        public const string xsltKPT = "KPT.xsl";
        public const string xsltTempXML = "TempXML.xml";

        public const string CODE_STD_TP = "558103010000";
        public const string CODE_FIX_ERROR = "000000000001";
        public const string CODE_STD_MP = "558101020000";
        public const string CODE_DOP = "558101100000";
        public const string CODE_DOP_PDF = "558502020000";
        public const string CODE_TECHNICAL_ERROR = "558101090100";

        public const string DIRECT_INFO = "\\Data\\";

        public const string XML_ELM_CODE = "code";
        public const string XML_ELM_DATE = "date";
        public const string XML_ELM_MESSAGE = "message";

        public const string SignsPack   = "Подождите ... идет подпись пакета...";
        public const string ErrorSignPack = "Произошла ошибка при подписывании пакета";
        public const string MSGWAITOUT = "Подождите ... идет отправка...";
        public const string MSGOUT = "Отправить";
        public const string GUOKS = "GKUOKS";
        public const string GKUZU = "GKUZU";
        public const string PDF = ".PDF";

        public const string VALIDATEPACK = "Проверка пакета!";
        public const string ERRORCONNECTPORTAL = "Сброс соединения с порталом росреестра ";
        public const string ERRORINSERTBD = "Данные пакет был отправлен на портал - но в базу не добавлен! ";

        public const string XSDdRequest_Type = @"\\DATA\\dRequest_Type_.xsd";
        public const string XSDdRequest_Update = @"\\DATA\\dRequest_Update_.xsd";
        public const string XSDADRESCODE = @"\\DATA\\adresCod.xsd";

        /// <summary>
        ///   Параметры xslt
        /// </summary>
        public const string  xslSerial = "Serial";
        public const string  xslNumber = "Number";
        public const string xslvTypeUpdate = "vTypeUpdate";

        public const string xslDateDoc = "DateDoc";
        public const string  xslIssuqe = "Issuqe";
        public const string  xslDocRegion = "DocRegion";
        public const string  xslDocNote = "DocNote";
        public const string  xslSnils = "Snils";
        public const string  xslSurname = "Surname";
        public const string  xslFirst = "First";
        public const string  xslPatronumic = "Patronumic";
        public const string xslRegion_doc = "Region_doc";
        public const string  xslNameDoc = "NameDoc";
        public const string  xslEmail = "Email";

        public const string xslCountFilePDf = "CountFilePDF"; //в xslt генерируются автоматически
        public const string xslNumbersPDF = "NumnbersPDF";// список  номероа для  каждого документа через ;
        public const string xslDatesPDF = "DatesPDF";//список дат для каждой из pdf через ;

        public const string xslCurr_Date = "Curr_Date";
        public const string xslCurr_Time = "Curr_Time";
        public const string xslFIO_Cadastr = "FIO_Cadastr";

        public const string xslRequestNumber = "RequestNumber";

        public const string xslPDFNumber = "PdfNumber";
        public const string xslPDFDate = "PDFDate";
        public const string xslPDFIssueQue = "PDFIssueQue";
        public const string xslPDFIS = "PDFIS";
        public const string xslPDFName = "PDFName";

        // на случай нехнической ошибки
        public const string xslTechCadNum = "techCadnum";
        public const string xslTechTypeParcel = "Parcel";
        public const string xslTechinGKn = "inGkn";
        public const string xslTechindocs = "indocs";
        public const string xslTechinType = "typs";
    }
}
