﻿namespace PortalRosreestr
{
    partial class FrTechnicalError
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.txt_docs = new System.Windows.Forms.TextBox();
            this.txt_kadastr = new System.Windows.Forms.TextBox();
            this.label12 = new System.Windows.Forms.Label();
            this.lbl_docs = new System.Windows.Forms.Label();
            this.lbl_kad = new System.Windows.Forms.Label();
            this.txt_cadNumber = new System.Windows.Forms.TextBox();
            this.rbCadNum = new System.Windows.Forms.RadioButton();
            this.rbBuilding = new System.Windows.Forms.RadioButton();
            this.rbStroy = new System.Windows.Forms.RadioButton();
            this.rbPomech = new System.Windows.Forms.RadioButton();
            this.rb_NotStroy = new System.Windows.Forms.RadioButton();
            this.label1 = new System.Windows.Forms.Label();
            this.ok = new System.Windows.Forms.Button();
            this.Cancel = new System.Windows.Forms.Button();
            this.cbb_type_error = new System.Windows.Forms.ComboBox();
            this.SuspendLayout();
            // 
            // txt_docs
            // 
            this.txt_docs.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.txt_docs.Location = new System.Drawing.Point(148, 221);
            this.txt_docs.MaximumSize = new System.Drawing.Size(462, 27);
            this.txt_docs.MinimumSize = new System.Drawing.Size(462, 27);
            this.txt_docs.Name = "txt_docs";
            this.txt_docs.Size = new System.Drawing.Size(462, 20);
            this.txt_docs.TabIndex = 6;
            // 
            // txt_kadastr
            // 
            this.txt_kadastr.Location = new System.Drawing.Point(148, 188);
            this.txt_kadastr.Name = "txt_kadastr";
            this.txt_kadastr.Size = new System.Drawing.Size(462, 20);
            this.txt_kadastr.TabIndex = 7;
            this.txt_kadastr.TextChanged += new System.EventHandler(this.txt_kadastr_TextChanged);
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(13, 265);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(129, 13);
            this.label12.TabIndex = 3;
            this.label12.Text = "Тип исправляемой ошб.";
            // 
            // lbl_docs
            // 
            this.lbl_docs.AutoSize = true;
            this.lbl_docs.Location = new System.Drawing.Point(13, 224);
            this.lbl_docs.Name = "lbl_docs";
            this.lbl_docs.Size = new System.Drawing.Size(122, 13);
            this.lbl_docs.TabIndex = 4;
            this.lbl_docs.Text = "Сведения в документе";
            // 
            // lbl_kad
            // 
            this.lbl_kad.AutoSize = true;
            this.lbl_kad.Location = new System.Drawing.Point(13, 18);
            this.lbl_kad.Name = "lbl_kad";
            this.lbl_kad.Size = new System.Drawing.Size(110, 13);
            this.lbl_kad.TabIndex = 5;
            this.lbl_kad.Text = "Кадастровый номер";
            // 
            // txt_cadNumber
            // 
            this.txt_cadNumber.Location = new System.Drawing.Point(148, 18);
            this.txt_cadNumber.Name = "txt_cadNumber";
            this.txt_cadNumber.Size = new System.Drawing.Size(462, 20);
            this.txt_cadNumber.TabIndex = 7;
            this.txt_cadNumber.TextChanged += new System.EventHandler(this.txt_kadastr_TextChanged);
            // 
            // rbCadNum
            // 
            this.rbCadNum.AutoSize = true;
            this.rbCadNum.Checked = true;
            this.rbCadNum.Location = new System.Drawing.Point(16, 56);
            this.rbCadNum.Name = "rbCadNum";
            this.rbCadNum.Size = new System.Drawing.Size(126, 17);
            this.rbCadNum.TabIndex = 9;
            this.rbCadNum.TabStop = true;
            this.rbCadNum.Text = "Земельный участок";
            this.rbCadNum.UseVisualStyleBackColor = true;
            // 
            // rbBuilding
            // 
            this.rbBuilding.AutoSize = true;
            this.rbBuilding.Location = new System.Drawing.Point(16, 79);
            this.rbBuilding.Name = "rbBuilding";
            this.rbBuilding.Size = new System.Drawing.Size(62, 17);
            this.rbBuilding.TabIndex = 9;
            this.rbBuilding.Text = "Здание";
            this.rbBuilding.UseVisualStyleBackColor = true;
            // 
            // rbStroy
            // 
            this.rbStroy.AutoSize = true;
            this.rbStroy.Location = new System.Drawing.Point(16, 102);
            this.rbStroy.Name = "rbStroy";
            this.rbStroy.Size = new System.Drawing.Size(87, 17);
            this.rbStroy.TabIndex = 9;
            this.rbStroy.Text = "Сооружение";
            this.rbStroy.UseVisualStyleBackColor = true;
            // 
            // rbPomech
            // 
            this.rbPomech.AutoSize = true;
            this.rbPomech.Location = new System.Drawing.Point(16, 125);
            this.rbPomech.Name = "rbPomech";
            this.rbPomech.Size = new System.Drawing.Size(86, 17);
            this.rbPomech.TabIndex = 9;
            this.rbPomech.Text = "Помещение";
            this.rbPomech.UseVisualStyleBackColor = true;
            this.rbPomech.CheckedChanged += new System.EventHandler(this.radioButton4_CheckedChanged);
            // 
            // rb_NotStroy
            // 
            this.rb_NotStroy.AutoSize = true;
            this.rb_NotStroy.Location = new System.Drawing.Point(16, 148);
            this.rb_NotStroy.Name = "rb_NotStroy";
            this.rb_NotStroy.Size = new System.Drawing.Size(229, 17);
            this.rb_NotStroy.TabIndex = 9;
            this.rb_NotStroy.Text = "Объект незавершенного строительства";
            this.rb_NotStroy.UseVisualStyleBackColor = true;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(13, 188);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(115, 13);
            this.label1.TabIndex = 4;
            this.label1.Text = "Сведения в кадастре";
            // 
            // ok
            // 
            this.ok.DialogResult = System.Windows.Forms.DialogResult.OK;
            this.ok.Location = new System.Drawing.Point(454, 307);
            this.ok.Name = "ok";
            this.ok.Size = new System.Drawing.Size(75, 23);
            this.ok.TabIndex = 10;
            this.ok.Text = "Ok";
            this.ok.UseVisualStyleBackColor = true;
            this.ok.Click += new System.EventHandler(this.ok_Click);
            // 
            // Cancel
            // 
            this.Cancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.Cancel.Location = new System.Drawing.Point(535, 307);
            this.Cancel.Name = "Cancel";
            this.Cancel.Size = new System.Drawing.Size(75, 23);
            this.Cancel.TabIndex = 10;
            this.Cancel.Text = "Cancel";
            this.Cancel.UseVisualStyleBackColor = true;
            // 
            // cbb_type_error
            // 
            this.cbb_type_error.Cursor = System.Windows.Forms.Cursors.Hand;
            this.cbb_type_error.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbb_type_error.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cbb_type_error.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.cbb_type_error.FormattingEnabled = true;
            this.cbb_type_error.Location = new System.Drawing.Point(148, 258);
            this.cbb_type_error.Name = "cbb_type_error";
            this.cbb_type_error.Size = new System.Drawing.Size(462, 26);
            this.cbb_type_error.TabIndex = 67;
            // 
            // FrTechnicalError
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(617, 334);
            this.Controls.Add(this.cbb_type_error);
            this.Controls.Add(this.Cancel);
            this.Controls.Add(this.ok);
            this.Controls.Add(this.rb_NotStroy);
            this.Controls.Add(this.rbPomech);
            this.Controls.Add(this.rbStroy);
            this.Controls.Add(this.rbBuilding);
            this.Controls.Add(this.rbCadNum);
            this.Controls.Add(this.txt_docs);
            this.Controls.Add(this.txt_cadNumber);
            this.Controls.Add(this.txt_kadastr);
            this.Controls.Add(this.label12);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.lbl_docs);
            this.Controls.Add(this.lbl_kad);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
            this.Name = "FrTechnicalError";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Данные для исправления тех. ошибки";
            this.Load += new System.EventHandler(this.FrTechnicalError_Load);
            this.Shown += new System.EventHandler(this.FrTechnicalError_Shown);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label lbl_docs;
        private System.Windows.Forms.Label lbl_kad;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button ok;
        private System.Windows.Forms.Button Cancel;
        public System.Windows.Forms.TextBox txt_docs;
        public System.Windows.Forms.TextBox txt_kadastr;
        public System.Windows.Forms.TextBox txt_cadNumber;
        public System.Windows.Forms.RadioButton rbCadNum;
        public System.Windows.Forms.RadioButton rbBuilding;
        public System.Windows.Forms.RadioButton rbStroy;
        public System.Windows.Forms.RadioButton rbPomech;
        public System.Windows.Forms.RadioButton rb_NotStroy;
        public System.Windows.Forms.ComboBox cbb_type_error;
    }
}