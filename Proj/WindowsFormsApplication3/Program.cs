﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;
using NLog;
using System.Threading;
using System.IO;

namespace PortalRosreestr
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {

            ThreadExceptionHandler.logger.Info("================================Очередной запуск " + DateTime.Today.ToString() + "===================================");

            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            
            ThreadExceptionHandler handler = new ThreadExceptionHandler();
            Application.ThreadException += new ThreadExceptionEventHandler(handler.Application_ThreadException);
            Application.SetUnhandledExceptionMode(UnhandledExceptionMode.CatchException);
            AppDomain.CurrentDomain.UnhandledException +=new UnhandledExceptionEventHandler(CurrentDomain_UnhandledException);
            if (!Directory.Exists(Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData) + "\\PortalRosreestr"))
            Directory.CreateDirectory(Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData) + "\\PortalRosreestr");
            Directory.CreateDirectory(Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData) + "\\PortalRosreestr\\Logs");
            
            Application.Run(new FrPortalRosreestr());
            
        }

        private static void CurrentDomain_UnhandledException(object sender, UnhandledExceptionEventArgs e)
        {
            try
            {
                Exception ex = (Exception)e.ExceptionObject;
                ThreadExceptionHandler.logger.Debug(ex.Message + " " + ex.StackTrace);
            }
            catch (Exception exc)
            {
                try
                {
                    MessageBox.Show("Fatal Non-UI Error",
                        "Fatal Non-UI Error. Could not write the error to the event log. Reason: "
                        + exc.Message, MessageBoxButtons.OK, MessageBoxIcon.Stop);
                }
                finally
                {
                    MessageBox.Show("Неожиданно!");
                    //Application.Exit();
                }
            }
        }
    }

    /// <summary>
    ///  Обмениваться данными между формами - и выполнять различные методы и процедуры 
    /// </summary>
    public static class CallBackMy
    {
        public delegate void callBackEvent(string what);
        public static callBackEvent callBackEventHandler;
    }
  
}
