﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Windows.Forms;
using PortalRosreestr.ru.rosreestr.portal;
using System.IO;
using SQLiteHelperTestApp;


namespace PortalRosreestr
{   
    public partial class FrPortalRosreestr : Form
    {
        public static sqlStructurecs sqlStruct;
        public static Rostreestr portalRosreestr;
        public static FrSettings frSettings = new FrSettings();
        public static FrSetCertificat frSettCert = new FrSetCertificat();
        public static frmShowLoadAnswer frShowLoadAnswer = new frmShowLoadAnswer();

        private string _textFilters;
        frmSearch frmSH = null;
        DataTable dtTableList = new DataTable();
        
        public FrPortalRosreestr()
        {
                ///добавим обработчик события- который запустить функцию  - какую не важно
              //TODO : test потом подключить   CallBackMy.callBackEventHandler = new CallBackMy.callBackEvent(this.MessMain);
                
                InitializeComponent();       
                

                dataGridView.EnableHeadersVisualStyles = false; //для применения свои стилей в header
             //   dataGridView.ScrollBars = ScrollBars.Both;

                SettHelper.props.readSetting();
                if (SettHelper.props.GeneralSett.isShowSettings)
                {
                    frSettings.ShowDialog();                //настройки
                }
                else frSettings.readSetting();

                notifyIcon.Visible = false;
                notifyIcon.MouseDoubleClick += new MouseEventHandler(notifyIcon_MouseDoubleClick);

                portalRosreestr = new Rostreestr(); ///соединение с порталом  получение serialNUmber кадастрвого инженера
                if (!portalRosreestr.isConnect()) Environment.Exit(0);
                sqlStruct = new sqlStructurecs(); ///соединение с базой                
                ///Редактирование данных кадастрового энж

                if ((!sqlStruct.FindCertByID()) || ((SettHelper.props.GeneralSett.isShowSettCert)))
                {
                    frSettCert.ShowDialog();
                }
                else frSettCert.readSetting();

                //hScrollBar1 = dataGridView.ScrollBars;
        }

        /// <summary>
        ///  тест делегата передача параметров в другую форму
        /// </summary>
        /// <param name="param"></param>
        void MessMain(string param)
        {
         //  MessageBox.Show(lblRefresh.Text);
        }
        
        /// <summary>
        ///   Обновление  данных
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void backgroundWorker1_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            this.pictureBox.Image = null;
            if (e.Cancelled)
            {
                this.labelProgress.Text = "Operation cancelled by the user!";
                this.pictureBox.Image = Properties.Resources.WarningImage;
            }
            else
            {
                if (e.Error != null)
                {
                    this.labelProgress.Text = "Operation failed: " + e.Error.Message;
                    this.pictureBox.Image = Properties.Resources.ErrorImage;
                }
                else
                {
                    this.labelProgress.Text = "Operation finished successfuly!";
                    this.pictureBox.Image = Properties.Resources.InformationImage;
                }
            }
        }


        
       /// <summary>
       ///  Получение  списка  заявок с  портала росреестра
       /// </summary>
       /// <returns></returns>
         bool ListEventsPoral()
        {
            string lbl = "На сервере Росреестра пакетов : ";
            this.lblRefresh.Invoke((MethodInvoker)delegate
            {
                this.lblRefresh.Text = "";
            });

            List<eventStruct> lstEventsOrbr = portalRosreestr.ListEvents();
            ThreadExceptionHandler.logger.Info("list get " + DateTime.Now.ToString());
            if ((lstEventsOrbr == null) || (lstEventsOrbr.Count <= 0)) return false; ///нечего обновлять
            try
            {
                ThreadExceptionHandler.logger.Info("Список заявок");
                lbl += lstEventsOrbr.Count.ToString()+ "; ";
                for (int i = 0; i < lstEventsOrbr.Count(); i++)
                {
                    var el = lstEventsOrbr[i];
                    this.lblRefresh.Invoke((MethodInvoker)delegate
                    {
                        this.lblRefresh.Text = lbl+ "Обновление статуса заявки "+ (i+1).ToString()+" - " + el.requestNumber + " "+el.eventID;
                    });                   
                    ThreadExceptionHandler.logger.Info(el.requestNumber + " " + el.eventID);
                    if (!FrPortalRosreestr.sqlStruct.IsMainRequest(el.requestNumber))
                        ThreadExceptionHandler.logger.Info("net");
                    ThreadExceptionHandler.logger.Info("IsMainRequest " + DateTime.Now.ToString());
                    if (el.eventID.IndexOf("STATUS") != -1)
                    {
                        portalRosreestr.UpdateStatus(el);
                        ThreadExceptionHandler.logger.Info("UpdateStatus " + DateTime.Now.ToString());
                    }
                    else
                        if (FrPortalRosreestr.sqlStruct.IsMainRequest(el.requestNumber))
                        {
                           portalRosreestr.InsertOutDataRosreestr(el);
                           ThreadExceptionHandler.logger.Info("InsertOutDataRosreestr " + DateTime.Now.ToString());
                        }
                };
                return true;
            }
            catch (Exception ex)
            {
                ThreadExceptionHandler.ShowEventDialog("Возникли проблемы с обновлением данных! ", ex);
                return false;
            }
        }
       
        /// <summary>
        ///  Обновления состояния заявок  
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void backgrWork_DoWork(object sender, DoWorkEventArgs e)
        {
            ThreadExceptionHandler.logger.Info("Start " + DateTime.Now.ToString());
            ListEventsPoral();
            ThreadExceptionHandler.logger.Info("GetListEventsPoral " + DateTime.Now.ToString());
            ValidateChildren();
            bindSr.SuspendBinding(); /// приостанавливаем все действия  привязки данных - для обнолвнеия
            bindSr.RaiseListChangedEvents = false;            
            dtTableList.BeginLoadData(); 
            dtTableList.Clear();
            dtTableList = sqlStruct.GetEvents(CurrentCert.SerialNumber);

            dtTableList.EndLoadData();
            bindSr.RaiseListChangedEvents = true;
            
        }

       
        /// <summary>
        ///    После  завершения  обновления спсика  заявок
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void backgrWork_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            try{
                bindSr.DataSource = dtTableList;
            }
            catch (Exception ex)
            {
                ThreadExceptionHandler.ShowEventDialog("ОЙ", ex);
                return;
            }
            finally
            {
                this.dataGridView.Enabled = true;
                this.pnlRefresh.Visible = false;
            }

        }

       
        /// <summary>
        ///  Скачать из бызы полученный результат 
        /// </summary>
        /// <param name="i"> номер строки в гриде</param>
        private void SaveAnswer(int i)
        {
                string requestNumber = dataGridView["requestNumber",i].Value.ToString();
                byte[] res = sqlStruct.LoadFileByOutFile(requestNumber);
                if (res != null) 
                    File.WriteAllBytes(saveFileDialog1.FileName + "_" + requestNumber + "_out.zip", res);
        }

       
        /// <summary>
        ///   Скачать ответный файл с росреестра
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void dataGridView_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            try
            {
                if ((e.ColumnIndex == 2))
                {
                    if (dataGridView.SelectedRows[0].Cells["countOutFile"].Value.ToString() == "0") return;
                    saveFileDialog1.Title = "Скачать ответ?";
                    if (saveFileDialog1.ShowDialog() == DialogResult.OK)
                    {
                        SaveAnswer(e.RowIndex);
                        MessageBox.Show("Результат готов! ");
                    }
                }
            }
            catch (Exception ex)
            {
                ThreadExceptionHandler.ShowEventDialog("", ex);
            }
        }

        /// <summary>
        ///   Обработка состояния заявок   - -TODO :  так - не правильн - потом пределать - может  быть ))
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void dataGridView_CellFormatting(object sender, DataGridViewCellFormattingEventArgs e)
        {
            if (e.ColumnIndex == -1) return;
            try
            {
                if (dataGridView.Columns[e.ColumnIndex].Name == "ID")
                {
                    if ((e.Value != null))
                    {
                        var str = dataGridView["Status", e.RowIndex].Value;
                        if (str != null)
                        {
                            if ((str.ToString().IndexOf("не пройдена") != -1) ||
                                (str.ToString().IndexOf("Завершена отказом") != -1) ||
                                (str.ToString().IndexOf("Приостановлена.") != -1) ||
                                (str.ToString().IndexOf("Приостановлена.") != -1) ||
                                (str.ToString().IndexOf("Отказ в обработке") != -1) ||
                                (str.ToString().IndexOf("ГКУ приостановлен") != -1)
                               )
                            {
                                e.Value = imageList1.Images["cancel"];
                            }
                            else
                            {
                                if ((str.ToString().IndexOf("Ответ") != -1) ||
                                    (str.ToString().IndexOf("Выполнена") != -1)
                                    )
                                {
                                    e.Value = imageList1.Images["es"];
                     
                                }
                                else e.Value = imageList1.Images["advance"];
                            }
                        }
                    }
                }
                else
                {
                    if (dataGridView.Columns[e.ColumnIndex].Name == "countOutFile")
                    {
                        var zn = dataGridView["countOutFile", e.RowIndex].Value;
                        if (zn != null)
                        {
                            if (zn.ToString() != "0") e.Value = imageList1.Images["file"];
                            else e.Value = imageList1.Images["pusto"];
                        }

                    }
                }
            }
            catch (Exception ex)
            {
                ThreadExceptionHandler.ShowEventDialog("", ex);
            }
        }

        /// <summary>
        ///   Поиск
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void dataGridView_KeyDown(object sender, KeyEventArgs e)
        {
            try
            {
                if ((e.KeyCode == Keys.F) && (e.Modifiers == Keys.Control))
                {

                    if (поискToolStripMenuItem.Enabled)
                        tSBtnFind_Click(null, null);
                }
                else if (e.KeyCode == Keys.Escape)
                {
                    frmSH = null;
                    bindSr.Filter = "";
                }
            }
            catch (Exception ex)
            {
                ThreadExceptionHandler.ShowEventDialog("", ex);
            }

        }


        /// <summary>
        ///   Close
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Form1_FormClosed(object sender, FormClosedEventArgs e)
        {
            timer.Stop();
            externalWS2.Dispose();
            frSettings.Dispose();
            dtTableList.Dispose();
            ThreadExceptionHandler.logger.Info("================================Завершение работы " + DateTime.Today.ToString() + "===================================");
        }

        /// <summary>
        ///   Показали обновилил
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void FrPortalRosreestr_Shown(object sender, EventArgs e)
        {
            LoadData();
            ThreadExceptionHandler.logger.Info("show form " + DateTime.Now.ToString());

            // FrPortalRosreestr.ActiveForm.Text = "Реестр запросов кадастрового инженера (" + SettHelper.props.CertFields.FIO + " " + SettHelper.props.CertFields.Name + " " + SettHelper.props.CertFields.SurName + ")";
            // MessageBox.Show(SettHelper.props.CertFields.FIO + SettHelper.props.CertFields.Name + " " + SettHelper.props.CertFields.SurName);
            string capt = "Реестр запросов кадастрового инженера (" + SettHelper.props.CertFields.FIO + " " + SettHelper.props.CertFields.Name + " " + SettHelper.props.CertFields.SurName + ")";
            this.Text = capt;
            
                       // FrPortalRosreestr.ActiveForm.Text = "Реестр запросов кадастрового инженера (" + SettHelper.props.CertFields.FIO+")
        }

        /// <summary>
        ///  Запуск потока на  обновление
        /// </summary>
        void LoadData()
        {
            
            if (backgroundWorkerRefreshTable.IsBusy) return;
            this.dataGridView.Enabled = false;
            this.pnlRefresh.Visible = true;
            
            Application.DoEvents();

            backgroundWorkerRefreshTable.RunWorkerAsync();
        }

        /// <summary>
        ///    Показ формы  на создание заявления
        /// </summary>
        void  SendingPackage()
        {
            portalRosreestr.frOutEvents.ShowDialog(this);
        }

        private void notifyIcon_BalloonTipShown(object sender, EventArgs e)
        {
            notifyIcon.BalloonTipText = "Новые заявки от росреестра =" + portalRosreestr.CountPortalEvents().ToString();
        }

        /// <summary>
        ///   Трей
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void notifyIcon_MouseDoubleClick(object sender, MouseEventArgs e)
        {
            notifyIcon.Visible = false;
            this.ShowInTaskbar = true;
            WindowState = FormWindowState.Normal;
        }


        /// <summary>
        ///   Фильтр записей
        /// </summary>
        /// <param name="fields"></param>
        private void searchgrid_TextChanged(List<field> fields)
        {
            _textFilters = ""; //reset filters
            bool first = true; //to handle the " and "
            foreach (field f in fields)
            {
                if (f.Value.Length > 0) //only if there is a value to filter for
                {
                    if (!first) _textFilters += " and ";
                    _textFilters += f.Field + " like '%" + f.Value + "%'";
                    first = false;
                }
            }
            bindSr.Filter = _textFilters;
        }


     
        /// <summary>
        ///  Автоматическое обноавление
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void timer1_Tick(object sender, EventArgs e)
        {
            notifyIcon.Visible = true;
            notifyIcon.ShowBalloonTip(40);
            double countRex = portalRosreestr.CountPortalEvents();
            double res = countRex - Convert.ToDouble(lblRefresh.Tag);
            if (res <= 0) return;
            lblRefresh.Text = "Кол-во новых заявок :  " + res.ToString();
            lblRefresh.Tag = res.ToString();
            LoadData();
        }

        private void toolStripButton1_Click(object sender, EventArgs e)
        {
            frSettings.txtBD.Enabled = false;
            frSettings.btnSelBD.Enabled = false;
            frSettings.ShowDialog();
        }

        private void toolStripButton2_Click(object sender, EventArgs e)
        {
            AboutBox1 act = new AboutBox1();
            act.ShowDialog();
        }

        private void toolStripButton6_Click(object sender, EventArgs e)
        {
            lblRefresh.Text = "История заявок : " + portalRosreestr.CountPortalEvents().ToString();
        }

        /// <summary>
        ///  Вызов  поиска 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void tSBtnFind_Click(object sender, EventArgs e)
        {
            if (frmSH == null)
            {
                List<field> fields = new List<field>();
                field f = new field();
                f.FriendlyName = "txtNamePack";
                f.Field = "NamePack";
                fields.Add(f);
                f = new field();
                f.FriendlyName = "Data";
                f.Field = "Data";
                fields.Add(f);
                f = new field();
                f.FriendlyName = "Номер заявки";
                f.Field = "RequestNumber";
                fields.Add(f);
                f = new field();
                f.FriendlyName = "E-mail";
                f.Field = "Email";
                fields.Add(f);
                f = new field();
                f.FriendlyName = "Статус";
                f.Field = "Status";
                fields.Add(f);


                frmSH = new frmSearch(fields);
                frmSH.TextChanged += new SearchContextChangedHandler(searchgrid_TextChanged);
            }
            frmSH.ShowDialog();

        }

        private void toolStripLabel1_Click(object sender, EventArgs e)
        {
            /// Загрузить file
            OpenFileDialog op = new OpenFileDialog();

            if (op.ShowDialog() == DialogResult.OK)
            {
                FileStream fs = new FileStream(op.InitialDirectory + op.FileName, FileMode.Open, FileAccess.Read);
                byte[] readBuf = new byte[fs.Length];
                fs.Read(readBuf, 0, (Convert.ToInt32(fs.Length)));
                sqlStruct.UpdateFileByRequestNumber(dataGridView.SelectedRows[0].Cells["RequestNumber"].Value.ToString(), readBuf);
            }


        }

        /// <summary>
        ///   Отправить  заявку!
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void toolStripLabelOUT_Click(object sender, EventArgs e)
        {
            SendingPackage();
            // outEvents.ShowDialog(this);            
         
            LoadData();
        }

        private void toolStripLabelUpdate_Click(object sender, EventArgs e)
        {            
            LoadData();
        }
        private void UpdateStarusByID(DataGridViewCell dataGridViewCell)
        {
            throw new NotImplementedException();
        }


        /// <summary>
        ///  Удаление заявки  с портала росреестра
        /// </summary>
        /// <param name="i"></param>
        void _Portal_DeletePack(int i)
        {
            string req = dataGridView.Rows[i].Cells["RequestNumber"].Value.ToString();
            string gd = sqlStruct.EventIDByRequest(req);
            List<eventStruct> lstEventsOrbr = portalRosreestr.ListEvents();
            ThreadExceptionHandler.logger.Info("list get " + DateTime.Now.ToString());
            if ((lstEventsOrbr != null) && (lstEventsOrbr.Count > 0))
            {
                for (int j = 0; j < lstEventsOrbr.Count(); j++)
                {
                    if (lstEventsOrbr[j].requestNumber == req)
                        portalRosreestr.DeleteEvent(lstEventsOrbr[j].eventID);
                }
            }
        }
       

        /// <summary>
        ///  Удалить заявку из  бызы данных 
        /// </summary>
        void _BD_DeletePack(int i)
        {
            if (sqlStruct.BD_DeletePack(dataGridView.Rows[i].Cells["RequestNumber"].Value.ToString()))
            {
                this.bindSr.RemoveAt(dataGridView.Rows[i].Index);
            }
        }
       
        /// <summary>
        ///  Удаление  заявок
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void вырезатьToolStripButton_Click(object sender, EventArgs e)
        {
                    if (backgroundWorkerDelete.IsBusy) return;

                    this.dataGridView.Enabled = false;
                    this.pnlRefresh.Visible = true;
                    Application.DoEvents();
                    backgroundWorkerDelete.RunWorkerAsync();
                    frShowLoadAnswer.label1.Text = "...Подождите... удаление";
                    frShowLoadAnswer.ShowDialog(this);
        }


        /// <summary>
        ///   Отправить  в архив
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void отправитьВАрхивToolStripMenuItem_Click(object sender, EventArgs e)
        {
            foreach (DataGridViewRow el in this.dataGridView.Rows)
            {
                DataGridViewCheckBoxCell cell = el.Cells[0] as DataGridViewCheckBoxCell;
                if (cell.Value == cell.TrueValue)
                    UpdateStarusByID(el.Cells["IDRosreestr"]);  
            }
        }

        private void tlsmEngenery_Click(object sender, EventArgs e)
        {
           
        }

        private void backWOut_DoWork(object sender, DoWorkEventArgs e)
        {
            //FrOutEvents outEvents = new FrOutEvents();
            portalRosreestr.frOutEvents.ShowDialog(this);
        
        }

        private void backWOut_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            try
            {
             //   bindSr.DataSource = dtTableList;
             //   int countRec = portalRosreestr.CountPortalEvents();
             //   tlSpNewCountRecord.Text = "На сервере Росреестра пакетов :  " + countRec.ToString();
             //   tlSpNewCountRecord.Tag = countRec;
            }
            catch (Exception ex)
            {
               // ThreadExceptionHandler.ShowEventDialog("ОЙ", ex);
               // return;
            }
            finally
            {
                //TODO : временно this.Cursor = Cursors.Arrow;
                //TODO : временно this.pictureBox.Image = null;
               // this.dataGridView.Enabled = true;
                //TODO : временно  this.panel3.Invoke((MethodInvoker)delegate
                //TODO : временно  {
                //TODO : временно      panel3.Visible = false;
                //TODO : временно  });
            }

        }

        private void dataGridView_DataError(object sender, DataGridViewDataErrorEventArgs e)
        {
            if (e.RowIndex < 0) ThreadExceptionHandler.logger.Error(e.ThrowException.ToString());
        }


        /// <summary>
        ///  Скачать исходный пакет - отправленный на портал росреестра
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void dataGridView_CellContentDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.ColumnIndex == 6) return;
            if (e.ColumnIndex == 5)
            try 
            {

                if (dataGridView.SelectedRows.Count == 1)
                {
                    string requestNumber = dataGridView.SelectedRows[0].Cells["requestNumber"].Value.ToString();
                    // string name = dataGridView.SelectedRows[0].Cells["fileNameReq"].Value.ToString();
                    string name = sqlStruct.FileNameByRequestNumber(requestNumber);
                    saveFileDialog1.Title = "Скачать исходный файл?";
                    if (saveFileDialog1.ShowDialog() == DialogResult.OK)
                    {
                        byte[] res = sqlStruct.LoadFileByRequestNumber(requestNumber);
                        if (res == null) MessageBox.Show("Файл  не найден");
                        else
                        {
                            File.WriteAllBytes(saveFileDialog1.FileName + "_" + name, res);
                            MessageBox.Show("Результат готов! ");
                        }
                    }
                }    
            }
            catch (Exception ex)
            {
                ThreadExceptionHandler.ShowEventDialog("", ex);
            }
        }

        private void сменаКадИнженераToolStripMenuItem_Click(object sender, EventArgs e)
        {
            frSettCert.ShowDialog();
            LoadData();
            FrPortalRosreestr.ActiveForm.Text = "Реестр запросов кадастрового инженера (" + SettHelper.props.CertFields.FIO + " " + SettHelper.props.CertFields.Name + " " + SettHelper.props.CertFields.SurName + ")";
        }

        private void folderBrowserDialog1_HelpRequest(object sender, EventArgs e)
        {

        }

        private void pictureBox2_Click(object sender, EventArgs e)
        {

        }

        private void кодыПлатежейToolStripMenuItem_Click(object sender, EventArgs e)
        {
            FrKodyPlay fr = new FrKodyPlay();
            fr.ShowDialog();
        }

        private void LoadFilesAnswers()
        {
                for (int i = dataGridView.Rows.Count - 1; i >= 0; i--)
                {
                    if ((bool)(dataGridView.Rows[i].Cells["Checked"].EditedFormattedValue))
                    {
                        SaveAnswer(i);
                        frShowLoadAnswer.textBox1.Invoke((MethodInvoker)delegate
                        {
                            frShowLoadAnswer.textBox1.Text += dataGridView.Rows[i].Cells["requestNumber"].Value.ToString() + Environment.NewLine;
                        });
                      
                    }
                }
                //MessageBox.Show(dlg.Radio.ToString());
            
        }

        private void скачатьОтветToolStripMenuItem_Click(object sender, EventArgs e)
        {
            saveFileDialog1.Title = "Скачать ответ?";
            if (saveFileDialog1.ShowDialog() == DialogResult.OK)
            {

                if (backgroundWorkerLoad.IsBusy) return;

                // this.dataGridView.Enabled = false;
                this.pnlRefresh.Visible = true;
                Application.DoEvents();
                backgroundWorkerLoad.RunWorkerAsync();
                frShowLoadAnswer.label1.Text = "...Подождите...";
                frShowLoadAnswer.ShowDialog(this);
            }
        }

        private void backgroundWorkerLoad_DoWork(object sender, DoWorkEventArgs e)
        {
            ThreadExceptionHandler.logger.Info("Start " + DateTime.Now.ToString());
            LoadFilesAnswers();
            ThreadExceptionHandler.logger.Info("GetListEventsPoral " + DateTime.Now.ToString());
            ValidateChildren();            
        }

        private void backgroundWorkerLoad_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            try
            {
                MessageBox.Show("Результат готов!");
            }
            catch (Exception ex)
            {
                ThreadExceptionHandler.ShowEventDialog("ОЙ", ex);
                return;
            }
            finally
            {
                frShowLoadAnswer.Close();
               // this.dataGridView.Enabled = true;
               // this.pnlRefresh.Visible = false;
            }
        }

        private void checkBox1_CheckedChanged(object sender, EventArgs e)
        {
            if (checkBox1.Checked == false)
            {
                foreach (DataGridViewRow row in dataGridView.Rows)
                {
                    DataGridViewCheckBoxCell chk = (DataGridViewCheckBoxCell)row.Cells[0];

                    chk.Value = chk.TrueValue;
                }
            }
            else if (checkBox1.Checked == true)
            {
                foreach (DataGridViewRow row in dataGridView.Rows)
                {
                    DataGridViewCheckBoxCell chk = (DataGridViewCheckBoxCell)row.Cells[0];
                    chk.Value = 1;
                    if (row.IsNewRow)
                    {
                        chk.Value = 0;
                    }
                }
            }
        }

        private void FrPortalRosreestr_Load(object sender, EventArgs e)
        {

        }

        void _BD_OutArchive(int i)
        {
           sqlStruct.UpdateStatusByRequestNumber(dataGridView.Rows[i].Cells["RequestNumber"].Value.ToString());
             //  dataGridView.Rows.Remove(dataGridView.Rows[i]);
        }

        private void OutArchiveReq()
        {
            if (backgroundWorkerOutArchive.IsBusy) return;
            Cursor.Current = Cursors.WaitCursor;
            Application.DoEvents();
            panel1.Enabled = false;
            backgroundWorkerOutArchive.RunWorkerAsync();
        }

        /// <summary>
        ///   Отправить в архив
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void toolStripMenuItem3_Click(object sender, EventArgs e)
        {
                if (backgroundWorkerOutArchive.IsBusy) return;
                this.dataGridView.Enabled = false;
                this.pnlRefresh.Visible = true;
                Application.DoEvents();
                backgroundWorkerOutArchive.RunWorkerAsync();
                frShowLoadAnswer.label1.Text = "... Идет отправка в архив...";
                frShowLoadAnswer.ShowDialog(this);
                
        }

     

        private void UpdateStatusArchive()
        {
                for (int i = dataGridView.Rows.Count - 1; i >= 0; i--)
                {
                    if ((bool)(dataGridView.Rows[i].Cells["Checked"].EditedFormattedValue))
                    {
                       sqlStruct.UpdateStatusByRequestNumber(dataGridView.Rows[i].Cells["RequestNumber"].Value.ToString());
                       frShowLoadAnswer.textBox1.Invoke((MethodInvoker)delegate
                       {
                           frShowLoadAnswer.textBox1.Text += dataGridView.Rows[i].Cells["requestNumber"].Value.ToString() + Environment.NewLine;
                       });
                    }
                    
                }
        }

        private void backgroundWorkerOutArchive_DoWork(object sender, DoWorkEventArgs e)
        {
            ThreadExceptionHandler.logger.Info("Start " + DateTime.Now.ToString());
            UpdateStatusArchive();
            ThreadExceptionHandler.logger.Info("GetListEventsPoral " + DateTime.Now.ToString());
            ValidateChildren();
            bindSr.SuspendBinding(); /// приостанавливаем все действия  привязки данных - для обнолвнеия
            bindSr.RaiseListChangedEvents = false;
            dtTableList.BeginLoadData();
            dtTableList.Clear();
            dtTableList = sqlStruct.GetEvents(CurrentCert.SerialNumber);

            dtTableList.EndLoadData();
            bindSr.RaiseListChangedEvents = true;           

        }

        private void backgroundWorkerOutArchive_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            try
            {
                bindSr.DataSource = dtTableList;
                MessageBox.Show("Данные отправлены в архив! (Просмотреть  их сможете, с помощью поиска!)");
            }
            catch (Exception ex)
            {
                ThreadExceptionHandler.ShowEventDialog("ОЙ", ex);
                return;
            }
            finally
            {
                this.dataGridView.Enabled = true;
                this.pnlRefresh.Visible = false;
                frShowLoadAnswer.Close();
            }
            
        }

        private void backgroundWorkerLoadKPT_DoWork(object sender, DoWorkEventArgs e)
        {
            ThreadExceptionHandler.logger.Info("sending packet ");
            if (e.Cancel == true) return;
            UpdateStatusArchive();
        }

        private void backgroundWorkerLoadKPT_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {

            try
            {
                //this.buttonOut.Text = Consts.MSGOUT;

            }
            catch (Exception ex)
            {
                ThreadExceptionHandler.logger.Error(ex.Message + " " + ex.StackTrace);
            }
            finally
            {
                this.Cursor = Cursors.Default;
                panel3.Visible = false;                
            }
        }

        private void startToolStripMenuItem_Click(object sender, EventArgs e)
        {

        }

        private void DeleteRequestyNUmbers()
        {
               /* for (int i = dataGridView.Rows.Count - 1; i >= 0; i--)
                {
                    if ((bool)(dataGridView.Rows[i].Cells["Checked"].EditedFormattedValue))
                    {
                       sqlStruct.UpdateStatusByRequestNumber(dataGridView.Rows[i].Cells["RequestNumber"].Value.ToString());
                       frShowLoadAnswer.textBox1.Invoke((MethodInvoker)delegate
                       {
                           frShowLoadAnswer.textBox1.Text += dataGridView.Rows[i].Cells["requestNumber"].Value.ToString() + Environment.NewLine;
                       });
                    }
                    
                }
                * */
             using (MyDialog dlg = new MyDialog())
                {
                    DialogResult r = dlg.ShowDialog();
                    if (dlg.DialogResult == System.Windows.Forms.DialogResult.Cancel) return;
                    int isType = dlg.Radio;
                    this.dtTableList.BeginInit();
                    for (int i = dataGridView.Rows.Count-1; i >=0; i--)
                       {
                           if ((bool)(dataGridView.Rows[i].Cells["Checked"].EditedFormattedValue))
                           {
                               _Portal_DeletePack(i);
                               frShowLoadAnswer.textBox1.Invoke((MethodInvoker)delegate
                               {
                                   frShowLoadAnswer.textBox1.Text += dataGridView.Rows[i].Cells["requestNumber"].Value.ToString() + Environment.NewLine;
                               });
                               if (isType == 0) _BD_DeletePack(i);                               
                           }
                       }
                 /*bindSr.SuspendBinding(); /// приостанавливаем все действия  привязки данных - для обнолвнеия
                 bindSr.RaiseListChangedEvents = false;            
                 bindSr.RaiseListChangedEvents = true;*/
                    this.dtTableList.EndInit();
             }

        }

        private void backgroundWorkerDelete_DoWork(object sender, DoWorkEventArgs e)
        {
      
            ThreadExceptionHandler.logger.Info("Start " + DateTime.Now.ToString());
            DeleteRequestyNUmbers();
            ThreadExceptionHandler.logger.Info("GetListEventsPoral " + DateTime.Now.ToString());
            ValidateChildren();

            /*bindSr.SuspendBinding(); /// приостанавливаем все действия  привязки данных - для обнолвнеия
            bindSr.RaiseListChangedEvents = false;
            dtTableList.BeginLoadData();
            dtTableList.Clear();
            dtTableList = sqlStruct.GetEvents(CurrentCert.SerialNumber);

            dtTableList.EndLoadData();
            bindSr.RaiseListChangedEvents = true;           */

        }

        private void backgroundWorkerDelete_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            try
            {
                bindSr.DataSource = dtTableList;
                bindSr.ResetBindings(false);
                MessageBox.Show("Данные  удалены!");
            }
            catch (Exception ex)
            {
                ThreadExceptionHandler.ShowEventDialog("ОЙ", ex);
                return;
            }
            finally
            {
                this.dataGridView.Enabled = true;
                this.pnlRefresh.Visible = false;
                frShowLoadAnswer.Close();
            }
        }

        private void toolStripMenuItem6_Click(object sender, EventArgs e)
        {
            portalRosreestr.frOutEvents.StartSign();
            MessageBox.Show("Архив подписан!");
        }
    }

    
    /// <summary>
    ///   Диалоговое окно выбора действий
    /// </summary>
    class MyDialog : System.Windows.Forms.Form
    {
        public int Radio;
        private Button cancelButton;
        private CheckBox checkbox;
        private Button okButton;
        private RadioButton radio1, radio2, radio3;
        private GroupBox radiogroup;
        public MyDialog()
        {
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            Size = new Size(500, 170);
            FormBorderStyle = FormBorderStyle.FixedDialog;
            this.Font = new System.Drawing.Font("Georgia", 9F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(204)));

            Text = "Как удалять?";

            okButton = new Button();
            okButton.DialogResult = DialogResult.OK;
            okButton.Location = new Point(20, 90);
            okButton.Size = new Size(80, 25);
            okButton.Text = "OK";
            Controls.Add(okButton);

            cancelButton = new Button();
            cancelButton.Location = new Point(400, 90);
            cancelButton.Size = new Size(80, 25);
            cancelButton.Text = "Cancel";
            cancelButton.DialogResult = DialogResult.Cancel;
            Controls.Add(cancelButton);


            radiogroup = new GroupBox();
            radiogroup.Text = "Удалять";
            radiogroup.Location = new Point(10, 60);
            radiogroup.Size = new Size(380, 80);
            radiogroup.Dock = DockStyle.Top;
            Controls.Add(radiogroup);

            radio1 = new RadioButton();
            radio1.Location = new Point(10, 15);
            radio1.Size = new Size(460, 30);
            radio1.UseCompatibleTextRendering = true;
            radio1.Click += new EventHandler(OnRadio);
            radio1.Text = "только статус заявку, с сервера росреестра (увеличивает скорость обновления информации)";
            radiogroup.Controls.Add(radio1);


            radio2 = new RadioButton();
            radio2.Location = new Point(10, 45);
            radio2.Size = new Size(460, 30);
            radio2.Click += new EventHandler(OnRadio);
            radio2.Text = "заявку, безвозвратно (удаление будет произведено и из базы, и с сервера росреестра)";
            radio2.UseCompatibleTextRendering = true;
            radiogroup.Controls.Add(radio2);

        }

        public bool Check
        {
            get { return checkbox.Checked; }
            set { checkbox.Checked = value; }
        }

        void OnRadio(Object sender, EventArgs e)
        {
            int n = -1;
            foreach (Object o in radiogroup.Controls)
            {
                if (o is RadioButton)
                {
                    RadioButton r = (RadioButton)o;
                    if (r.Checked)
                        Radio = n;
                    n++;
                }
            }
        }
    }

}
