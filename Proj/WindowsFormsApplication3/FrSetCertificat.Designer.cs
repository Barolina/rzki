﻿namespace PortalRosreestr
{
    partial class FrSetCertificat
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FrSetCertificat));
            this.tabCtrlRec = new System.Windows.Forms.TabControl();
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.linkLabel1 = new System.Windows.Forms.LinkLabel();
            this.dtP = new System.Windows.Forms.DateTimePicker();
            this.chkShow = new System.Windows.Forms.CheckBox();
            this.txtEmailPerson = new System.Windows.Forms.TextBox();
            this.lblEmailPerson = new System.Windows.Forms.Label();
            this.cbbRegion = new System.Windows.Forms.ComboBox();
            this.label3 = new System.Windows.Forms.Label();
            this.txtNumber = new System.Windows.Forms.MaskedTextBox();
            this.txtSeria = new System.Windows.Forms.MaskedTextBox();
            this.txtSnils = new System.Windows.Forms.MaskedTextBox();
            this.txtTel = new System.Windows.Forms.TextBox();
            this.lblTel = new System.Windows.Forms.Label();
            this.txtAddress = new System.Windows.Forms.TextBox();
            this.txtIssque = new System.Windows.Forms.TextBox();
            this.lblIssque = new System.Windows.Forms.Label();
            this.lblNumber = new System.Windows.Forms.Label();
            this.lblSeria = new System.Windows.Forms.Label();
            this.lblData = new System.Windows.Forms.Label();
            this.lblSnils = new System.Windows.Forms.Label();
            this.txtSurName = new System.Windows.Forms.TextBox();
            this.lblSurName = new System.Windows.Forms.Label();
            this.txtName = new System.Windows.Forms.TextBox();
            this.lblName = new System.Windows.Forms.Label();
            this.txtFIO = new System.Windows.Forms.TextBox();
            this.lblFIO = new System.Windows.Forms.Label();
            this.lblAdress = new System.Windows.Forms.Label();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.textBoxzEmail = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.dateTimePickerzDate = new System.Windows.Forms.DateTimePicker();
            this.maskedTextBoxzNumber = new System.Windows.Forms.MaskedTextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.textBoxzName = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.textBoxzFIO = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.btnOk = new System.Windows.Forms.Button();
            this.tabCtrlRec.SuspendLayout();
            this.tabPage2.SuspendLayout();
            this.tabPage1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.SuspendLayout();
            // 
            // tabCtrlRec
            // 
            this.tabCtrlRec.Controls.Add(this.tabPage2);
            this.tabCtrlRec.Controls.Add(this.tabPage1);
            this.tabCtrlRec.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tabCtrlRec.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.tabCtrlRec.ItemSize = new System.Drawing.Size(147, 35);
            this.tabCtrlRec.Location = new System.Drawing.Point(0, 0);
            this.tabCtrlRec.Multiline = true;
            this.tabCtrlRec.Name = "tabCtrlRec";
            this.tabCtrlRec.SelectedIndex = 0;
            this.tabCtrlRec.ShowToolTips = true;
            this.tabCtrlRec.Size = new System.Drawing.Size(662, 428);
            this.tabCtrlRec.TabIndex = 17;
            // 
            // tabPage2
            // 
            this.tabPage2.Controls.Add(this.linkLabel1);
            this.tabPage2.Controls.Add(this.dtP);
            this.tabPage2.Controls.Add(this.chkShow);
            this.tabPage2.Controls.Add(this.txtEmailPerson);
            this.tabPage2.Controls.Add(this.lblEmailPerson);
            this.tabPage2.Controls.Add(this.cbbRegion);
            this.tabPage2.Controls.Add(this.label3);
            this.tabPage2.Controls.Add(this.txtNumber);
            this.tabPage2.Controls.Add(this.txtSeria);
            this.tabPage2.Controls.Add(this.txtSnils);
            this.tabPage2.Controls.Add(this.txtTel);
            this.tabPage2.Controls.Add(this.lblTel);
            this.tabPage2.Controls.Add(this.txtAddress);
            this.tabPage2.Controls.Add(this.txtIssque);
            this.tabPage2.Controls.Add(this.lblIssque);
            this.tabPage2.Controls.Add(this.lblNumber);
            this.tabPage2.Controls.Add(this.lblSeria);
            this.tabPage2.Controls.Add(this.lblData);
            this.tabPage2.Controls.Add(this.lblSnils);
            this.tabPage2.Controls.Add(this.txtSurName);
            this.tabPage2.Controls.Add(this.lblSurName);
            this.tabPage2.Controls.Add(this.txtName);
            this.tabPage2.Controls.Add(this.lblName);
            this.tabPage2.Controls.Add(this.txtFIO);
            this.tabPage2.Controls.Add(this.lblFIO);
            this.tabPage2.Controls.Add(this.lblAdress);
            this.tabPage2.ImageIndex = 1;
            this.tabPage2.Location = new System.Drawing.Point(4, 39);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage2.Size = new System.Drawing.Size(654, 385);
            this.tabPage2.TabIndex = 2;
            this.tabPage2.Text = "Данные владельца сертификата";
            this.tabPage2.UseVisualStyleBackColor = true;
            // 
            // linkLabel1
            // 
            this.linkLabel1.AutoSize = true;
            this.linkLabel1.Location = new System.Drawing.Point(534, 13);
            this.linkLabel1.Name = "linkLabel1";
            this.linkLabel1.Size = new System.Drawing.Size(114, 13);
            this.linkLabel1.TabIndex = 56;
            this.linkLabel1.TabStop = true;
            this.linkLabel1.Text = "Сменить сертификат";
            this.linkLabel1.Click += new System.EventHandler(this.linkLabel1_Click);
            // 
            // dtP
            // 
            this.dtP.Location = new System.Drawing.Point(188, 89);
            this.dtP.Name = "dtP";
            this.dtP.Size = new System.Drawing.Size(240, 20);
            this.dtP.TabIndex = 55;
            // 
            // chkShow
            // 
            this.chkShow.AutoSize = true;
            this.chkShow.Location = new System.Drawing.Point(426, 329);
            this.chkShow.Name = "chkShow";
            this.chkShow.Size = new System.Drawing.Size(217, 17);
            this.chkShow.TabIndex = 19;
            this.chkShow.Text = "Показывать при следующем запуске";
            this.chkShow.UseVisualStyleBackColor = true;
            this.chkShow.CheckedChanged += new System.EventHandler(this.chkShow_CheckedChanged);
            // 
            // txtEmailPerson
            // 
            this.txtEmailPerson.Location = new System.Drawing.Point(12, 301);
            this.txtEmailPerson.Name = "txtEmailPerson";
            this.txtEmailPerson.Size = new System.Drawing.Size(630, 20);
            this.txtEmailPerson.TabIndex = 53;
            // 
            // lblEmailPerson
            // 
            this.lblEmailPerson.AutoSize = true;
            this.lblEmailPerson.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.lblEmailPerson.Location = new System.Drawing.Point(9, 285);
            this.lblEmailPerson.Name = "lblEmailPerson";
            this.lblEmailPerson.Size = new System.Drawing.Size(237, 13);
            this.lblEmailPerson.TabIndex = 54;
            this.lblEmailPerson.Text = "E-mail   пользователя  (для получения ответа)";
            // 
            // cbbRegion
            // 
            this.cbbRegion.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.cbbRegion.FormattingEnabled = true;
            this.cbbRegion.Location = new System.Drawing.Point(11, 175);
            this.cbbRegion.Name = "cbbRegion";
            this.cbbRegion.Size = new System.Drawing.Size(631, 24);
            this.cbbRegion.TabIndex = 52;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.label3.Location = new System.Drawing.Point(12, 159);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(43, 13);
            this.label3.TabIndex = 51;
            this.label3.Text = "Регион";
            // 
            // txtNumber
            // 
            this.txtNumber.Location = new System.Drawing.Point(542, 89);
            this.txtNumber.Mask = "000000";
            this.txtNumber.Name = "txtNumber";
            this.txtNumber.Size = new System.Drawing.Size(100, 20);
            this.txtNumber.TabIndex = 49;
            // 
            // txtSeria
            // 
            this.txtSeria.Location = new System.Drawing.Point(436, 89);
            this.txtSeria.Mask = "0000";
            this.txtSeria.Name = "txtSeria";
            this.txtSeria.Size = new System.Drawing.Size(96, 20);
            this.txtSeria.TabIndex = 48;
            // 
            // txtSnils
            // 
            this.txtSnils.Location = new System.Drawing.Point(12, 86);
            this.txtSnils.Mask = "000-000-000 00";
            this.txtSnils.Name = "txtSnils";
            this.txtSnils.Size = new System.Drawing.Size(100, 20);
            this.txtSnils.TabIndex = 47;
            // 
            // txtTel
            // 
            this.txtTel.Location = new System.Drawing.Point(12, 253);
            this.txtTel.Name = "txtTel";
            this.txtTel.Size = new System.Drawing.Size(631, 20);
            this.txtTel.TabIndex = 44;
            // 
            // lblTel
            // 
            this.lblTel.AutoSize = true;
            this.lblTel.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.lblTel.Location = new System.Drawing.Point(9, 237);
            this.lblTel.Name = "lblTel";
            this.lblTel.Size = new System.Drawing.Size(114, 13);
            this.lblTel.TabIndex = 45;
            this.lblTel.Text = "Контактный телефон";
            // 
            // txtAddress
            // 
            this.txtAddress.Location = new System.Drawing.Point(12, 214);
            this.txtAddress.Name = "txtAddress";
            this.txtAddress.Size = new System.Drawing.Size(631, 20);
            this.txtAddress.TabIndex = 42;
            // 
            // txtIssque
            // 
            this.txtIssque.Location = new System.Drawing.Point(11, 135);
            this.txtIssque.Name = "txtIssque";
            this.txtIssque.Size = new System.Drawing.Size(631, 20);
            this.txtIssque.TabIndex = 40;
            // 
            // lblIssque
            // 
            this.lblIssque.AutoSize = true;
            this.lblIssque.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.lblIssque.Location = new System.Drawing.Point(8, 119);
            this.lblIssque.Name = "lblIssque";
            this.lblIssque.Size = new System.Drawing.Size(63, 13);
            this.lblIssque.TabIndex = 41;
            this.lblIssque.Text = "Кем выдан";
            // 
            // lblNumber
            // 
            this.lblNumber.AutoSize = true;
            this.lblNumber.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.lblNumber.Location = new System.Drawing.Point(542, 73);
            this.lblNumber.Name = "lblNumber";
            this.lblNumber.Size = new System.Drawing.Size(41, 13);
            this.lblNumber.TabIndex = 39;
            this.lblNumber.Text = "Номер";
            // 
            // lblSeria
            // 
            this.lblSeria.AutoSize = true;
            this.lblSeria.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.lblSeria.Location = new System.Drawing.Point(433, 73);
            this.lblSeria.Name = "lblSeria";
            this.lblSeria.Size = new System.Drawing.Size(38, 13);
            this.lblSeria.TabIndex = 38;
            this.lblSeria.Text = "Серия";
            // 
            // lblData
            // 
            this.lblData.AutoSize = true;
            this.lblData.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.lblData.Location = new System.Drawing.Point(185, 73);
            this.lblData.Name = "lblData";
            this.lblData.Size = new System.Drawing.Size(133, 13);
            this.lblData.TabIndex = 37;
            this.lblData.Text = "Дата  выдачи документа";
            // 
            // lblSnils
            // 
            this.lblSnils.AutoSize = true;
            this.lblSnils.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.lblSnils.Location = new System.Drawing.Point(8, 73);
            this.lblSnils.Name = "lblSnils";
            this.lblSnils.Size = new System.Drawing.Size(38, 13);
            this.lblSnils.TabIndex = 36;
            this.lblSnils.Text = "Снилс";
            // 
            // txtSurName
            // 
            this.txtSurName.Location = new System.Drawing.Point(436, 47);
            this.txtSurName.Name = "txtSurName";
            this.txtSurName.Size = new System.Drawing.Size(207, 20);
            this.txtSurName.TabIndex = 34;
            // 
            // lblSurName
            // 
            this.lblSurName.AutoSize = true;
            this.lblSurName.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.lblSurName.Location = new System.Drawing.Point(436, 31);
            this.lblSurName.Name = "lblSurName";
            this.lblSurName.Size = new System.Drawing.Size(54, 13);
            this.lblSurName.TabIndex = 35;
            this.lblSurName.Text = "Отчество";
            // 
            // txtName
            // 
            this.txtName.Location = new System.Drawing.Point(188, 47);
            this.txtName.Name = "txtName";
            this.txtName.Size = new System.Drawing.Size(240, 20);
            this.txtName.TabIndex = 32;
            // 
            // lblName
            // 
            this.lblName.AutoSize = true;
            this.lblName.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.lblName.Location = new System.Drawing.Point(186, 31);
            this.lblName.Name = "lblName";
            this.lblName.Size = new System.Drawing.Size(35, 13);
            this.lblName.TabIndex = 33;
            this.lblName.Text = "Имя  ";
            // 
            // txtFIO
            // 
            this.txtFIO.Location = new System.Drawing.Point(11, 47);
            this.txtFIO.Name = "txtFIO";
            this.txtFIO.Size = new System.Drawing.Size(171, 20);
            this.txtFIO.TabIndex = 30;
            // 
            // lblFIO
            // 
            this.lblFIO.AutoSize = true;
            this.lblFIO.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.lblFIO.Location = new System.Drawing.Point(8, 31);
            this.lblFIO.Name = "lblFIO";
            this.lblFIO.Size = new System.Drawing.Size(59, 13);
            this.lblFIO.TabIndex = 31;
            this.lblFIO.Text = "Фамилия ";
            // 
            // lblAdress
            // 
            this.lblAdress.AutoSize = true;
            this.lblAdress.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.lblAdress.Location = new System.Drawing.Point(9, 198);
            this.lblAdress.Name = "lblAdress";
            this.lblAdress.Size = new System.Drawing.Size(38, 13);
            this.lblAdress.TabIndex = 43;
            this.lblAdress.Text = "Адрес";
            // 
            // tabPage1
            // 
            this.tabPage1.Controls.Add(this.textBoxzEmail);
            this.tabPage1.Controls.Add(this.label1);
            this.tabPage1.Controls.Add(this.groupBox2);
            this.tabPage1.Location = new System.Drawing.Point(4, 39);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage1.Size = new System.Drawing.Size(654, 385);
            this.tabPage1.TabIndex = 3;
            this.tabPage1.Text = "Данные заявителя КПТ";
            this.tabPage1.UseVisualStyleBackColor = true;
            // 
            // textBoxzEmail
            // 
            this.textBoxzEmail.Location = new System.Drawing.Point(10, 138);
            this.textBoxzEmail.Name = "textBoxzEmail";
            this.textBoxzEmail.Size = new System.Drawing.Size(580, 20);
            this.textBoxzEmail.TabIndex = 62;
            this.textBoxzEmail.TextChanged += new System.EventHandler(this.textBox1_TextChanged);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.label1.Location = new System.Drawing.Point(7, 122);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(237, 13);
            this.label1.TabIndex = 63;
            this.label1.Text = "E-mail   пользователя  (для получения ответа)";
            this.label1.Click += new System.EventHandler(this.label1_Click);
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.dateTimePickerzDate);
            this.groupBox2.Controls.Add(this.maskedTextBoxzNumber);
            this.groupBox2.Controls.Add(this.label2);
            this.groupBox2.Controls.Add(this.label4);
            this.groupBox2.Controls.Add(this.textBoxzName);
            this.groupBox2.Controls.Add(this.label5);
            this.groupBox2.Controls.Add(this.textBoxzFIO);
            this.groupBox2.Controls.Add(this.label6);
            this.groupBox2.Location = new System.Drawing.Point(8, 20);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(580, 97);
            this.groupBox2.TabIndex = 61;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Заявитель";
            // 
            // dateTimePickerzDate
            // 
            this.dateTimePickerzDate.Location = new System.Drawing.Point(15, 70);
            this.dateTimePickerzDate.Name = "dateTimePickerzDate";
            this.dateTimePickerzDate.Size = new System.Drawing.Size(240, 20);
            this.dateTimePickerzDate.TabIndex = 59;
            // 
            // maskedTextBoxzNumber
            // 
            this.maskedTextBoxzNumber.Location = new System.Drawing.Point(265, 70);
            this.maskedTextBoxzNumber.Mask = "000000";
            this.maskedTextBoxzNumber.Name = "maskedTextBoxzNumber";
            this.maskedTextBoxzNumber.Size = new System.Drawing.Size(239, 20);
            this.maskedTextBoxzNumber.TabIndex = 58;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.label2.Location = new System.Drawing.Point(265, 54);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(91, 13);
            this.label2.TabIndex = 57;
            this.label2.Text = "Номер паспорта";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.label4.Location = new System.Drawing.Point(12, 54);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(126, 13);
            this.label4.TabIndex = 56;
            this.label4.Text = "Дата  выдачи пасопрта";
            // 
            // textBoxzName
            // 
            this.textBoxzName.Location = new System.Drawing.Point(264, 32);
            this.textBoxzName.Name = "textBoxzName";
            this.textBoxzName.Size = new System.Drawing.Size(240, 20);
            this.textBoxzName.TabIndex = 36;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.label5.Location = new System.Drawing.Point(262, 16);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(35, 13);
            this.label5.TabIndex = 37;
            this.label5.Text = "Имя  ";
            // 
            // textBoxzFIO
            // 
            this.textBoxzFIO.Location = new System.Drawing.Point(15, 32);
            this.textBoxzFIO.Name = "textBoxzFIO";
            this.textBoxzFIO.Size = new System.Drawing.Size(240, 20);
            this.textBoxzFIO.TabIndex = 34;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.label6.Location = new System.Drawing.Point(12, 16);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(59, 13);
            this.label6.TabIndex = 35;
            this.label6.Text = "Фамилия ";
            // 
            // btnOk
            // 
            this.btnOk.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.btnOk.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.btnOk.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnOk.ForeColor = System.Drawing.Color.White;
            this.btnOk.Image = global::PortalRosreestr.Properties.Resources.fon3;
            this.btnOk.Location = new System.Drawing.Point(0, 391);
            this.btnOk.Name = "btnOk";
            this.btnOk.Size = new System.Drawing.Size(662, 37);
            this.btnOk.TabIndex = 18;
            this.btnOk.Text = "Сохранить";
            this.btnOk.UseVisualStyleBackColor = true;
            this.btnOk.Click += new System.EventHandler(this.btnOk_Click);
            // 
            // FrSetCertificat
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(662, 428);
            this.Controls.Add(this.btnOk);
            this.Controls.Add(this.tabCtrlRec);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "FrSetCertificat";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Shown += new System.EventHandler(this.FrSetCertificat_Shown);
            this.tabCtrlRec.ResumeLayout(false);
            this.tabPage2.ResumeLayout(false);
            this.tabPage2.PerformLayout();
            this.tabPage1.ResumeLayout(false);
            this.tabPage1.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TabControl tabCtrlRec;
        private System.Windows.Forms.TabPage tabPage2;
        private System.Windows.Forms.TextBox txtEmailPerson;
        private System.Windows.Forms.Label lblEmailPerson;
        private System.Windows.Forms.ComboBox cbbRegion;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.MaskedTextBox txtNumber;
        private System.Windows.Forms.MaskedTextBox txtSeria;
        private System.Windows.Forms.MaskedTextBox txtSnils;
        private System.Windows.Forms.TextBox txtTel;
        private System.Windows.Forms.Label lblTel;
        private System.Windows.Forms.TextBox txtAddress;
        private System.Windows.Forms.TextBox txtIssque;
        private System.Windows.Forms.Label lblIssque;
        private System.Windows.Forms.Label lblNumber;
        private System.Windows.Forms.Label lblSeria;
        private System.Windows.Forms.Label lblData;
        private System.Windows.Forms.Label lblSnils;
        private System.Windows.Forms.TextBox txtSurName;
        private System.Windows.Forms.Label lblSurName;
        private System.Windows.Forms.TextBox txtName;
        private System.Windows.Forms.Label lblName;
        private System.Windows.Forms.TextBox txtFIO;
        private System.Windows.Forms.Label lblFIO;
        private System.Windows.Forms.Label lblAdress;
        private System.Windows.Forms.Button btnOk;
        public System.Windows.Forms.CheckBox chkShow;
        private System.Windows.Forms.DateTimePicker dtP;
        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.TextBox textBoxzEmail;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.DateTimePicker dateTimePickerzDate;
        private System.Windows.Forms.MaskedTextBox maskedTextBoxzNumber;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox textBoxzName;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox textBoxzFIO;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.LinkLabel linkLabel1;
    }
}