﻿namespace PortalRosreestr
{
    partial class FrOutEvents
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.TreeNode treeNode1 = new System.Windows.Forms.TreeNode("Ваше заявление");
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FrOutEvents));
            this.oFDLGZIP1 = new System.Windows.Forms.OpenFileDialog();
            this.button1 = new System.Windows.Forms.Button();
            this.folderBrowserDialog1 = new System.Windows.Forms.FolderBrowserDialog();
            this.backgroundWorkerCREATEREQ = new System.ComponentModel.BackgroundWorker();
            this.toolStrip1 = new System.Windows.Forms.ToolStrip();
            this.toolStripLabelFias = new System.Windows.Forms.ToolStripLabel();
            this.button2 = new System.Windows.Forms.Button();
            this.button3 = new System.Windows.Forms.Button();
            this.treeView1 = new System.Windows.Forms.TreeView();
            this.button4 = new System.Windows.Forms.Button();
            this.button5 = new System.Windows.Forms.Button();
            this.panel1 = new System.Windows.Forms.Panel();
            this.button7 = new System.Windows.Forms.Button();
            this.txtGeneralInformation = new System.Windows.Forms.TextBox();
            this.tabControl2 = new System.Windows.Forms.TabControl();
            this.tb_Exist = new System.Windows.Forms.TabPage();
            this.cbbUpdate = new System.Windows.Forms.ComboBox();
            this.label9 = new System.Windows.Forms.Label();
            this.tb_Fix_Error = new System.Windows.Forms.TabPage();
            this.cbb_fix_error = new System.Windows.Forms.ComboBox();
            this.label3 = new System.Windows.Forms.Label();
            this.tbDop = new System.Windows.Forms.TabPage();
            this.grBoxDOP = new System.Windows.Forms.GroupBox();
            this.label4 = new System.Windows.Forms.Label();
            this.txtRequetsNumber = new System.Windows.Forms.TextBox();
            this.tbTexError = new System.Windows.Forms.TabPage();
            this.txtTechicalError = new System.Windows.Forms.TextBox();
            this.tb_Other = new System.Windows.Forms.TabPage();
            this.label10 = new System.Windows.Forms.Label();
            this.button6 = new System.Windows.Forms.Button();
            this.radioButton3 = new System.Windows.Forms.RadioButton();
            this.radioButton2 = new System.Windows.Forms.RadioButton();
            this.label7 = new System.Windows.Forms.Label();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.radioButton1 = new System.Windows.Forms.RadioButton();
            this.rbtnSignReq = new System.Windows.Forms.RadioButton();
            this.rbtnSignRG = new System.Windows.Forms.RadioButton();
            this.cbbRegion = new System.Windows.Forms.ComboBox();
            this.lblNamePack = new System.Windows.Forms.Label();
            this.textBoxNamePack = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.buttonOut = new System.Windows.Forms.Button();
            this.panel2 = new System.Windows.Forms.Panel();
            this.textBoxLog = new System.Windows.Forms.TextBox();
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tabPageOUT = new System.Windows.Forms.TabPage();
            this.tabPageLOAD = new System.Windows.Forms.TabPage();
            this.buttonloadKPT = new System.Windows.Forms.Button();
            this.textBoxLOGLOAD = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.textBoxListKPT = new System.Windows.Forms.TextBox();
            this.backgroundWorkerLoadKPT = new System.ComponentModel.BackgroundWorker();
            this.backgroundWorker1 = new System.ComponentModel.BackgroundWorker();
            this.externalWS2 = new PortalRosreestr.ru.rosreestr.portal.ExternalWS();
            this.externalWS1 = new PortalRosreestr.ru.rosreestr.portal.ExternalWS();
            this.dataGridViewTextBoxColumn1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ReqName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.reqNumber = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.tbNew = new System.Windows.Forms.TabPage();
            this.label2 = new System.Windows.Forms.Label();
            this.toolStrip1.SuspendLayout();
            this.panel1.SuspendLayout();
            this.tabControl2.SuspendLayout();
            this.tb_Exist.SuspendLayout();
            this.tb_Fix_Error.SuspendLayout();
            this.tbDop.SuspendLayout();
            this.grBoxDOP.SuspendLayout();
            this.tbTexError.SuspendLayout();
            this.tb_Other.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.panel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            this.tabControl1.SuspendLayout();
            this.tabPageOUT.SuspendLayout();
            this.tabPageLOAD.SuspendLayout();
            this.tbNew.SuspendLayout();
            this.SuspendLayout();
            // 
            // oFDLGZIP1
            // 
            this.oFDLGZIP1.FileName = "openFileDialog1";
            this.oFDLGZIP1.Title = "Выбор пакета";
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(12, 5);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(75, 23);
            this.button1.TabIndex = 28;
            this.button1.Text = "button1";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Visible = false;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // folderBrowserDialog1
            // 
            this.folderBrowserDialog1.RootFolder = System.Environment.SpecialFolder.MyComputer;
            // 
            // backgroundWorkerCREATEREQ
            // 
            this.backgroundWorkerCREATEREQ.WorkerReportsProgress = true;
            this.backgroundWorkerCREATEREQ.WorkerSupportsCancellation = true;
            this.backgroundWorkerCREATEREQ.DoWork += new System.ComponentModel.DoWorkEventHandler(this.backgroundWorkerCREATEREQ_DoWork);
            this.backgroundWorkerCREATEREQ.RunWorkerCompleted += new System.ComponentModel.RunWorkerCompletedEventHandler(this.backgroundWorkerCREATEREQ_RunWorkerCompleted);
            // 
            // toolStrip1
            // 
            this.toolStrip1.ImageScalingSize = new System.Drawing.Size(18, 18);
            this.toolStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripLabelFias});
            this.toolStrip1.Location = new System.Drawing.Point(0, 0);
            this.toolStrip1.Name = "toolStrip1";
            this.toolStrip1.Size = new System.Drawing.Size(605, 25);
            this.toolStrip1.TabIndex = 30;
            this.toolStrip1.Text = "toolStrip1";
            this.toolStrip1.Visible = false;
            // 
            // toolStripLabelFias
            // 
            this.toolStripLabelFias.AccessibleRole = System.Windows.Forms.AccessibleRole.MenuBar;
            this.toolStripLabelFias.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right;
            this.toolStripLabelFias.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.toolStripLabelFias.Font = new System.Drawing.Font("Segoe Script", 9F, System.Drawing.FontStyle.Italic);
            this.toolStripLabelFias.IsLink = true;
            this.toolStripLabelFias.LinkColor = System.Drawing.SystemColors.HotTrack;
            this.toolStripLabelFias.Name = "toolStripLabelFias";
            this.toolStripLabelFias.Overflow = System.Windows.Forms.ToolStripItemOverflow.Never;
            this.toolStripLabelFias.Size = new System.Drawing.Size(179, 22);
            this.toolStripLabelFias.Text = "Справочник регионов";
            // 
            // button2
            // 
            this.button2.Location = new System.Drawing.Point(93, 5);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(75, 23);
            this.button2.TabIndex = 37;
            this.button2.Text = "button2";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Visible = false;
            // 
            // button3
            // 
            this.button3.Location = new System.Drawing.Point(496, 211);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(75, 23);
            this.button3.TabIndex = 40;
            this.button3.Text = "Просмотреть заявление";
            this.button3.UseVisualStyleBackColor = true;
            this.button3.Visible = false;
            // 
            // treeView1
            // 
            this.treeView1.Location = new System.Drawing.Point(594, 12);
            this.treeView1.Name = "treeView1";
            treeNode1.Name = "Узел0";
            treeNode1.Text = "Ваше заявление";
            this.treeView1.Nodes.AddRange(new System.Windows.Forms.TreeNode[] {
            treeNode1});
            this.treeView1.Size = new System.Drawing.Size(296, 272);
            this.treeView1.TabIndex = 41;
            this.treeView1.Visible = false;
            // 
            // button4
            // 
            this.button4.Location = new System.Drawing.Point(44, 254);
            this.button4.Name = "button4";
            this.button4.Size = new System.Drawing.Size(174, 27);
            this.button4.TabIndex = 42;
            this.button4.Text = "button4";
            this.button4.UseVisualStyleBackColor = true;
            // 
            // button5
            // 
            this.button5.Location = new System.Drawing.Point(332, 35);
            this.button5.Name = "button5";
            this.button5.Size = new System.Drawing.Size(75, 23);
            this.button5.TabIndex = 54;
            this.button5.Text = "button5";
            this.button5.UseVisualStyleBackColor = true;
            this.button5.Visible = false;
            this.button5.Click += new System.EventHandler(this.button5_Click);
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.button7);
            this.panel1.Controls.Add(this.txtGeneralInformation);
            this.panel1.Controls.Add(this.tabControl2);
            this.panel1.Controls.Add(this.button6);
            this.panel1.Controls.Add(this.radioButton3);
            this.panel1.Controls.Add(this.radioButton2);
            this.panel1.Controls.Add(this.label7);
            this.panel1.Controls.Add(this.textBox1);
            this.panel1.Controls.Add(this.label6);
            this.panel1.Controls.Add(this.groupBox1);
            this.panel1.Controls.Add(this.cbbRegion);
            this.panel1.Controls.Add(this.lblNamePack);
            this.panel1.Controls.Add(this.textBoxNamePack);
            this.panel1.Controls.Add(this.label1);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel1.Location = new System.Drawing.Point(3, 3);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(591, 415);
            this.panel1.TabIndex = 55;
            // 
            // button7
            // 
            this.button7.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button7.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.button7.Location = new System.Drawing.Point(-7, 236);
            this.button7.Name = "button7";
            this.button7.Size = new System.Drawing.Size(601, 34);
            this.button7.TabIndex = 80;
            this.button7.Text = "ВЫБРАТЬ (Прикрепить) ФАЙЛЫ";
            this.button7.UseVisualStyleBackColor = true;
            this.button7.Click += new System.EventHandler(this.button7_Click_1);
            // 
            // txtGeneralInformation
            // 
            this.txtGeneralInformation.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.txtGeneralInformation.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.txtGeneralInformation.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(45)))), ((int)(((byte)(85)))), ((int)(((byte)(90)))));
            this.txtGeneralInformation.Location = new System.Drawing.Point(-3, 270);
            this.txtGeneralInformation.Multiline = true;
            this.txtGeneralInformation.Name = "txtGeneralInformation";
            this.txtGeneralInformation.ReadOnly = true;
            this.txtGeneralInformation.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.txtGeneralInformation.Size = new System.Drawing.Size(597, 69);
            this.txtGeneralInformation.TabIndex = 0;
            this.txtGeneralInformation.Text = "Список  прикрепленных файлов!";
            // 
            // tabControl2
            // 
            this.tabControl2.AccessibleRole = System.Windows.Forms.AccessibleRole.ScrollBar;
            this.tabControl2.AllowDrop = true;
            this.tabControl2.Appearance = System.Windows.Forms.TabAppearance.FlatButtons;
            this.tabControl2.Controls.Add(this.tbNew);
            this.tabControl2.Controls.Add(this.tb_Exist);
            this.tabControl2.Controls.Add(this.tb_Fix_Error);
            this.tabControl2.Controls.Add(this.tbDop);
            this.tabControl2.Controls.Add(this.tbTexError);
            this.tabControl2.Controls.Add(this.tb_Other);
            this.tabControl2.Cursor = System.Windows.Forms.Cursors.Hand;
            this.tabControl2.Font = new System.Drawing.Font("Times New Roman", 10.1F, System.Drawing.FontStyle.Underline, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.tabControl2.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.tabControl2.ItemSize = new System.Drawing.Size(105, 28);
            this.tabControl2.Location = new System.Drawing.Point(5, 104);
            this.tabControl2.Name = "tabControl2";
            this.tabControl2.SelectedIndex = 0;
            this.tabControl2.ShowToolTips = true;
            this.tabControl2.Size = new System.Drawing.Size(583, 134);
            this.tabControl2.TabIndex = 67;
            this.tabControl2.Selected += new System.Windows.Forms.TabControlEventHandler(this.tabControl2_Selected);
            // 
            // tb_Exist
            // 
            this.tb_Exist.BackColor = System.Drawing.SystemColors.Window;
            this.tb_Exist.Controls.Add(this.cbbUpdate);
            this.tb_Exist.Controls.Add(this.label9);
            this.tb_Exist.Location = new System.Drawing.Point(4, 32);
            this.tb_Exist.Name = "tb_Exist";
            this.tb_Exist.Padding = new System.Windows.Forms.Padding(3);
            this.tb_Exist.Size = new System.Drawing.Size(575, 98);
            this.tb_Exist.TabIndex = 1;
            this.tb_Exist.Tag = "558101030000";
            this.tb_Exist.Text = "ГУ изменений";
            this.tb_Exist.UseVisualStyleBackColor = true;
            // 
            // cbbUpdate
            // 
            this.cbbUpdate.AllowDrop = true;
            this.cbbUpdate.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbbUpdate.DropDownWidth = 650;
            this.cbbUpdate.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.cbbUpdate.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.cbbUpdate.ImeMode = System.Windows.Forms.ImeMode.On;
            this.cbbUpdate.ItemHeight = 20;
            this.cbbUpdate.Location = new System.Drawing.Point(6, 29);
            this.cbbUpdate.MaxDropDownItems = 20;
            this.cbbUpdate.Name = "cbbUpdate";
            this.cbbUpdate.Size = new System.Drawing.Size(550, 28);
            this.cbbUpdate.TabIndex = 81;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.label9.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label9.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(45)))), ((int)(((byte)(85)))), ((int)(((byte)(90)))));
            this.label9.Location = new System.Drawing.Point(9, 6);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(141, 17);
            this.label9.TabIndex = 80;
            this.label9.Text = "Перчень изменений";
            // 
            // tb_Fix_Error
            // 
            this.tb_Fix_Error.Controls.Add(this.cbb_fix_error);
            this.tb_Fix_Error.Controls.Add(this.label3);
            this.tb_Fix_Error.Location = new System.Drawing.Point(4, 32);
            this.tb_Fix_Error.Name = "tb_Fix_Error";
            this.tb_Fix_Error.Size = new System.Drawing.Size(575, 98);
            this.tb_Fix_Error.TabIndex = 4;
            this.tb_Fix_Error.Tag = "558101030000";
            this.tb_Fix_Error.Text = "Исправление кад. ошибки";
            this.tb_Fix_Error.ToolTipText = "Исправление кадастровой ошибки в сведениях государственного   кадастра недвижимос" +
    "ти ";
            this.tb_Fix_Error.UseVisualStyleBackColor = true;
            // 
            // cbb_fix_error
            // 
            this.cbb_fix_error.AllowDrop = true;
            this.cbb_fix_error.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbb_fix_error.DropDownWidth = 650;
            this.cbb_fix_error.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.cbb_fix_error.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.cbb_fix_error.ImeMode = System.Windows.Forms.ImeMode.On;
            this.cbb_fix_error.ItemHeight = 20;
            this.cbb_fix_error.Location = new System.Drawing.Point(4, 30);
            this.cbb_fix_error.MaxDropDownItems = 20;
            this.cbb_fix_error.Name = "cbb_fix_error";
            this.cbb_fix_error.Size = new System.Drawing.Size(550, 28);
            this.cbb_fix_error.TabIndex = 83;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label3.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(45)))), ((int)(((byte)(85)))), ((int)(((byte)(90)))));
            this.label3.Location = new System.Drawing.Point(7, 7);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(141, 17);
            this.label3.TabIndex = 82;
            this.label3.Text = "Перчень изменений";
            // 
            // tbDop
            // 
            this.tbDop.Controls.Add(this.grBoxDOP);
            this.tbDop.Location = new System.Drawing.Point(4, 32);
            this.tbDop.Name = "tbDop";
            this.tbDop.Padding = new System.Windows.Forms.Padding(3);
            this.tbDop.Size = new System.Drawing.Size(575, 98);
            this.tbDop.TabIndex = 2;
            this.tbDop.Tag = "558101100000";
            this.tbDop.Text = "Доп. документы (*.zip или *.pdf)";
            this.tbDop.UseVisualStyleBackColor = true;
            // 
            // grBoxDOP
            // 
            this.grBoxDOP.Controls.Add(this.label4);
            this.grBoxDOP.Controls.Add(this.txtRequetsNumber);
            this.grBoxDOP.Dock = System.Windows.Forms.DockStyle.Top;
            this.grBoxDOP.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.grBoxDOP.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F);
            this.grBoxDOP.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(45)))), ((int)(((byte)(85)))), ((int)(((byte)(90)))));
            this.grBoxDOP.Location = new System.Drawing.Point(3, 3);
            this.grBoxDOP.Name = "grBoxDOP";
            this.grBoxDOP.Size = new System.Drawing.Size(569, 60);
            this.grBoxDOP.TabIndex = 67;
            this.grBoxDOP.TabStop = false;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(3, 2);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(100, 17);
            this.label4.TabIndex = 1;
            this.label4.Text = "Номер заявки";
            // 
            // txtRequetsNumber
            // 
            this.txtRequetsNumber.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.txtRequetsNumber.Location = new System.Drawing.Point(5, 29);
            this.txtRequetsNumber.MaximumSize = new System.Drawing.Size(575, 27);
            this.txtRequetsNumber.MinimumSize = new System.Drawing.Size(575, 27);
            this.txtRequetsNumber.Name = "txtRequetsNumber";
            this.txtRequetsNumber.Size = new System.Drawing.Size(575, 23);
            this.txtRequetsNumber.TabIndex = 0;
            // 
            // tbTexError
            // 
            this.tbTexError.Controls.Add(this.txtTechicalError);
            this.tbTexError.Location = new System.Drawing.Point(4, 32);
            this.tbTexError.Name = "tbTexError";
            this.tbTexError.Size = new System.Drawing.Size(575, 98);
            this.tbTexError.TabIndex = 5;
            this.tbTexError.Tag = "558101090100";
            this.tbTexError.Text = "Испр. тех. ошибки";
            this.tbTexError.ToolTipText = "Заявление об исправлении технической ошибки";
            this.tbTexError.UseVisualStyleBackColor = true;
            // 
            // txtTechicalError
            // 
            this.txtTechicalError.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.txtTechicalError.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.txtTechicalError.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(45)))), ((int)(((byte)(85)))), ((int)(((byte)(90)))));
            this.txtTechicalError.Location = new System.Drawing.Point(4, 4);
            this.txtTechicalError.Multiline = true;
            this.txtTechicalError.Name = "txtTechicalError";
            this.txtTechicalError.ReadOnly = true;
            this.txtTechicalError.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.txtTechicalError.Size = new System.Drawing.Size(568, 69);
            this.txtTechicalError.TabIndex = 1;
            this.txtTechicalError.Text = "Два ввода данных дважды кликните мышкой";
            this.txtTechicalError.DoubleClick += new System.EventHandler(this.txtTechicalError_DoubleClick);
            // 
            // tb_Other
            // 
            this.tb_Other.Controls.Add(this.label10);
            this.tb_Other.Location = new System.Drawing.Point(4, 32);
            this.tb_Other.Name = "tb_Other";
            this.tb_Other.Padding = new System.Windows.Forms.Padding(3);
            this.tb_Other.Size = new System.Drawing.Size(575, 98);
            this.tb_Other.TabIndex = 3;
            this.tb_Other.Tag = "558199000000";
            this.tb_Other.Text = "Иное  заявление";
            this.tb_Other.UseVisualStyleBackColor = true;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label10.Location = new System.Drawing.Point(157, 25);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(371, 16);
            this.label10.TabIndex = 1;
            this.label10.Text = "*. только  вместе с формированным файлом заявления!";
            // 
            // button6
            // 
            this.button6.Location = new System.Drawing.Point(366, 319);
            this.button6.Name = "button6";
            this.button6.Size = new System.Drawing.Size(208, 27);
            this.button6.TabIndex = 68;
            this.button6.Text = "Подписать МП или ТП";
            this.button6.UseVisualStyleBackColor = true;
            this.button6.Visible = false;
            this.button6.Click += new System.EventHandler(this.button6_Click);
            // 
            // radioButton3
            // 
            this.radioButton3.AutoSize = true;
            this.radioButton3.Location = new System.Drawing.Point(19, 297);
            this.radioButton3.Name = "radioButton3";
            this.radioButton3.Size = new System.Drawing.Size(14, 13);
            this.radioButton3.TabIndex = 77;
            this.radioButton3.TabStop = true;
            this.radioButton3.UseVisualStyleBackColor = true;
            this.radioButton3.Visible = false;
            // 
            // radioButton2
            // 
            this.radioButton2.AutoSize = true;
            this.radioButton2.Location = new System.Drawing.Point(266, 319);
            this.radioButton2.Name = "radioButton2";
            this.radioButton2.Size = new System.Drawing.Size(14, 13);
            this.radioButton2.TabIndex = 76;
            this.radioButton2.TabStop = true;
            this.radioButton2.UseVisualStyleBackColor = true;
            this.radioButton2.Visible = false;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.label7.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label7.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(45)))), ((int)(((byte)(85)))), ((int)(((byte)(90)))));
            this.label7.Location = new System.Drawing.Point(39, 295);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(93, 17);
            this.label7.TabIndex = 75;
            this.label7.Text = "Директория ";
            // 
            // textBox1
            // 
            this.textBox1.AutoCompleteCustomSource.AddRange(new string[] {
            "выбор файла двойным  нажатием"});
            this.textBox1.Location = new System.Drawing.Point(22, 315);
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new System.Drawing.Size(238, 20);
            this.textBox1.TabIndex = 74;
            this.textBox1.Text = "выбор файла двойным  нажатием\r\n";
            this.textBox1.Visible = false;
            this.textBox1.Click += new System.EventHandler(this.textBox1_Click);
            this.textBox1.TextChanged += new System.EventHandler(this.textBox1_TextChanged);
            this.textBox1.DoubleClick += new System.EventHandler(this.textBox1_DoubleClick);
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(11, 318);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(12, 13);
            this.label6.TabIndex = 73;
            this.label6.Text = "/";
            this.label6.Visible = false;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.radioButton1);
            this.groupBox1.Controls.Add(this.button5);
            this.groupBox1.Controls.Add(this.rbtnSignReq);
            this.groupBox1.Controls.Add(this.rbtnSignRG);
            this.groupBox1.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.groupBox1.Location = new System.Drawing.Point(16, 341);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(567, 58);
            this.groupBox1.TabIndex = 72;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Подписать (*.sig)";
            this.groupBox1.Enter += new System.EventHandler(this.groupBox1_Enter);
            // 
            // radioButton1
            // 
            this.radioButton1.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.radioButton1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))));
            this.radioButton1.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(45)))), ((int)(((byte)(85)))), ((int)(((byte)(90)))));
            this.radioButton1.Location = new System.Drawing.Point(332, 19);
            this.radioButton1.Name = "radioButton1";
            this.radioButton1.Size = new System.Drawing.Size(240, 39);
            this.radioButton1.TabIndex = 72;
            this.radioButton1.Text = "не подписывать (заявление уже было подписано)";
            this.radioButton1.UseVisualStyleBackColor = true;
            // 
            // rbtnSignReq
            // 
            this.rbtnSignReq.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.rbtnSignReq.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))));
            this.rbtnSignReq.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(45)))), ((int)(((byte)(85)))), ((int)(((byte)(90)))));
            this.rbtnSignReq.Location = new System.Drawing.Point(188, 19);
            this.rbtnSignReq.Name = "rbtnSignReq";
            this.rbtnSignReq.Size = new System.Drawing.Size(131, 39);
            this.rbtnSignReq.TabIndex = 71;
            this.rbtnSignReq.Text = "только заявление";
            this.rbtnSignReq.UseVisualStyleBackColor = true;
            // 
            // rbtnSignRG
            // 
            this.rbtnSignRG.Checked = true;
            this.rbtnSignRG.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.rbtnSignRG.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))));
            this.rbtnSignRG.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(45)))), ((int)(((byte)(85)))), ((int)(((byte)(90)))));
            this.rbtnSignRG.Location = new System.Drawing.Point(26, 17);
            this.rbtnSignRG.Name = "rbtnSignRG";
            this.rbtnSignRG.Size = new System.Drawing.Size(130, 41);
            this.rbtnSignRG.TabIndex = 70;
            this.rbtnSignRG.TabStop = true;
            this.rbtnSignRG.Text = "полностью";
            this.rbtnSignRG.UseVisualStyleBackColor = true;
            this.rbtnSignRG.CheckedChanged += new System.EventHandler(this.rbtnSignRG_CheckedChanged);
            // 
            // cbbRegion
            // 
            this.cbbRegion.Cursor = System.Windows.Forms.Cursors.Hand;
            this.cbbRegion.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbbRegion.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cbbRegion.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.cbbRegion.FormattingEnabled = true;
            this.cbbRegion.Location = new System.Drawing.Point(7, 68);
            this.cbbRegion.Name = "cbbRegion";
            this.cbbRegion.Size = new System.Drawing.Size(583, 26);
            this.cbbRegion.TabIndex = 66;
            // 
            // lblNamePack
            // 
            this.lblNamePack.AutoSize = true;
            this.lblNamePack.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.lblNamePack.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.lblNamePack.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(45)))), ((int)(((byte)(85)))), ((int)(((byte)(90)))));
            this.lblNamePack.Location = new System.Drawing.Point(10, 4);
            this.lblNamePack.Name = "lblNamePack";
            this.lblNamePack.Size = new System.Drawing.Size(139, 17);
            this.lblNamePack.TabIndex = 64;
            this.lblNamePack.Text = "Описание пакета";
            // 
            // textBoxNamePack
            // 
            this.textBoxNamePack.Location = new System.Drawing.Point(5, 19);
            this.textBoxNamePack.MaximumSize = new System.Drawing.Size(550, 35);
            this.textBoxNamePack.MinimumSize = new System.Drawing.Size(580, 27);
            this.textBoxNamePack.Name = "textBoxNamePack";
            this.textBoxNamePack.Size = new System.Drawing.Size(580, 27);
            this.textBoxNamePack.TabIndex = 63;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label1.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(45)))), ((int)(((byte)(85)))), ((int)(((byte)(90)))));
            this.label1.Location = new System.Drawing.Point(8, 49);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(143, 17);
            this.label1.TabIndex = 56;
            this.label1.Text = "Региона отправки";
            // 
            // buttonOut
            // 
            this.buttonOut.Cursor = System.Windows.Forms.Cursors.Hand;
            this.buttonOut.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonOut.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.buttonOut.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.buttonOut.Image = global::PortalRosreestr.Properties.Resources.fon3;
            this.buttonOut.Location = new System.Drawing.Point(195, 3);
            this.buttonOut.Name = "buttonOut";
            this.buttonOut.Size = new System.Drawing.Size(397, 33);
            this.buttonOut.TabIndex = 65;
            this.buttonOut.Text = "Отправить пакет";
            this.buttonOut.UseVisualStyleBackColor = true;
            this.buttonOut.Click += new System.EventHandler(this.btnOutPortalZIP_Click);
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.buttonOut);
            this.panel2.Controls.Add(this.textBoxLog);
            this.panel2.Controls.Add(this.dataGridView1);
            this.panel2.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panel2.Location = new System.Drawing.Point(3, 418);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(591, 163);
            this.panel2.TabIndex = 56;
            // 
            // textBoxLog
            // 
            this.textBoxLog.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.textBoxLog.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.textBoxLog.Location = new System.Drawing.Point(0, 38);
            this.textBoxLog.Multiline = true;
            this.textBoxLog.Name = "textBoxLog";
            this.textBoxLog.Size = new System.Drawing.Size(591, 31);
            this.textBoxLog.TabIndex = 40;
            this.textBoxLog.Text = "Результат работы:";
            // 
            // dataGridView1
            // 
            this.dataGridView1.AllowUserToAddRows = false;
            this.dataGridView1.AllowUserToOrderColumns = true;
            this.dataGridView1.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.None;
            this.dataGridView1.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.Single;
            this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView1.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.ReqName,
            this.reqNumber});
            this.dataGridView1.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.dataGridView1.EnableHeadersVisualStyles = false;
            this.dataGridView1.Location = new System.Drawing.Point(0, 69);
            this.dataGridView1.Name = "dataGridView1";
            this.dataGridView1.RowHeadersWidth = 100;
            this.dataGridView1.RowTemplate.Height = 25;
            this.dataGridView1.RowTemplate.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.dataGridView1.Size = new System.Drawing.Size(591, 94);
            this.dataGridView1.StandardTab = true;
            this.dataGridView1.TabIndex = 39;
            // 
            // tabControl1
            // 
            this.tabControl1.Controls.Add(this.tabPageOUT);
            this.tabControl1.Controls.Add(this.tabPageLOAD);
            this.tabControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tabControl1.Location = new System.Drawing.Point(0, 0);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(605, 610);
            this.tabControl1.TabIndex = 66;
            // 
            // tabPageOUT
            // 
            this.tabPageOUT.Controls.Add(this.panel1);
            this.tabPageOUT.Controls.Add(this.panel2);
            this.tabPageOUT.Location = new System.Drawing.Point(4, 22);
            this.tabPageOUT.Name = "tabPageOUT";
            this.tabPageOUT.Padding = new System.Windows.Forms.Padding(3);
            this.tabPageOUT.Size = new System.Drawing.Size(597, 584);
            this.tabPageOUT.TabIndex = 0;
            this.tabPageOUT.Text = "Отправить";
            this.tabPageOUT.UseVisualStyleBackColor = true;
            // 
            // tabPageLOAD
            // 
            this.tabPageLOAD.Controls.Add(this.buttonloadKPT);
            this.tabPageLOAD.Controls.Add(this.textBoxLOGLOAD);
            this.tabPageLOAD.Controls.Add(this.label5);
            this.tabPageLOAD.Controls.Add(this.textBoxListKPT);
            this.tabPageLOAD.Location = new System.Drawing.Point(4, 22);
            this.tabPageLOAD.Name = "tabPageLOAD";
            this.tabPageLOAD.Padding = new System.Windows.Forms.Padding(3);
            this.tabPageLOAD.Size = new System.Drawing.Size(597, 584);
            this.tabPageLOAD.TabIndex = 1;
            this.tabPageLOAD.Text = "Заказать";
            this.tabPageLOAD.UseVisualStyleBackColor = true;
            // 
            // buttonloadKPT
            // 
            this.buttonloadKPT.Cursor = System.Windows.Forms.Cursors.Hand;
            this.buttonloadKPT.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonloadKPT.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.buttonloadKPT.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.buttonloadKPT.Image = global::PortalRosreestr.Properties.Resources.fon3;
            this.buttonloadKPT.Location = new System.Drawing.Point(334, 440);
            this.buttonloadKPT.Name = "buttonloadKPT";
            this.buttonloadKPT.Size = new System.Drawing.Size(266, 33);
            this.buttonloadKPT.TabIndex = 66;
            this.buttonloadKPT.Text = "Заказать КПТ";
            this.buttonloadKPT.UseVisualStyleBackColor = true;
            this.buttonloadKPT.Click += new System.EventHandler(this.button7_Click);
            // 
            // textBoxLOGLOAD
            // 
            this.textBoxLOGLOAD.Location = new System.Drawing.Point(6, 442);
            this.textBoxLOGLOAD.Multiline = true;
            this.textBoxLOGLOAD.Name = "textBoxLOGLOAD";
            this.textBoxLOGLOAD.ScrollBars = System.Windows.Forms.ScrollBars.Horizontal;
            this.textBoxLOGLOAD.Size = new System.Drawing.Size(322, 31);
            this.textBoxLOGLOAD.TabIndex = 63;
            this.textBoxLOGLOAD.Text = "LOG";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.label5.Location = new System.Drawing.Point(8, 8);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(69, 13);
            this.label5.TabIndex = 62;
            this.label5.Text = "Список КПТ";
            // 
            // textBoxListKPT
            // 
            this.textBoxListKPT.Cursor = System.Windows.Forms.Cursors.Default;
            this.textBoxListKPT.Location = new System.Drawing.Point(8, 24);
            this.textBoxListKPT.Multiline = true;
            this.textBoxListKPT.Name = "textBoxListKPT";
            this.textBoxListKPT.ScrollBars = System.Windows.Forms.ScrollBars.Horizontal;
            this.textBoxListKPT.Size = new System.Drawing.Size(582, 408);
            this.textBoxListKPT.TabIndex = 61;
            this.textBoxListKPT.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // backgroundWorkerLoadKPT
            // 
            this.backgroundWorkerLoadKPT.WorkerReportsProgress = true;
            this.backgroundWorkerLoadKPT.WorkerSupportsCancellation = true;
            this.backgroundWorkerLoadKPT.DoWork += new System.ComponentModel.DoWorkEventHandler(this.backgroundWorkerLoadKPT_DoWork);
            this.backgroundWorkerLoadKPT.RunWorkerCompleted += new System.ComponentModel.RunWorkerCompletedEventHandler(this.backgroundWorkerLoadKPT_RunWorkerCompleted);
            // 
            // backgroundWorker1
            // 
            this.backgroundWorker1.WorkerReportsProgress = true;
            this.backgroundWorker1.WorkerSupportsCancellation = true;
            this.backgroundWorker1.DoWork += new System.ComponentModel.DoWorkEventHandler(this.backgroundWorker1_DoWork);
            this.backgroundWorker1.RunWorkerCompleted += new System.ComponentModel.RunWorkerCompletedEventHandler(this.backgroundWorker1_RunWorkerCompleted);
            // 
            // externalWS2
            // 
            this.externalWS2.AllowAutoRedirect = true;
            this.externalWS2.EnableDecompression = true;
            this.externalWS2.PreAuthenticate = true;
            this.externalWS2.Timeout = 200000;
            this.externalWS2.Url = "https://portal.rosreestr.ru:4433/cxf/External";
            this.externalWS2.UseDefaultCredentials = true;
            // 
            // externalWS1
            // 
            this.externalWS1.AllowAutoRedirect = true;
            this.externalWS1.EnableDecompression = true;
            this.externalWS1.PreAuthenticate = true;
            this.externalWS1.Timeout = 200000;
            this.externalWS1.Url = "https://portal.rosreestr.ru:4433/cxf/External";
            this.externalWS1.UseDefaultCredentials = true;
            // 
            // dataGridViewTextBoxColumn1
            // 
            this.dataGridViewTextBoxColumn1.FillWeight = 250F;
            this.dataGridViewTextBoxColumn1.HeaderText = "Наименование пакета";
            this.dataGridViewTextBoxColumn1.Name = "dataGridViewTextBoxColumn1";
            this.dataGridViewTextBoxColumn1.Width = 300;
            // 
            // dataGridViewTextBoxColumn2
            // 
            this.dataGridViewTextBoxColumn2.HeaderText = "Номер заявки";
            this.dataGridViewTextBoxColumn2.Name = "dataGridViewTextBoxColumn2";
            this.dataGridViewTextBoxColumn2.Width = 150;
            // 
            // ReqName
            // 
            this.ReqName.FillWeight = 250F;
            this.ReqName.HeaderText = "Наименование пакета";
            this.ReqName.Name = "ReqName";
            this.ReqName.Width = 300;
            // 
            // reqNumber
            // 
            this.reqNumber.HeaderText = "Номер заявки";
            this.reqNumber.Name = "reqNumber";
            this.reqNumber.Width = 150;
            // 
            // tbNew
            // 
            this.tbNew.BackColor = System.Drawing.SystemColors.Window;
            this.tbNew.Controls.Add(this.label2);
            this.tbNew.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.tbNew.Location = new System.Drawing.Point(4, 32);
            this.tbNew.Name = "tbNew";
            this.tbNew.Padding = new System.Windows.Forms.Padding(3);
            this.tbNew.Size = new System.Drawing.Size(575, 98);
            this.tbNew.TabIndex = 0;
            this.tbNew.Tag = "558101020000";
            this.tbNew.Text = "Постановке на ГУ";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label2.Location = new System.Drawing.Point(195, 41);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(184, 16);
            this.label2.TabIndex = 1;
            this.label2.Text = "техплан или межевой план";
            this.label2.Visible = false;
            // 
            // FrOutEvents
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(605, 610);
            this.Controls.Add(this.tabControl1);
            this.Controls.Add(this.button4);
            this.Controls.Add(this.treeView1);
            this.Controls.Add(this.button3);
            this.Controls.Add(this.button2);
            this.Controls.Add(this.toolStrip1);
            this.Controls.Add(this.button1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.Name = "FrOutEvents";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Формируем заявление";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.OutEvents_FormClosing);
            this.Load += new System.EventHandler(this.OutEvents_Load);
            this.toolStrip1.ResumeLayout(false);
            this.toolStrip1.PerformLayout();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.tabControl2.ResumeLayout(false);
            this.tb_Exist.ResumeLayout(false);
            this.tb_Exist.PerformLayout();
            this.tb_Fix_Error.ResumeLayout(false);
            this.tb_Fix_Error.PerformLayout();
            this.tbDop.ResumeLayout(false);
            this.grBoxDOP.ResumeLayout(false);
            this.grBoxDOP.PerformLayout();
            this.tbTexError.ResumeLayout(false);
            this.tbTexError.PerformLayout();
            this.tb_Other.ResumeLayout(false);
            this.tb_Other.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            this.tabControl1.ResumeLayout(false);
            this.tabPageOUT.ResumeLayout(false);
            this.tabPageLOAD.ResumeLayout(false);
            this.tabPageLOAD.PerformLayout();
            this.tbNew.ResumeLayout(false);
            this.tbNew.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.OpenFileDialog oFDLGZIP1;
        private System.Windows.Forms.Button button1;
        public System.Windows.Forms.FolderBrowserDialog folderBrowserDialog1;
        private System.ComponentModel.BackgroundWorker backgroundWorkerCREATEREQ;
        private System.Windows.Forms.ToolStrip toolStrip1;
        private System.Windows.Forms.ToolStripLabel toolStripLabelFias;
        private System.Windows.Forms.Button button2;
        public ru.rosreestr.portal.ExternalWS externalWS2;
        private System.Windows.Forms.Button button3;
        private System.Windows.Forms.TreeView treeView1;
        private System.Windows.Forms.Button button4;
        private System.Windows.Forms.Button button5;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Button buttonOut;
        private System.Windows.Forms.Label lblNamePack;
        private System.Windows.Forms.TextBox textBoxNamePack;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.TextBox textBoxLog;
        private System.Windows.Forms.DataGridView dataGridView1;
        private System.Windows.Forms.DataGridViewTextBoxColumn ReqName;
        private System.Windows.Forms.DataGridViewTextBoxColumn reqNumber;
        private System.Windows.Forms.Label label4;
        public System.Windows.Forms.TextBox txtRequetsNumber;
        public System.Windows.Forms.GroupBox grBoxDOP;
        private System.Windows.Forms.Button button6;
        private System.Windows.Forms.RadioButton rbtnSignReq;
        private System.Windows.Forms.RadioButton rbtnSignRG;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.RadioButton radioButton1;
        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage tabPageOUT;
        private System.Windows.Forms.TabPage tabPageLOAD;
        private System.Windows.Forms.TextBox textBoxListKPT;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox textBoxLOGLOAD;
        private System.ComponentModel.BackgroundWorker backgroundWorkerLoadKPT;
        private System.Windows.Forms.Button buttonloadKPT;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox textBox1;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.RadioButton radioButton2;
        private System.Windows.Forms.RadioButton radioButton3;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.TabPage tbDop;
        private System.Windows.Forms.TabPage tb_Other;
        public System.Windows.Forms.ComboBox cbbRegion;
        private System.Windows.Forms.Label label10;
        public System.Windows.Forms.ComboBox cbbUpdate;
        public System.Windows.Forms.TabControl tabControl2;
        public System.Windows.Forms.TabPage tb_Exist;
        public ru.rosreestr.portal.ExternalWS externalWS1;
        private System.ComponentModel.BackgroundWorker backgroundWorker1;
        private System.Windows.Forms.Button button7;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn1;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn2;
        public System.Windows.Forms.TextBox txtGeneralInformation;
        public System.Windows.Forms.ComboBox cbb_fix_error;
        private System.Windows.Forms.Label label3;
        public System.Windows.Forms.TabPage tb_Fix_Error;
        public System.Windows.Forms.TextBox txtTechicalError;
        public System.Windows.Forms.TabPage tbTexError;
        private System.Windows.Forms.TabPage tbNew;
        private System.Windows.Forms.Label label2;
    }
}