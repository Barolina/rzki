﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.IO;
using Ionic.Zip;

using System.Xml;
using System.Xml.XPath;
using System.Xml.Xsl;
using PortalRosreestr.ru.rosreestr.portal;
using System.Threading;
using System.Xml.Linq;
using SQLiteHelperTestApp;
using WindowsFormsDataGrid;



namespace PortalRosreestr
{
    
    public partial class FrOutEvents : Form
    {
        Dictionary<string, object> dic = new Dictionary<string, object>();///хранение временных записей - используется для вставки в БД (insert update)
        List<KeyValuePair<string, string>> okatos = null;
        LoadKPT loadKPT;
        public FmListAppledFiles fmPack;
        public FrTechnicalError frTechicalError;
        List<string> lstPDf = new List<string>();
        string lstNumbers = "";
        string lstDate = "";

        /// <summary>
        ///   ОСНОВНЫЕ ЭЛЕМЕНТЫ УЧАСТВУЮЩИЕ В ОТПРАВКЕ НА ПОРТАЛ
        /// </summary>
        public string path_MP_TP_Req = "";
        public string dovPDF = "";
        public int countDopPDf = 0;

        
        public FrOutEvents()
        {
            InitializeComponent();
            loadKPT = new LoadKPT();
            fmPack = new FmListAppledFiles();
            frTechicalError = new FrTechnicalError();
        }

      /* public Form1 FmPack
        {
            get
            {
                return this.fmPack;
            }
        }*/

        #region Отправка пакета на СМЭВ - create req - СМЭВ 
        /// <summary>
        /// Отправка пакета СМЭВ
        /// </summary>
        /// <param name="fileName"></param>
        /// <returns></returns>
        private string _SendEvent(string fileName)
        {
            try
            {
                ThreadExceptionHandler.logger.Info("_SendEvent перед подписыванием ");
               
                if (this.rbtnSignRG.Checked) { if (!SignPackage(fileName)) return null; }
                else if (this.rbtnSignReq.Checked) { if (!SignReq(fileName)) return null; };

                this.buttonOut.Invoke((MethodInvoker)delegate
                {
                    this.buttonOut.Text = Consts.MSGWAITOUT;
                });
            //    if (MessageBox.Show("Проверте пакет перед отправкой (временно!) " + oFDLGZIP.InitialDirectory + fileName, " ", MessageBoxButtons.OKCancel) == System.Windows.Forms.DialogResult.OK)
            //    {
              
                createRequestIn createReq;
                createReq = new createRequestIn();

                this.cbbRegion.Invoke((MethodInvoker)delegate
                {
                    createReq.region = this.cbbRegion.SelectedValue.ToString();
                });
                createReq.okato = OkatoByKodeRegion(createReq.region).Trim();
                this.tabControl2.Invoke((MethodInvoker)delegate
                {
                    this.tabControl2.SelectedTab.Invoke((MethodInvoker)delegate
                    {
                        createReq.requestType = tabControl2.SelectedTab.Tag.ToString();
                    });
                });
            

                FileStream fs = new FileStream(fileName, FileMode.Open, FileAccess.Read);
                byte[] readBuf = new byte[fs.Length];
                fs.Read(readBuf, 0, (Convert.ToInt32(fs.Length)));
                createReq.requestData = readBuf;

               createRequestOut reqOut = externalWS2.createRequest(createReq);
               

                this.buttonOut.Invoke((MethodInvoker)delegate
                {
                    buttonOut.Text = Consts.MSGOUT;
                });

                return reqOut.requestNumber;
            //   }
            //           else
            //    {
            //        MessageBox.Show("нет так нет");
            //        if ((backgroundWorkerCREATEREQ.IsBusy)) backgroundWorkerCREATEREQ.CancelAsync();
            //        return null;
             //   }


            }
            catch (Exception ex)
            {
                ThreadExceptionHandler.logger.Debug(ex.ToString() + " " + ex.Message.ToString());
                return null;
            }

        }
        #endregion

        #region поток на отправку
        private void backgroundWorkerCREATEREQ_DoWork(object sender, System.ComponentModel.DoWorkEventArgs e)
        {
            ThreadExceptionHandler.logger.Info("sending packet ");
            if (e.Cancel == true) return;
            SendingFiles();
            //while (!backgroundWorkerCREATEREQ.CancellationPending)
           // {
           //     if (e.Cancel == true) { break; }
           //     SendingFiles();                
           // }

            
        }
        #endregion

        #region работа потока
        private void backgroundWorkerCREATEREQ_RunWorkerCompleted(object sender, System.ComponentModel.RunWorkerCompletedEventArgs e)
        {
            try
            {
                this.buttonOut.Text = Consts.MSGOUT;
            }
            catch (Exception ex)
            {
                ThreadExceptionHandler.logger.Error(ex.Message + " " + ex.StackTrace);
            }
            finally
            {
                this.Cursor = Cursors.Default;
                panel1.Enabled = true;
            }

        }
        #endregion

        #region проверка данных - которые должен бьл ввести пользователь
        /// <summary>
        /// Сам процесс отправки
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnOutPortalZIP_Click(object sender, EventArgs e)
        {
            if (textBoxNamePack.Text.ToString() == "") { MessageBox.Show("ВВедите краткое описание отправляемого пакета!"); return; }
            if (cbbRegion.Text.ToString() == "") { MessageBox.Show("ВВедите регион!"); return; }
            if ((okatos == null) || (okatos.Count <= 0)) { MessageBox.Show("ВВедите окато!"); return; }
            if (SettHelper.props.CertFields.EmailPerson == "") { MessageBox.Show("ВВедите email!"); return; }
            TabPage selectTab = null;
            this.tabControl2.SelectedTab.Invoke((MethodInvoker)delegate
            {                
                selectTab = tabControl2.SelectedTab;
            });
            if ((path_MP_TP_Req == "") &&(countDopPDf <=0) && (selectTab != this.tbTexError))  { MessageBox.Show("Нет выбранного пакета!"); return; }

            StartSending();
        }
        #endregion

        #region TEST1 - архивация и преобразование 
        /// <summary>
        ///   разархивируем пакет и  проба на преобразование xslt
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void button1_Click(object sender, EventArgs e)
        {
            string startPath = @"d:\123";
            string zipPath = fmPack.txtBoxFile.Text;
            //string extractPath = @"c:\example\extract";

            ZipFile zip = new ZipFile(zipPath);
            zip.ExtractAll(startPath);

            string[] fales = Directory.GetFiles(startPath, "*.xml");
            string fileXMl = "";
            foreach (string fale in fales)
            {
               // txtOKato.Text += fale + Environment.NewLine;
                fileXMl = fale + Environment.NewLine;
            }

            string filesXSl = "ConversionREQ.xsl";
            string fileREs = @"D:\123\req_.xml";

            if (File.Exists(fileREs)) { File.Delete(fileREs); };

            XslTransform xslTR = new XslTransform();
            xslTR.Load(filesXSl);
            XPathDocument xpathDoc = new XPathDocument(fileXMl);

            XmlTextWriter xmlWrite = new XmlTextWriter(fileREs, Encoding.UTF8);
            xmlWrite.Formatting = Formatting.Indented;

            xslTR.Transform(xpathDoc, null, xmlWrite, null);

            xmlWrite.Close();

        }
        #endregion

        #region TEST2 - подпись
        /// <summary>
        ///    трансформация xslt
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void button5_Click(object sender, EventArgs e)
        {
            //if (isCorrectness(this.txtBoxFile.Text.ToString())!="")//проврека правильности пакета
            //    MessageBox.Show("Смотрим!");
            SignReq(fmPack.txtBoxFile.Text.ToString());
        }
        #endregion
         
        #region  TEST3
        /// <summary>
        /// Test
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void button6_Click(object sender, EventArgs e)
        {
            StartSign();
        }
        #endregion

        #region TEST4
        /// <summary>
        ///  отображение настроек для доп. пакета
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void comboBoxTypeRequest_SelectedIndexChanged(object sender, EventArgs e)
        {
           /// this.grBoxDOP.Enabled = (this.comboBoxTypeRequest.SelectedIndex == 3);
            //this.groupBox1.Visible = (this.comboBoxTypeRequest.SelectedIndex != 2);
           // radioButton1.Checked = true;
          //  label9.Visible = (this.comboBoxTypeRequest.SelectedIndex == 2);
        }

        #endregion

        #region Создание записи словаря - для отправки
        private void CreateDic(int i, string el)
        {
            dic["GUID"] = "asdadad";///при изменении статуса меняется
            dic["DateDispath"] = DateTime.Today.ToString();
            if (i > 0)
                dic["NamePack"] = textBoxNamePack.Text.ToString() + i.ToString();
            else dic["NamePack"] = textBoxNamePack.Text.ToString();

            this.cbbRegion.Invoke((MethodInvoker)delegate
            {
                dic["Region"] = this.cbbRegion.SelectedValue.ToString();
            });
           
            dic["Status"] = "Старт";
            dic["Email"] = SettHelper.props.CertFields.EmailPerson.ToString();
            dic["Certificate"] = CurrentCert.SerialNumber;
            dic["Data"] = DateTime.Today.ToString();            
            dic["fileNameReq"] = Path.GetFileName(el);
            if (SettHelper.props.CertFields.isDublicatPack)
            {
                FileStream fs = new FileStream(oFDLGZIP1.InitialDirectory + el, FileMode.Open, FileAccess.Read);
                byte[] readBuf = new byte[fs.Length];
                fs.Read(readBuf, 0, (Convert.ToInt32(fs.Length)));
                dic["FILE"] = readBuf;
                fs.Close();
            }
        }
        #endregion

        private void InitDictionary()
        {
            dic.Clear();
        }

        #region Проверка пакета  для отправки - МП или ТП или REQ или формировать заявление
        private string isCorrectness(string pathMP_TP_REQ)
        {
            if ((Path.GetFileNameWithoutExtension(pathMP_TP_REQ).ToLower().IndexOf("req".ToLower()) != -1) &&
                ((Path.GetExtension(pathMP_TP_REQ).ToLower().IndexOf("zip".ToLower()) != -1)))
            {
                return path_MP_TP_Req;  //отправляем  уже готовое заявление
            }
            else
            {
                string val = "";
                TabPage selectTab = null;               
                this.tabControl2.Invoke((MethodInvoker)delegate
                {
                    this.tabControl2.SelectedTab.Invoke((MethodInvoker)delegate
                    {
                        val = tabControl2.SelectedTab.Tag.ToString();
                        selectTab = tabControl2.SelectedTab;
                    });
                });
                if ((((Path.GetFileNameWithoutExtension(pathMP_TP_REQ).ToLower().IndexOf(Consts.GUOKS.ToLower()) != -1) ||
                     (Path.GetFileNameWithoutExtension(pathMP_TP_REQ).ToLower().IndexOf(Consts.GKUZU.ToLower()) != -1))&&
                     ((Path.GetExtension(pathMP_TP_REQ).ToLower().IndexOf("zip".ToLower()) != -1))
                    )||
                    ((path_MP_TP_Req =="")&&(countDopPDf>0)) ||
                    (selectTab == this.tbTexError)
                    )
                    {                        
                    if (selectTab == this.tb_Fix_Error)
                            return FrPortalRosreestr.portalRosreestr.CreatePackReq(pathMP_TP_REQ, val, true);
                        else
                            return FrPortalRosreestr.portalRosreestr.CreatePackReq(pathMP_TP_REQ, val);
                    }
                else
                {
                    return "";
                }
            }
        }
        #endregion

        #region Получение  окато
        string OkatoByKodeRegion(string kod)
        {
            if (okatos == null) return "";
            KeyValuePair<string, string> el = okatos.Find(okato => okato.Key == kod);
            return el.Value;
        }
        #endregion

        #region FormClosing
        private void OutEvents_FormClosing(object sender, FormClosingEventArgs e)
        {
            try
            {
                if ((backgroundWorkerCREATEREQ.IsBusy)) backgroundWorkerCREATEREQ.CancelAsync();
            }
            catch (Exception ex)
            {
                ThreadExceptionHandler.logger.Error(ex.Message + " " + ex.StackTrace.ToString());
            }
            finally
            {
                backgroundWorkerCREATEREQ.Dispose();
            }

        }
        #endregion

        #region LoadData адрес ит окато и др
        //isCorrec
        /// <summary>
        ///  Заггрузка Requets.Type.xsd
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void OutEvents_Load(object sender, EventArgs e)
        {
            try
            {
                //comboBoxTypeRequest.Sorted = false;
               // comboBoxTypeRequest.DataSource = items;
               // comboBoxTypeRequest.ValueMember = "Key";
                //comboBoxTypeRequest.DisplayMember = "Value";
                var items = FrPortalRosreestr.portalRosreestr.ReadXsd(Application.StartupPath + Consts.XSDdRequest_Update);
                cbbUpdate.Sorted = false;
                cbbUpdate.DataSource = items;
                cbbUpdate.ValueMember = "Key";
                cbbUpdate.DisplayMember = "Value";

                cbb_fix_error.Sorted = false;
                cbb_fix_error.DataSource = items;
                cbb_fix_error.ValueMember = "Key";
                cbb_fix_error.DisplayMember = "Value";

                var itemsRe = FrPortalRosreestr.portalRosreestr.ReadXsd(Application.StartupPath + @"\\DATA\\adresCod.xsd");
                cbbRegion.Sorted = false;
                cbbRegion.DataSource = itemsRe;
                cbbRegion.ValueMember = "Key";
                cbbRegion.DisplayMember = "Value";

                okatos = FrPortalRosreestr.portalRosreestr.ReadXsd(Application.StartupPath + @"\\DATA\\regOKATO.xsd");

               // CallBackMy.callBackEventHandler("Tets");
            }
            catch (Exception ex)
            {
                ThreadExceptionHandler.ShowEventDialog("", ex);
            }
        }
        #endregion

        #region Разбор xml заявления
        /// <summary>
        ///  Разбор xml заявуления
        /// </summary>
        /// <param name="FileName"></param>
        /// <returns></returns>
        private Dictionary<string, object> ReadReQ(string packs)
        {
            try
            {
                InitDictionary();
                string pack = packs;
                string fileName = Path.GetFileNameWithoutExtension(pack) + "getData" + "\\";
                string tempDir = Path.GetTempPath() + fileName;
                ZipFile zip = new ZipFile(pack);
                if (Directory.Exists(tempDir)) Directory.Delete(tempDir, true);
                zip.ExtractAll(tempDir);
                zip.Dispose();

                IEnumerable<string> fileReq = Directory.GetFiles(tempDir, "*.xml");

                if (fileReq.Count() <= 0) return dic;

                XElement xmlDoc = XElement.Load(fileReq.First().ToString());

                dic["GUID"] = "a29095a0-cdb7-4d59-88ef-7c5b03ffaf5";
                dic["DateDispath"] = DateTime.Today.ToString();
                dic["RequestNumber"] = reqNumber;
                dic["Status"] = "Начало обработки";
                dic["Certificate"] = CurrentCert.SerialNumber;
                return dic;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message + " " + ex.Source + " " + ex.HelpLink + " " + ex.StackTrace);
                ThreadExceptionHandler.logger.Debug(ex.Message + " " + ex.Source + " " + ex.HelpLink + " " + ex.StackTrace);
                return null;
            }
        }

        #endregion

        #region Показать ответ
        private bool SendingEvent(string el, int i)
        {
            try
            {
                string _region = "";
                this.cbbRegion.Invoke((MethodInvoker)delegate
                {
                    _region = this.cbbRegion.Text.ToString();
                });
                if (MessageBox.Show("Отправить пакет в регион  " +_region, " ", MessageBoxButtons.OKCancel) == System.Windows.Forms.DialogResult.OK)
                {
                  
                    InitDictionary();
                    CreateDic(i, el);
                    string reqNumber = _SendEvent(oFDLGZIP1.InitialDirectory + el);
                    ThreadExceptionHandler.logger.Info("requestNumber "+reqNumber);
                    if ((reqNumber == null) || (reqNumber == ""))
                    {
                        MessageBox.Show(Consts.ERRORCONNECTPORTAL + Path.GetFileName(el));
                        return false;
                    }
                    string err = "";
                    try
                    {
                        this.dataGridView1.Invoke((MethodInvoker)delegate
                        {
                            dataGridView1.Rows.Add(el, reqNumber);
                            dic["RequestNumber"] = reqNumber;
                            err = FrPortalRosreestr.sqlStruct.InsertRequest(dic);
                        });
                        return true;
                    }
                    catch (Exception ex)
                    {
                        ThreadExceptionHandler.ShowEventDialog(Consts.ERRORINSERTBD, ex);
                        return false;
                    }
                }
                else
                {
                    MessageBox.Show("нет так нет");
                    if ((backgroundWorkerCREATEREQ.IsBusy)) backgroundWorkerCREATEREQ.CancelAsync();
                    return true;
                }
            }
            catch (Exception Ex)
            {
                ThreadExceptionHandler.logger.Error(Ex.Message + " " + Ex.StackTrace);
                return false;
            }
        }
        #endregion

        /// <summary>
        ///  ПОДГОТОВКА К ОТПРАВКЕ ПАКЕТА
        /// </summary>
        void SendingFiles()
        {
            this.dataGridView1.Invoke((MethodInvoker)delegate
            {
                dataGridView1.Rows.Clear(); ///очищаем результирующую таблицу
            });
            int i = 0;
            this.buttonOut.Invoke((MethodInvoker)delegate
            {
                this.buttonOut.Text = Consts.VALIDATEPACK;
            });
            ThreadExceptionHandler.logger.Info("isCorrerct ");
            

            string _reqFile = isCorrectness(path_MP_TP_Req);
            if (_reqFile != "")//проврека правильности пакета
            {
                ThreadExceptionHandler.logger.Info("SendEvent "+_reqFile);
                SendingEvent(_reqFile, i);
                i++;
            }//isCorrect
            #region отправка сразу нескольких межевых или техпланов
            // else
         //   {
                // Если есть однотипные пакеты (то есть   отправка в один регион )
                // то достаточно выбрать только каталог 
                // Каждый пакет должен содержаться в отдельной папке 
                // Наименование папки будет копироваться в наименование пакета 
                // В наименованиии папке может так же содержаться наименование номера предыдущей заявки

                /* ДО НОВОЙ РЕАЛИЗАЦИИ  (ОТПРАВКА НЕСКОЛКИХ  ПАКЕТОВ) -  ОТПРАВКА НЕСКОЛЬКИХ ПАКЕТОВ СРАЗУ 
                 
                    DirectoryInfo dir = new DirectoryInfo(textBox1.Text);
                    foreach (var item in dir.GetDirectories())
                    {
                            this.textBoxNamePack.Invoke((MethodInvoker)delegate
                            {
                                this.textBoxNamePack.Text = item.Name;
                            });
                            int _pos = item.Name.IndexOf("35-");
                            string Numberreq = "";
                            if (_pos != -1 ) 
                               Numberreq  = item.Name.Substring(item.Name.IndexOf("35"),10);
                            if (Numberreq != "")
                            {
                                this.txtRequetsNumber.Invoke((MethodInvoker)delegate
                                {
                                    this.txtRequetsNumber.Text = Numberreq;
                                });
                            }
                            string[] files = Directory.GetFiles(item.FullName, "*", SearchOption.AllDirectories).Where(s => (Path.GetExtension(s) == ".zip")).ToArray();
                            this.buttonOut.Invoke((MethodInvoker)delegate
                            {
                                this.buttonOut.Text = Consts.VALIDATEPACK + item.Name;
                            });
                            if (files.ToArray().Count() > 0)
                            {
                                string _reqFile = isCorrectness(files[0]);
                                if (_reqFile != "")//проврека правильности пакета
                                {
                                   this.textBoxLOGLOAD.Invoke((MethodInvoker)delegate
                                   {
                                        textBoxLOGLOAD.AppendText("Формирование пакета № "+i.ToString());
                                   });
                                   if (!SendingEvent(_reqFile, i)) break;
                                    i++;
                                }//isCorrect
                            }
                    }
                 */
            //  }
            #endregion
        }


        #region LoadKPT
        void loadingKPT()
        {
            this.textBoxLOGLOAD.Invoke((MethodInvoker)delegate
            {
                textBoxLOGLOAD.Clear();
                textBoxLOGLOAD.AppendText("Старт");
            });
           
            String[] s = textBoxListKPT.Text.Split(new String[] { "\r\n" }, StringSplitOptions.RemoveEmptyEntries);
            this.textBoxListKPT.Invoke((MethodInvoker)delegate
            {
                textBoxListKPT.Enabled = false;
            });
            this.buttonloadKPT.Invoke((MethodInvoker)delegate
            {
                buttonloadKPT.Text = "Подождите!...";
            });
            
            for (int i = 0; i < s.Count(); i++)
            {
                this.textBoxLOGLOAD.Invoke((MethodInvoker)delegate
                {
                    textBoxLOGLOAD.AppendText("Кадастровый номер " + s[i]);
                });
                this.buttonloadKPT.Invoke((MethodInvoker)delegate
                {
                    buttonloadKPT.Text = "Подождите! " + i.ToString() + "..";
                });
                
               
                InitDictionary();
                string cadNum = s[i];
                dic["GUID"] = "----";///при изменении статуса меняется
                dic["DateDispath"] = DateTime.Today.ToString();
                dic["NamePack"] = cadNum;

                dic["Region"] = loadKPT.KodRegionByCadNum(cadNum);

                dic["Status"] = "Старт";
                dic["Email"] = SettHelper.props.DeclarentFields.zEmail.ToString();
                dic["Certificate"] = CurrentCert.SerialNumber;
                dic["Data"] = DateTime.Today.ToString();
                this.textBoxLOGLOAD.Invoke((MethodInvoker)delegate
                {
                    textBoxLOGLOAD.AppendText("Идет формирование заявлния ");
                });
                

                string reqNumber = GET_KPT(s[i]);

                // textBoxListKPT.Lines[i] =s[i] + " " + reqNumber;              
                string err = "";
                try
                {
                    this.textBoxLOGLOAD.Invoke((MethodInvoker)delegate
                    {
                        textBoxLOGLOAD.AppendText("Сохранение данных! ");
                    });
                   
                    dic["RequestNumber"] = reqNumber;
                    err = FrPortalRosreestr.sqlStruct.InsertRequest(dic);
                    // return true;
                    this.textBoxLOGLOAD.Invoke((MethodInvoker)delegate
                    {
                        textBoxLOGLOAD.AppendText("Результат ! " + reqNumber);
                        textBoxLOGLOAD.AppendText("---------------------------------------------------------");
                    });
                    this.buttonloadKPT.Invoke((MethodInvoker)delegate
                    {
                        buttonloadKPT.Text = "Заказ КПТ"; 
                    });
                   
                    
                }
                catch (Exception ex)
                {
                    ThreadExceptionHandler.ShowEventDialog(Consts.ERRORINSERTBD, ex);
                    // return false;
                }
            }
        }

        #endregion

        #region Подпись пакета
        /// <summary>
        /// Подпись пакета
        /// </summary>
        /// <param name="pack"></param>
        private bool SignPackage(string pack)
        {
            try
            {
                this.buttonOut.Invoke((MethodInvoker)delegate
                {
                    this.buttonOut.Text = Consts.SignsPack;
                });
                string fileName = Path.GetFileNameWithoutExtension(pack) + "\\";
                string tempDir = Path.GetTempPath() + fileName;

                ZipFile zip = new ZipFile(pack);

                zip.AlternateEncodingUsage = ZipOption.Always; //
               // zip.AlternateEncoding = Encoding.GetEncoding(866); 

                if (Directory.Exists(tempDir)) Directory.Delete(tempDir, true);
                zip.ExtractAll(tempDir);
                zip.Dispose();

                Certificate.SignGUOKS(tempDir); //попись  пакета 
                Certificate.Signs(tempDir);   // подпись заявления и приложенных файлов

                zip = new ZipFile(tempDir);
                zip.AlternateEncodingUsage = ZipOption.Always; //
                //zip.AlternateEncoding = Encoding.GetEncoding(866); 

                zip.AddDirectory(tempDir);
                zip.Save(Path.GetFullPath(pack));
                zip.Dispose();
                if (Directory.Exists(tempDir)) Directory.Delete(tempDir, true);
                return true;
            }
            catch (Exception ex)
            {
                ThreadExceptionHandler.ShowEventDialog(Consts.ErrorSignPack, ex);
                return false;
            }
        }
        #endregion

        private bool SignPackageEl(string pack)
        {
            try
            {
                string fileName = Path.GetFileNameWithoutExtension(pack) + "\\";
                string tempDir = Path.GetTempPath() + fileName;

                ZipFile zip = new ZipFile(pack);

                zip.AlternateEncodingUsage = ZipOption.Always; //
                // zip.AlternateEncoding = Encoding.GetEncoding(866); 

                if (Directory.Exists(tempDir)) Directory.Delete(tempDir, true);
                zip.ExtractAll(tempDir);
                zip.Dispose();

                //Certificate.SignGUOKS(tempDir); //попись  пакета 
                Certificate.Signs(tempDir);   // подпись заявления и приложенных файлов

                zip = new ZipFile(tempDir);
                zip.AlternateEncodingUsage = ZipOption.Always; //
                //zip.AlternateEncoding = Encoding.GetEncoding(866); 

                zip.AddDirectory(tempDir);
                zip.Save(Path.GetFullPath(pack));
                zip.Dispose();
                if (Directory.Exists(tempDir)) Directory.Delete(tempDir, true);
                return true;
            }
            catch (Exception ex)
            {
                ThreadExceptionHandler.ShowEventDialog(Consts.ErrorSignPack, ex);
                return false;
            }
        }

        #region Подпись МП или ТП
        /// <summary>
        /// Подпись пакета
        /// </summary>
        /// <param name="pack"></param>
        private bool SignReq(string pack)
        {
            try
            {
                this.buttonOut.Invoke((MethodInvoker)delegate
                {
                    this.buttonOut.Text = Consts.SignsPack;
                });
                string fileName = Path.GetFileNameWithoutExtension(pack) + "\\";
                ThreadExceptionHandler.logger.Info("SignReq fileName " + fileName);
                
                string tempDir = Path.GetTempPath() + fileName;

                ZipFile zip = new ZipFile(pack);
                zip.AlternateEncodingUsage = ZipOption.Always;
              //  zip.AlternateEncoding = Encoding.GetEncoding((866));
                
                if (Directory.Exists(tempDir)) Directory.Delete(tempDir, true);
                zip.ExtractAll(tempDir);
                zip.Dispose();
                ThreadExceptionHandler.logger.Info("подпись заявления и приложенных файлов");
                Certificate.Signs(tempDir);   // подпись заявления и приложенных файлов
                ThreadExceptionHandler.logger.Info("Завершение");
                zip = new ZipFile(tempDir);
                
                zip.AddDirectory(tempDir);
                zip.Save(Path.GetFullPath(pack));
                zip.Dispose();
                if (Directory.Exists(tempDir)) Directory.Delete(tempDir, true);
                return true;
            }
            catch (Exception ex)
            {
                ThreadExceptionHandler.ShowEventDialog(Consts.ErrorSignPack, ex);
                return false;
            }
        }
        #endregion

        void StartSending()
        {
            if (backgroundWorkerCREATEREQ.IsBusy) return;
            Cursor.Current = Cursors.WaitCursor;
            Application.DoEvents();
            panel1.Enabled = false;
            backgroundWorkerCREATEREQ.RunWorkerAsync();
        }

        public void StartSign()
        {
            if (backgroundWorker1.IsBusy) return;
            Cursor.Current = Cursors.WaitCursor;
            Application.DoEvents();
            panel1.Enabled = false;
            backgroundWorker1.RunWorkerAsync();
        }

        void StartLoad()
        {
            if (backgroundWorkerLoadKPT.IsBusy) return;
            Cursor.Current = Cursors.WaitCursor;
            Application.DoEvents();
            //panel1.Enabled = false;
            backgroundWorkerLoadKPT.RunWorkerAsync();
        }
    
        /// <summary>
        /// Выбор пакета для отправки или dop pdf
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtFileZIP_DoubleClick(object sender, EventArgs e)
        {
            
            /*oFDLGZIP1.Multiselect = true;
            if (oFDLGZIP1.ShowDialog() == DialogResult.OK)
            {
                if (oFDLGZIP1.FileNames.Count() == 1)
                    this.txtBoxFile.Text = oFDLGZIP1.InitialDirectory + oFDLGZIP1.FileName;
                else this.txtBoxFile.Text = "Выбранно : " + oFDLGZIP1.FileNames.Count().ToString() + "файлов!";

            }
            oFDLGZIP1.Multiselect = false;*/
        }


        #region GetKPT
        private string GET_KPT(string cadNum)
        {
            try
            {
                createRequestIn createReq;
                createReq = new createRequestIn();

                this.cbbRegion.Invoke((MethodInvoker)delegate
                {
                    createReq.region = loadKPT.KodRegionByCadNum(cadNum);
                });
                createReq.okato = OkatoByKodeRegion(createReq.region).Trim();
                //this.comboBoxTypeRequest.Invoke((MethodInvoker)delegate
               // {
                    createReq.requestType = "558101010000";
              //  });
                

                string pathFile = loadKPT.createReqKPT(cadNum);
                FileStream fs = new FileStream(pathFile, FileMode.Open, FileAccess.Read);
                byte[] readBuf = new byte[fs.Length];
                fs.Read(readBuf, 0, (Convert.ToInt32(fs.Length)));
                createReq.requestData = readBuf;

                dic["fileNameReq"] = "req_"+cadNum;
                dic["FILE"] = readBuf;
                fs.Close();
                this.textBoxLog.Invoke((MethodInvoker)delegate
                {
                    textBoxLog.AppendText("Оправка на портал росреестра! ");
                });
                
                createRequestOut reqOut = externalWS2.createRequest(createReq);
                this.buttonOut.Invoke((MethodInvoker)delegate
                {
                    buttonOut.Text = Consts.MSGOUT;
                });
                File.Delete(pathFile);
                return reqOut.requestNumber;
            }
            catch (Exception ex)
            {
                ThreadExceptionHandler.logger.Debug(ex.ToString() + " " + ex.Message.ToString());
                return null;
            }

        }
        #endregion

        private void button7_Click(object sender, EventArgs e)
        {
            if (textBoxListKPT.Text == "") return;
            StartLoad();
        }

        private void lblNumber_Click(object sender, EventArgs e)
        {

        }

        private void txtNumber_MaskInputRejected(object sender, MaskInputRejectedEventArgs e)
        {

        }

        private void backgroundWorkerLoadKPT_DoWork(object sender, System.ComponentModel.DoWorkEventArgs e)
        {
            ThreadExceptionHandler.logger.Info("sending packet ");
            if (e.Cancel == true) return;
            loadingKPT();
        }

        private void backgroundWorkerLoadKPT_RunWorkerCompleted(object sender, System.ComponentModel.RunWorkerCompletedEventArgs e)
        {
            try
            {
                this.buttonOut.Text = Consts.MSGOUT;
               
            }
            catch (Exception ex)
            {
                ThreadExceptionHandler.logger.Error(ex.Message + " " + ex.StackTrace);
            }
            finally
            {
                this.Cursor = Cursors.Default;
                tabControl1.Enabled = true;
                textBoxListKPT.Enabled = true;
            }
        }

        private void txtBoxFile_Click(object sender, EventArgs e)
        {
            radioButton2.Checked = true;
        }

        private void textBox1_Click(object sender, EventArgs e)
        {
            radioButton3.Checked = true;
        }

        private void textBox1_DoubleClick(object sender, EventArgs e)
        {
             
            if (folderBrowserDialog1.ShowDialog() == DialogResult.OK)
            {
                textBox1.Text = folderBrowserDialog1.SelectedPath; 
            }
        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {

        }

        private void txtPDF_DoubleClick(object sender, EventArgs e)
        {
           /* if (FrPortalRosreestr.portalRosreestr.frmPDfDocDev.ShowDialog() == DialogResult.OK)
            {
                txtPDF.Text = FrPortalRosreestr.portalRosreestr.frmPDfDocDev.txtPDF.Text;
            }
           */
            
        }

        private void groupBox1_Enter(object sender, EventArgs e)
        {

        }

        private void backgroundWorker1_DoWork(object sender, System.ComponentModel.DoWorkEventArgs e)
        {
            if (e.Cancel == true) return;
            SignPack();
        }

        private void  SignPack()
        {
            string selectedPath = "";
            var t = new Thread((ThreadStart)(() =>
            {
                OpenFileDialog fbd = new OpenFileDialog();
                fbd.Filter = "Файлы|*.zip";
                //fbd.RootFolder = System.Environment.SpecialFolder.MyComputer;
                //fbd.Description = "Выберите каталог для хранения файла заявления!";
                //fbd.ShowNewFolderButton = true;
                if (fbd.ShowDialog() == DialogResult.Cancel)
                    return;

                selectedPath = fbd.FileName;
            }));

            t.SetApartmentState(ApartmentState.STA);
            t.Start();
            t.Join();
            SignPackageEl(selectedPath);            
        }

        private void backgroundWorker1_RunWorkerCompleted(object sender, System.ComponentModel.RunWorkerCompletedEventArgs e)
        {
            try
            {
             //   this.buttonOut.Text = Consts.MSGOUT;
                
            }
            catch (Exception ex)
            {
                ThreadExceptionHandler.logger.Error(ex.Message + " " + ex.StackTrace);
            }
            finally
            {
               /* this.Cursor = Cursors.Default;
                panel1.Enabled = true;
                tabControl1.Enabled = true;
                panel2.Enabled = false;
                * */
            }
        }

        private void tabControl2_Selected(object sender, TabControlEventArgs e)
        {
          /*  label3.Text = "Файл  GKUOKS|GKUZU|req|  _*.zip";
            oFDLGZIP1.Filter = "(zip Files)|*.zip;";
            if ((sender as TabControl).SelectedTab == this.tbDop)
            {
                label3.Text = "Файл  PDF|GKUOKS|GKUZU|req|  _*.zip";
                oFDLGZIP1.Filter = "(zip Files, Pdf-допник)|*.zip;*.pdf";
            }*/
        }

        private void button7_Click_1(object sender, EventArgs e)
        {
            panel2.Enabled = false;
            lstPDf.Clear();
            lstNumbers = "";
            lstDate = "";
            
            this.fmPack.dataGridView.Visible = (this.tabControl2.SelectedTab == this.tbDop) || (this.tabControl2.SelectedTab == this.tbTexError);
            this.fmPack.label2.Visible = this.tabControl2.SelectedTab == this.tbDop || (this.tabControl2.SelectedTab == this.tbTexError);            


            if (this.fmPack.ShowDialog()==DialogResult.OK)
            {
                ///получить список файлов 
                /*
                lstPDf = this.fmPack.dataGridView.Rows.OfType<DataGridViewRow>()
                    .Where(x => x.Cells[1].Value != null)
                    .Select(x => x.Cells[1].Value.ToString()).ToList();
                lstNumbers = String.Join(";", this.fmPack.dataGridView.Rows.OfType<DataGridViewRow>()
                    .Where(x => x.Cells[2].Value != null)
                    .Select(x => x.Cells[2].Value.ToString()).ToArray());
                lstDate =  String.Join(";", this.fmPack.dataGridView.Rows.OfType<DataGridViewRow>()
                    .Where(x => x.Cells[1].Value != null)
                    .Select(x => x.Cells[1].Value.ToString()).ToArray());
                 */
                ///получить межевой или техплан
                ///получить доверенность 
                txtGeneralInformation.Text = "";
                path_MP_TP_Req = fmPack.txtBoxFile.Text.ToString();
                if (path_MP_TP_Req != "")
                    txtGeneralInformation.Text = txtGeneralInformation.Text + "МП/ТП план: " + path_MP_TP_Req + "\r\n";
                dovPDF = fmPack.txtPDF.Text.ToString();
                if (dovPDF != "")
                    txtGeneralInformation.Text = txtGeneralInformation.Text + "\n Доверенность: " + dovPDF + "\r\n";
                countDopPDf = fmPack.dataGridView.Rows.Count;
                if (countDopPDf > 0)
                    txtGeneralInformation.Text = txtGeneralInformation.Text + "\n Кол-во доп. файлов: " + countDopPDf.ToString();                
                panel2.Enabled = true;
            }
        }

        private void txtBoxFile_TextChanged(object sender, EventArgs e)
        {

        }

        private void label3_Click(object sender, EventArgs e)
        {

        }

        private void rbtnSignRG_CheckedChanged(object sender, EventArgs e)
        {

        }

        private void textBox2_TextChanged(object sender, EventArgs e)
        {

        }

        private void txtTechicalError_DoubleClick(object sender, EventArgs e)
        {
            if (this.frTechicalError.ShowDialog() == DialogResult.OK)            
            {                
                this.txtTechicalError.Text = "Данные:  \r\n" + this.frTechicalError.txt_cadNumber.Text + "\r\n" + this.frTechicalError.cbb_type_error.Text;
            }
        }
    }
   
}
