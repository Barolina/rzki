﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using SQLiteHelperTestApp;


namespace PortalRosreestr
{
    public partial class FrKodyPlay : Form
    {
        DataTable dt = new DataTable();
        public FrKodyPlay()
        {
            InitializeComponent();
        }

      public  void GetDataKOD()
    {
       // DataTable table1 = new DataTable();
        dt = FrPortalRosreestr.sqlStruct.LoadKodyPLay(CurrentCert.SerialNumber); //loG sqlStruct.GetEvents(CurrentCert.SerialNumber);
        bindingSource1.DataSource = dt;
        dataGridView1.AutoResizeColumns(DataGridViewAutoSizeColumnsMode.AllCellsExceptHeader);
    }
        private void FrKodyPlay_Load(object sender, EventArgs e)
        {
            dataGridView1.DataSource = bindingSource1;
            GetDataKOD();
           
        }
    }
}
