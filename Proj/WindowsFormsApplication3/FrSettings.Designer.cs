﻿namespace PortalRosreestr
{
    partial class FrSettings
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FrSettings));
            this.folderBrowserDialog1 = new System.Windows.Forms.FolderBrowserDialog();
            this.openFileDialog1 = new System.Windows.Forms.OpenFileDialog();
            this.panel1 = new System.Windows.Forms.Panel();
            this.chkShow = new System.Windows.Forms.CheckBox();
            this.chkTOfile = new System.Windows.Forms.CheckBox();
            this.label2 = new System.Windows.Forms.Label();
            this.checkedListBox1 = new System.Windows.Forms.CheckedListBox();
            this.btnSelBD = new System.Windows.Forms.Button();
            this.txtBD = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.btnOk = new System.Windows.Forms.Button();
            this.tabCtrlRec = new System.Windows.Forms.TabControl();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.checkBox1 = new System.Windows.Forms.CheckBox();
            this.imageList1 = new System.Windows.Forms.ImageList(this.components);
            this.panel1.SuspendLayout();
            this.tabCtrlRec.SuspendLayout();
            this.tabPage1.SuspendLayout();
            this.SuspendLayout();
            // 
            // openFileDialog1
            // 
            this.openFileDialog1.FileName = "openFileDialog1";
            this.openFileDialog1.Filter = "\"(*db)|*.db|Все файлы (*.*)|*.*\"";
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.chkShow);
            this.panel1.Controls.Add(this.chkTOfile);
            this.panel1.Controls.Add(this.label2);
            this.panel1.Controls.Add(this.checkedListBox1);
            this.panel1.Controls.Add(this.btnSelBD);
            this.panel1.Controls.Add(this.txtBD);
            this.panel1.Controls.Add(this.label1);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.panel1.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(45)))), ((int)(((byte)(85)))), ((int)(((byte)(90)))));
            this.panel1.Location = new System.Drawing.Point(3, 3);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(657, 158);
            this.panel1.TabIndex = 7;
            // 
            // chkShow
            // 
            this.chkShow.AutoSize = true;
            this.chkShow.Location = new System.Drawing.Point(411, 98);
            this.chkShow.Name = "chkShow";
            this.chkShow.Size = new System.Drawing.Size(238, 19);
            this.chkShow.TabIndex = 16;
            this.chkShow.Text = "Показывать при следующем запуске";
            this.chkShow.UseVisualStyleBackColor = true;
            this.chkShow.CheckedChanged += new System.EventHandler(this.chkShow_CheckedChanged);
            // 
            // chkTOfile
            // 
            this.chkTOfile.AutoSize = true;
            this.chkTOfile.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.chkTOfile.Location = new System.Drawing.Point(3, 54);
            this.chkTOfile.Name = "chkTOfile";
            this.chkTOfile.Size = new System.Drawing.Size(376, 19);
            this.chkTOfile.TabIndex = 15;
            this.chkTOfile.Text = "Дублировать исходный пакет  при отправке  (запись в базу)";
            this.chkTOfile.UseVisualStyleBackColor = true;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.label2.Location = new System.Drawing.Point(5, 54);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(144, 15);
            this.label2.TabIndex = 13;
            this.label2.Text = "Фильтр  списка заявок:";
            this.label2.Visible = false;
            // 
            // checkedListBox1
            // 
            this.checkedListBox1.FormattingEnabled = true;
            this.checkedListBox1.Items.AddRange(new object[] {
            "относительно введенного e-mail",
            "только приоcтановки",
            "только положительно завершенные ",
            "в работе "});
            this.checkedListBox1.Location = new System.Drawing.Point(6, 70);
            this.checkedListBox1.Name = "checkedListBox1";
            this.checkedListBox1.Size = new System.Drawing.Size(619, 100);
            this.checkedListBox1.TabIndex = 12;
            this.checkedListBox1.Visible = false;
            // 
            // btnSelBD
            // 
            this.btnSelBD.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnSelBD.Location = new System.Drawing.Point(626, 23);
            this.btnSelBD.Name = "btnSelBD";
            this.btnSelBD.Size = new System.Drawing.Size(23, 23);
            this.btnSelBD.TabIndex = 5;
            this.btnSelBD.Text = "...";
            this.btnSelBD.UseVisualStyleBackColor = true;
            this.btnSelBD.Click += new System.EventHandler(this.btnSelBD_Click);
            // 
            // txtBD
            // 
            this.txtBD.Location = new System.Drawing.Point(6, 25);
            this.txtBD.Name = "txtBD";
            this.txtBD.Size = new System.Drawing.Size(619, 21);
            this.txtBD.TabIndex = 3;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Underline, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label1.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.label1.Location = new System.Drawing.Point(3, 9);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(167, 15);
            this.label1.TabIndex = 4;
            this.label1.Text = "Файл для хранения данных";
            // 
            // btnOk
            // 
            this.btnOk.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.btnOk.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.btnOk.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnOk.ForeColor = System.Drawing.Color.White;
            this.btnOk.Image = global::PortalRosreestr.Properties.Resources.fon3;
            this.btnOk.Location = new System.Drawing.Point(0, 166);
            this.btnOk.Name = "btnOk";
            this.btnOk.Size = new System.Drawing.Size(671, 41);
            this.btnOk.TabIndex = 14;
            this.btnOk.Text = "Сохранить";
            this.btnOk.UseVisualStyleBackColor = true;
            this.btnOk.Click += new System.EventHandler(this.btnOk_Click_1);
            // 
            // tabCtrlRec
            // 
            this.tabCtrlRec.Controls.Add(this.tabPage1);
            this.tabCtrlRec.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tabCtrlRec.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.tabCtrlRec.ImageList = this.imageList1;
            this.tabCtrlRec.ItemSize = new System.Drawing.Size(147, 35);
            this.tabCtrlRec.Location = new System.Drawing.Point(0, 0);
            this.tabCtrlRec.Multiline = true;
            this.tabCtrlRec.Name = "tabCtrlRec";
            this.tabCtrlRec.SelectedIndex = 0;
            this.tabCtrlRec.ShowToolTips = true;
            this.tabCtrlRec.Size = new System.Drawing.Size(671, 207);
            this.tabCtrlRec.TabIndex = 16;
            // 
            // tabPage1
            // 
            this.tabPage1.BackColor = System.Drawing.Color.White;
            this.tabPage1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.tabPage1.Controls.Add(this.checkBox1);
            this.tabPage1.Controls.Add(this.panel1);
            this.tabPage1.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(1)))), ((int)(((byte)(85)))), ((int)(((byte)(177)))));
            this.tabPage1.ImageIndex = 0;
            this.tabPage1.Location = new System.Drawing.Point(4, 39);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage1.Size = new System.Drawing.Size(663, 164);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.Text = "Настройки";
            this.tabPage1.UseVisualStyleBackColor = true;
            // 
            // checkBox1
            // 
            this.checkBox1.AutoSize = true;
            this.checkBox1.Location = new System.Drawing.Point(494, -22);
            this.checkBox1.Name = "checkBox1";
            this.checkBox1.Size = new System.Drawing.Size(80, 17);
            this.checkBox1.TabIndex = 17;
            this.checkBox1.Text = "checkBox1";
            this.checkBox1.UseVisualStyleBackColor = true;
            // 
            // imageList1
            // 
            this.imageList1.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("imageList1.ImageStream")));
            this.imageList1.TransparentColor = System.Drawing.Color.Transparent;
            this.imageList1.Images.SetKeyName(0, "Settings.png");
            this.imageList1.Images.SetKeyName(1, "Properties.png");
            // 
            // FrSettings
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(671, 207);
            this.Controls.Add(this.btnOk);
            this.Controls.Add(this.tabCtrlRec);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "FrSettings";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Настройки";
            this.Shown += new System.EventHandler(this.FrSettings_Shown);
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.tabCtrlRec.ResumeLayout(false);
            this.tabPage1.ResumeLayout(false);
            this.tabPage1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.FolderBrowserDialog folderBrowserDialog1;
        private System.Windows.Forms.OpenFileDialog openFileDialog1;
        public System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TabControl tabCtrlRec;
        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.ImageList imageList1;
        private System.Windows.Forms.CheckedListBox checkedListBox1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Button btnOk;
        private System.Windows.Forms.CheckBox chkTOfile;
        private System.Windows.Forms.CheckBox checkBox1;
        public System.Windows.Forms.CheckBox chkShow;
        internal System.Windows.Forms.Button btnSelBD;
        internal System.Windows.Forms.TextBox txtBD;
    }
}