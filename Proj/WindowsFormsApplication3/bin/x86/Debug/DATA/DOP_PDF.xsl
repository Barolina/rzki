<?xml version="1.0" encoding="UTF-8"?>
<!--
  Версия заявления из 18 для TP03 and MP05
-->
<xsl:stylesheet version="2.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:fn="http://www.w3.org/2006/xpath-functions" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xalan="xalan://" extension-element-prefixes="xalan" xmlns:guid="clitype:System.Guid?partialname=mscorlib" exclude-result-prefixes="guid" xmlns:ms="urn:schemas-microsoft-com:xslt" xmlns:usr="urn:the-xml-files:xslt">
	<xsl:output method="xml" version="1.0" encoding="UTF-8" indent="yes"/>
	<xsl:strip-space elements="*"/>
	<xsl:param name="Curr_Date">2015-06-03</xsl:param>
	<xsl:param name="Curr_Time">18:10:43</xsl:param>
	<xsl:param name="NameDoc">--Паспорт гражданина Российской Федерации</xsl:param>
	<xsl:param name="Serial">--Серия</xsl:param>
	<xsl:param name="Number">--Номер</xsl:param>
	<xsl:param name="DateDoc">--Дата выдачи</xsl:param>
	<xsl:param name="Issuqe">--Где</xsl:param>
	<xsl:param name="DocRegion">--регион</xsl:param>
	<xsl:param name="DocNote">--Тщеу</xsl:param>
	<xsl:param name="Snils">--Снилс</xsl:param>
	<xsl:param name="Surname" select="'--фамилия'"/>
	<xsl:param name="First" select="'--имя'"/>
	<xsl:param name="Patronumic" select="'--отчество'"/>
	<xsl:param name="RequestNumber">--номер заявления</xsl:param>
	<xsl:param name="PDFName">--файл доверенности</xsl:param>
	<xsl:param name="PDFIS"/>
	<xsl:param name="PdfNumber"/>
	<xsl:param name="PDFDate"/>
	<xsl:param name="PDFIssueQue"/>
	<xsl:param name="CountImage" select="4"/>
	<xsl:param name="ImageIS" select="1"></xsl:param>
	<xsl:variable name="RootName" select="name(node())"/>
	<xsl:variable name="GUID_TP" select="TP/@GUID"/>
	<xsl:variable name="GUID_Mp" select="MP/@GUID"/>
	<xsl:template match="/">
		<xsl:call-template name="tSTD"/>
	</xsl:template>
	<!--  START -->
	<xsl:template name="tSTD">
		<Requests_GZK_Realty>
			<eDocument CodeType="112" Version="18">
				<Recipient Name=""/>
				<!--70000000000-->
			</eDocument>
			<Request_GZK_Realty>
				<Title>
					<xsl:call-template name="Rubic"/>
				</Title>
				<xsl:call-template name="Declarent"/>
				<xsl:call-template name="Applied_Documents"/>
			</Request_GZK_Realty>
		</Requests_GZK_Realty>
	</xsl:template>
	<!--Если подгружаем доверенность PDF-->
	<xsl:template name="WriteAppiliedDocumentPDF">
		<xsl:param name="pCode"/>
		<xsl:param name="pName"/>
		<xsl:param name="pNumber"/>
		<xsl:param name="pDate"/>
		<xsl:param name="pIssueOrgan"/>
		<xsl:param name="pOriginal" select="'1'"/>
		<xsl:param name="pOriginal_Sheet" select="'1'"/>
		<xsl:param name="pAppliedFileType" select="'02'"/>
		<xsl:param name="pAppliedFile"/>
		<xsl:element name="Applied_Document">
			<xsl:element name="Code_Document">
				<xsl:value-of select="$pCode"/>
			</xsl:element>
			<xsl:element name="Name">
				<xsl:value-of select="$pName"/>
			</xsl:element>
			<xsl:element name="Number">
				<xsl:value-of select="$pNumber"/>
			</xsl:element>
			<xsl:element name="Date">
				<xsl:value-of select="$pDate"/>
			</xsl:element>
			<xsl:element name="IssueOrgan">
				<xsl:value-of select="$pIssueOrgan"/>
			</xsl:element>
			<xsl:element name="Quantity">
				<xsl:attribute name="Original"><xsl:value-of select="$pOriginal"/></xsl:attribute>
				<xsl:attribute name="Original_Sheet"><xsl:value-of select="$pOriginal_Sheet"/></xsl:attribute>
			</xsl:element>
			<xsl:if test="$pAppliedFile">
				<xsl:element name="AppliedFiles">
					<xsl:element name="AppliedFile">
						<xsl:attribute name="type"><xsl:value-of select="$pAppliedFileType"/></xsl:attribute>
						<xsl:attribute name="name"><xsl:value-of select="$pAppliedFile"/></xsl:attribute>
					</xsl:element>
				</xsl:element>
			</xsl:if>
		</xsl:element>
	</xsl:template>
	<xsl:template name="Applied_Documents">
		<Applied_Documents>
			<xsl:if test="($PDFIS = '1')">
				<xsl:call-template name="WriteAppiliedDocumentPDF">
					<xsl:with-param name="pCode" select="'558401000000'"/>
					<xsl:with-param name="pName" select="'Доверенность'"/>
					<xsl:with-param name="pNumber" select="$PdfNumber"/>
					<xsl:with-param name="pDate" select="$PDFDate"/>
					<xsl:with-param name="pIssueOrgan" select="$PDFIssueQue"/>
					<xsl:with-param name="pAppliedFile" select="$PDFName"/>
				</xsl:call-template>
			</xsl:if>
			<Applied_Document>
				<Code_Document>558101100000</Code_Document>
				<Name>Заявление о представлении дополнительных документов на кадастровый учет</Name>
				<Number>1</Number>
				<Date>
					<xsl:value-of select="$Curr_Date"/>
				</Date>
				<IssueOrgan/>
				<Quantity Original="1" Original_Sheet="1"/>
			</Applied_Document>
			<!--
			<xsl:if test="$ImageIS='1'">
				<xsl:variable name="cnm" select="$CountImage-1"/>
				<xsl:value-of select="$cnm"/>
			</xsl:if>
-->
			<Applied_Document>
				<Code_Document>558502020000</Code_Document>
				<Name>pdf</Name>
				<Number>б/н</Number>
				<Date>2016-02-18</Date>
				<IssueOrgan/>
				<Duration>
					<Started>
						<xsl:value-of select="$Curr_Date"/>
					</Started>
				</Duration>
				<Images>
					<Image Name="dop\RFile1.pdf"/>
				</Images>
				<Quantity Original="1" Original_Sheet="1"/>
			</Applied_Document>
		</Applied_Documents>
	</xsl:template>
	<!--  AppliedFiles GUIDS ZIP -->
	<xsl:template name="AppliedFiles">
		<AppliedFiles>
			<xsl:choose>
				<xsl:when test="$RootName = 'TP'">
					<AppliedFile type="05">
						<xsl:attribute name="name"><xsl:value-of select="concat('GKUOKS_',$GUID_TP,'.zip')"/></xsl:attribute>
					</AppliedFile>
				</xsl:when>
				<xsl:otherwise>
					<AppliedFile type="02">
						<xsl:attribute name="name"><xsl:value-of select="concat('GKUZU_',$GUID_Mp,'.zip')"/></xsl:attribute>
					</AppliedFile>
				</xsl:otherwise>
			</xsl:choose>
		</AppliedFiles>
	</xsl:template>
	<xsl:template name="Declarent">
		<Declarants>
			<Declarant temp_id="1">
				<Person>
					<FIO>
						<Surname>
							<xsl:value-of select="$Surname"/>
						</Surname>
						<First>
							<xsl:value-of select="$First"/>
						</First>
						<Patronymic>
							<xsl:value-of select="$Patronumic"/>
						</Patronymic>
					</FIO>
					<Document>
						<Code_Document>008001001000</Code_Document>
						<Name>Паспорт гражданина Российской Федерации</Name>
						<Series>
							<xsl:value-of select="$Serial"/>
						</Series>
						<Number>
							<xsl:value-of select="$Number"/>
						</Number>
						<Date>
							<xsl:value-of select="$DateDoc"/>
						</Date>
						<IssueOrgan>
							<xsl:value-of select="$Issuqe"/>
						</IssueOrgan>
					</Document>
					<Location>
						<Region>
							<xsl:value-of select="$DocRegion"/>
						</Region>
						<Note>
							<xsl:value-of select="$DocNote"/>
						</Note>
					</Location>
					<SNILS>
						<xsl:value-of select="$Snils"/>
					</SNILS>
				</Person>
				<Delivery>
					<Kind_Delivery>785004000000</Kind_Delivery>
					<Way_Delivery>
						<Recipient>02</Recipient>
					</Way_Delivery>
				</Delivery>
			</Declarant>
		</Declarants>
	</xsl:template>
	<xsl:template name="Rubic">
		<Rubric>
			<Code>558101100000</Code>
			<Reg_Folder>
				<xsl:value-of select="$RequestNumber"/>
			</Reg_Folder>
			<Visit_Purpose>659007002000</Visit_Purpose>
		</Rubric>
	</xsl:template>
	<xsl:template name="tFIO">
		<Surname>
			<xsl:value-of select="Surname"/>
		</Surname>
		<First>
			<xsl:value-of select="First"/>
		</First>
		<Patronymic>
			<xsl:value-of select="Patronymic"/>
		</Patronymic>
	</xsl:template>
	<xsl:template match="FIO" mode="tFIO">
		<xsl:copy>
			<xsl:call-template name="tFIO"/>
		</xsl:copy>
	</xsl:template>
</xsl:stylesheet>
