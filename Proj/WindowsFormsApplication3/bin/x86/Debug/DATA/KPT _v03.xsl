<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:fn="http://www.w3.org/2006/xpath-functions" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xalan="xalan://" extension-element-prefixes="xalan" xmlns:guid="clitype:System.Guid?partialname=mscorlib" exclude-result-prefixes="guid" xmlns:ms="urn:schemas-microsoft-com:xslt" xmlns:usr="urn:the-xml-files:xslt" xsi:schemaLocation="urn://x-artefacts-rosreestr-gov-ru/requests/gkn/3.0.9 RequestGKN_v03.xsd" xmlns="urn://x-artefacts-rosreestr-gov-ru/requests/gkn/3.0.9" xmlns:Del1="urn://x-artefacts-rosreestr-ru/commons/complex-types/delivery/1.1.1" xmlns:GKN1="urn://x-artefacts-rosreestr-ru/commons/complex-types/general-gkn/1.0.2" xmlns:Person2="urn://x-artefacts-rosreestr-ru/commons/complex-types/person/2.0.1" xmlns:tns="urn://x-artefacts-smev-gov-ru/supplementary/commons/1.0.1">
	<xsl:output method="xml" indent="yes"/>
	<ms:script language="C#" implements-prefix="usr"><![CDATA[
public string generateGUID() { return Guid.NewGuid().ToString();}
]]></ms:script>
	<xsl:strip-space elements="*"/>
	<!--параметры для заполнения-->
	<xsl:param name="Curr_Date">2015-06-18</xsl:param>
	<xsl:param name="Curr_Time">18:10:43</xsl:param>
	<xsl:param name="_GUID_DO"/>
	
		<xsl:param name="CadNum"></xsl:param>
    	<xsl:param name="Other"></xsl:param>
	
	<xsl:template match="/">
		<xsl:apply-templates select="PropsDeclarent"/>
	</xsl:template>
	<xsl:template match="PropsDeclarent">
		<RequestGKN>
		    <xsl:attribute name="NameSoftware">KTSoft</xsl:attribute>
		    <xsl:attribute name="VersionSoftware">03</xsl:attribute> 
			<xsl:attribute name="GUID"><xsl:value-of select="$_GUID_DO"/></xsl:attribute>
			<Title>
			   <RecipientName>Орган кадастрового учета</RecipientName>
		  	  <RecipientType>Орган кадастрового учёта</RecipientType>
			</Title>
				<Declarant declarantKind="357099000000">
				<GKN1:Person>
					<tns:FamilyName><xsl:value-of select="zFIO"/></tns:FamilyName>
					<tns:FirstName><xsl:value-of select="zName"/></tns:FirstName>
					<Person2:Document>
						<Person2:CodeDocument>008001001000</Person2:CodeDocument>
						<Person2:Number><xsl:value-of select="zNumber"/></Person2:Number>
						<Person2:Date><xsl:value-of select="zDATEDoc"/>+04:00</Person2:Date>
						<Person2:IssueOrgan>---</Person2:IssueOrgan>
					</Person2:Document>
				</GKN1:Person>
				<GKN1:IncomingDate><xsl:value-of select="$Curr_Date"/>+03:00</GKN1:IncomingDate>
			</Declarant>
			<RequiredData>
					<KPT>
						<CadastralNumber><xsl:value-of select="$CadNum"/></CadastralNumber>
					</KPT>
     		</RequiredData>
				<Delivery>
					<Del1:LinkEmail><xsl:value-of select="zEmail"/></Del1:LinkEmail>
				</Delivery>
				<Concept>
		<ProcessingPersonalData>Подтверждаю свое согласие, а также согласие представляемого мною лица, на обработку персональных данных (сбор, систематизацию, накопление, хранение, уточнение (обновление, изменение), использование, распространение (в том числе передачу), обезличивание, блокирование, уничтожение персональных данных, а также иных действий, необходимых для обработки персональных данных в рамках предоставления органами кадастрового учета, в соответствии с законодательством Российской Федерации государственных услуг), в том числе в автоматизированном режиме, включая принятие решений на их основе органом кадастрового учета, в целях предоставления государственной услуги</ProcessingPersonalData>
		<ConfirmationReliability>Настоящим подтверждаю: сведения, включенные в запрос, относящиеся к моей личности и представляемому мною лицу, а также внесенные мною ниже, достоверны; документы (копии документов), приложенные к запросу, соответствуют требованиям, установленным законодательством Российской Федерации, на момент представления запроса эти документы действительны и содержат достоверные сведения</ConfirmationReliability>
	</Concept>
		</RequestGKN>
	</xsl:template>
	</xsl:stylesheet>
