<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:fn="http://www.w3.org/2006/xpath-functions" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:gen="urn://x-artefacts-rosreestr-ru/commons/complex-types/general-gkn/1.0.2" xmlns:fio="urn://x-artefacts-smev-gov-ru/supplementary/commons/1.0.1" xmlns:doc2="urn://x-artefacts-rosreestr-ru/commons/complex-types/document/4.0.3" xmlns:qua="urn://x-artefacts-rosreestr-ru/commons/complex-types/quantity/1.0.1" xmlns:fl2="urn://x-artefacts-rosreestr-ru/commons/complex-types/person/2.0.1" xmlns:adrInp4="urn://x-artefacts-rosreestr-ru/commons/complex-types/address-input/4.0.1" xmlns="urn://x-artefacts-rosreestr-gov-ru/statement/gkn/1.0.9" xmlns:guid="clitype:System.Guid?partialname=mscorlib" exclude-result-prefixes="guid" xmlns:ms="urn:schemas-microsoft-com:xslt" xmlns:usr="urn:the-xml-files:xslt">
	<import namespace="" schemaLocation="dRegionsRF_v01.xsd"/>
	<xsl:output method="xml" indent="yes"/>
	<ms:script language="C#" implements-prefix="usr"><![CDATA[
public string generateGUID() { return Guid.NewGuid().ToString();}
]]></ms:script>
	<xsl:strip-space elements="*"/>
	<!--параметры для заполнения-->
	<xsl:param name="Curr_Date">2015-06-18</xsl:param>
	<xsl:param name="Curr_Time">18:10:43</xsl:param>
	<xsl:param name="_GUID_DO">
		<xsl:value-of select="usr:generateGUID()"/>
	</xsl:param>
	<xsl:param name="Serial">--серия пас.</xsl:param>
	<xsl:param name="Number">--номер пас.</xsl:param>
	<xsl:param name="DateDoc">--дата пас.</xsl:param>
	<xsl:param name="Issuqe">--кем выд. пас.</xsl:param>
	<xsl:param name="DocRegion">--регион</xsl:param>
	<xsl:param name="DocNote">--местоположение</xsl:param>
	<xsl:param name="Snils">--снилс</xsl:param>
	<xsl:param name="Surname" select="'--фамилия'"/>
	<xsl:param name="First" select="'--имя'"/>
	<xsl:param name="Patronumic" select="'--отчество'"/>
	<xsl:param name="Email">--email</xsl:param>
	<!--по идеи не нужны-->
	<xsl:param name="NameDoc">--название документа</xsl:param>
	<xsl:param name="_GUID">--guid req</xsl:param>
	<xsl:param name="PDFName">--файл доверенности</xsl:param>
	<xsl:param name="PDFIS"/>
	<xsl:param name="PdfNumber"/>
	<xsl:param name="PDFDate"/>
	<xsl:param name="PDFIssueQue"/>
	<xsl:param name="vFormParcel">FormParcels</xsl:param>
	<xsl:param name="vSpecifyParcel">SpecifyParcel</xsl:param>
	<xsl:param name="vExistBuilding">Building</xsl:param>
	<xsl:param name="pIsUpdate"/>
	<xsl:variable name="TP_Date" select="TP/GeneralCadastralWorks/@DateCadastral"/>
	<xsl:variable name="GUId_TP" select="$_GUID"/>
	<xsl:variable name="FIO_Cadastr" select="concat(.//.//Contractor/.//FamilyName,' ',.//.//Contractor/.//FirstName,' ',.//.//Contractor/.//Patronymic)"/>
	<xsl:variable name="GUId_MP" select="$_GUID"/>
	<xsl:variable name="NameMp" select="MP/@GUID"/>
	<xsl:variable name="NameTP" select="TP/@GUID"/>
	<xsl:variable name="isReados" select="name(node())"/>
	<xsl:param name="vTP">TP</xsl:param>
	<xsl:param name="vMP">MP</xsl:param>
	<xsl:param name="vNew">New</xsl:param>
	<xsl:param name="vExist">Exist</xsl:param>
	<xsl:param name="vTypeUpdate"/>
	<!--тип измнений-->
	<xsl:param name="Region_doc">--регион отправки</xsl:param>
	<!--Start для техплана 3 версии и межевого 5 версии -->
	<xsl:template match="/">
		<xsl:apply-templates select="MP" mode="MP"/>
		<xsl:apply-templates select="TP"/>
		<!--xsl:choose>
		
<xsl:when test="$isReados='MP'">
				<xsl:apply-templates select="MP" mode="MP"/>
			</xsl:when>
			<xsl:otherwise>
				<xsl:apply-templates select="TP"/>
			</xsl:otherwise>
		</xsl:choose-->
	</xsl:template>
	<xsl:template name="_StatementGKU">
		<xsl:param name="pTypeAction"/>
		<!--уточнение или образование -->
		<xsl:param name="pTypeProj"/>
		<!-- межевой или тех план -->
		<xsl:element name="StatementGKU">
			<xsl:choose>
				<xsl:when test="$pTypeAction =$vNew">
					<xsl:element name="GKU">
						<xsl:element name="Objects">
							<xsl:element name="ObjKind">
								<xsl:choose>
									<xsl:when test="$pTypeProj=$vMP">002001001000</xsl:when>
									<xsl:otherwise>
										<xsl:choose>
											<xsl:when test="$pTypeProj=$vTP">
												<xsl:choose>
													<xsl:when test="name(node())='Building'">002001002000</xsl:when>
													<xsl:when test="name(node())='Construction'">002001004000</xsl:when>
													<xsl:when test="name(node())='Uncompleted'">002001005000</xsl:when>
													<xsl:when test="name(node())='Flat'">002001003000</xsl:when>
												</xsl:choose>
											</xsl:when>
										</xsl:choose>
									</xsl:otherwise>
								</xsl:choose>
							</xsl:element>
							<xsl:call-template name="_Location"/>
						</xsl:element>
					</xsl:element>
				</xsl:when>
				<xsl:when test="$pTypeAction =$vExist">
					<xsl:element name="GKUIS">
						<xsl:choose>
							<xsl:when test="$pTypeProj=$vMP">
								<xsl:element name="CadastralNumber">
									<xsl:choose>
										<xsl:when test="$vTypeUpdate='SubParcels'">
											<xsl:value-of select="node()/node()/CadastralNumberParcel"/>
										</xsl:when>
										<xsl:otherwise>
											<xsl:choose>
												<xsl:when test="node()/node()/ExistEZ/@CadastralNumber!=''">
													<xsl:value-of select="node()/node()/ExistEZ/@CadastralNumber"/>
												</xsl:when>
												<xsl:when test="node()/node()/ExistParcel/@CadastralNumber!=''">
													<xsl:value-of select="node()/node()/ExistParcel/@CadastralNumber"/>
												</xsl:when>
												<xsl:otherwise>
													<xsl:value-of select="node()/node()/node()/ExistEZParcels/@CadastralNumber"/>
												</xsl:otherwise>
											</xsl:choose>
										</xsl:otherwise>
									</xsl:choose>
								</xsl:element>
								<xsl:element name="Parcel">
									<xsl:choose>
										<xsl:when test="$vTypeUpdate='Area'">
											<xsl:element name="chArea">true</xsl:element>
										</xsl:when>
										<xsl:when test="$vTypeUpdate='Categories'">
											<xsl:element name="chCategories">true</xsl:element>
										</xsl:when>
										<xsl:when test="$vTypeUpdate='Address'">
											<xsl:element name="chAddress">true</xsl:element>
										</xsl:when>
										<xsl:when test="$vTypeUpdate='Location'">
											<xsl:element name="chLocation">true</xsl:element>
										</xsl:when>
										<xsl:when test="$vTypeUpdate='SubParcels'">
											<xsl:element name="chParts">
												<xsl:call-template name="_SubParcel"/>
											</xsl:element>
										</xsl:when>
									</xsl:choose>
								</xsl:element>
							</xsl:when>
							<xsl:when test="$pTypeProj=$vTP">
								<xsl:element name="CadastralNumber">
									<xsl:value-of select="node()/node()/node()/@CadastralNumber"/>
								</xsl:element>
								<xsl:choose>
									<xsl:when test="name(node())='Building'">
										<xsl:element name="Building">  
											<xsl:choose>
												<xsl:when test="$vTypeUpdate='Area'">
													<xsl:element name="chArea">true</xsl:element>
												</xsl:when>
												<xsl:when test="$vTypeUpdate='Address'">
													<xsl:element name="chAddress">true</xsl:element>
												</xsl:when>
												<xsl:when test="$vTypeUpdate='Location'">
													<xsl:element name="chLocation">
														<xsl:element name="bLocation">true</xsl:element>
													</xsl:element>
												</xsl:when>
												<xsl:when test="$vTypeUpdate='Assignation'">
													<xsl:element name="chAssignation">true</xsl:element>
												</xsl:when>
												<xsl:when test="$vTypeUpdate='Level'">
													<xsl:element name="chLevel">true</xsl:element>
												</xsl:when>
												<xsl:when test="$vTypeUpdate='Wall'">
													<xsl:element name="chWall">true</xsl:element>
												</xsl:when>
												<xsl:when test="$vTypeUpdate='Name'">
													<xsl:element name="chName">true</xsl:element>
												</xsl:when>
											</xsl:choose>
										</xsl:element>
									</xsl:when>
									<xsl:when test="name(node())='Construction'">
										<xsl:element name="Construction">
											<xsl:choose>
												<xsl:when test="$vTypeUpdate='Address'">
													<xsl:element name="chAddress">true</xsl:element>
												</xsl:when>
												<xsl:when test="$vTypeUpdate='Location'">
													<xsl:element name="chLocation">
														<xsl:element name="bLocation">true</xsl:element>
													</xsl:element>
												</xsl:when>
												<xsl:when test="$vTypeUpdate='Assignation'">
													<xsl:element name="chAssignation">true</xsl:element>
												</xsl:when>
												<xsl:when test="$vTypeUpdate='Level'">
													<xsl:element name="chLevel">true</xsl:element>
												</xsl:when>
												<xsl:when test="$vTypeUpdate='Characteristic'">
													<xsl:element name="chCharacteristic">true</xsl:element>
												</xsl:when>
												<xsl:when test="$vTypeUpdate='Name'">
													<xsl:element name="chName">true</xsl:element>
												</xsl:when>
											</xsl:choose>
										</xsl:element>
									</xsl:when>
									<xsl:when test="name(node())='Flat'">
										<xsl:element name="Flat">
											<xsl:choose>
												<xsl:when test="$vTypeUpdate='Address'">
													<xsl:element name="chAddress">true</xsl:element>
												</xsl:when>
												<xsl:when test="$vTypeUpdate='Location'">
													<xsl:element name="chLocation">true</xsl:element>
												</xsl:when>
												<xsl:when test="$vTypeUpdate='Assignation'">
													<xsl:element name="chAssignation">true</xsl:element>
												</xsl:when>
												<xsl:when test="$vTypeUpdate='Level'">
													<xsl:element name="chLevel">true</xsl:element>
												</xsl:when>
												<xsl:when test="$vTypeUpdate='Area'">
													<xsl:element name="chArea">true</xsl:element>
												</xsl:when>
												<xsl:when test="$vTypeUpdate='ParentCadastralNumber'">
													<xsl:element name="chParentCadastralNumber">true</xsl:element>
												</xsl:when>
											</xsl:choose>
										</xsl:element>
									</xsl:when>
									<xsl:when test="name(node())='Uncompleted'">
										<xsl:element name="Uncompleted">
										<xsl:choose>
											<xsl:when test="$vTypeUpdate='Address'">
												<xsl:element name="chAddress">true</xsl:element>
											</xsl:when>
											<xsl:when test="$vTypeUpdate='Location'">
												<xsl:element name="chLocation">
													<xsl:element name="bLocation">true</xsl:element>
												</xsl:element>
											</xsl:when>
											<xsl:when test="$vTypeUpdate='Assignation'">
												<xsl:element name="chAssignation">true</xsl:element>
											</xsl:when>
											<xsl:when test="$vTypeUpdate='Characteristic'">
												<xsl:element name="chCharacteristic">true</xsl:element>
											</xsl:when>
										</xsl:choose>
										</xsl:element>
									</xsl:when>
								</xsl:choose>
							</xsl:when>
						</xsl:choose>
					</xsl:element>
				</xsl:when>
			</xsl:choose>
			<xsl:element name="Delivery">
				<xsl:element name="LinkEmail">
					<xsl:value-of select="$Email"/>
				</xsl:element>
			</xsl:element>
		</xsl:element>
	</xsl:template>
	<xsl:template name="_SubParcel">
		<xsl:for-each select="..//node()/node()/NewSubParcel">
			<xsl:element name="Part">
				<xsl:element name="NewPart">
					<xsl:element name="Designation">
						<xsl:value-of select="@Definition"/>
					</xsl:element>
				</xsl:element>
			</xsl:element>
		</xsl:for-each>
		<xsl:for-each select="..//node()/node()/ExistSubParcel">
			<xsl:element name="Part">
				<xsl:element name="ChangePart">
					<xsl:element name="CadastralNumber">
						<xsl:value-of select="@NumberRecord"/>
					</xsl:element>
				</xsl:element>
			</xsl:element>
		</xsl:for-each>
	</xsl:template>
	<xsl:template name="_Location">
		<xsl:for-each select=".//node()/node()/node()/Address">
			<xsl:if test="OKATO!=''">
				<xsl:element name="Location">
					<xsl:element name="adrInp4:OKATO">
						<xsl:value-of select="OKATO"/>
					</xsl:element>
					<xsl:element name="adrInp4:KLADR">
						<xsl:value-of select="KLADR"/>
					</xsl:element>
					<xsl:element name="adrInp4:Region">
						<xsl:value-of select="Region"/>
					</xsl:element>
					<xsl:if test="City/@Name!=''">
						<xsl:element name="adrInp4:City">
							<xsl:attribute name="Name"><xsl:value-of select="City/@Name"/></xsl:attribute>
							<xsl:attribute name="Type"><xsl:value-of select="City/@Type"/></xsl:attribute>
						</xsl:element>
					</xsl:if>
					<xsl:if test="Street/@Name!=''">
						<xsl:element name="adrInp4:Street">
							<xsl:attribute name="Name"><xsl:value-of select="Street/@Name"/></xsl:attribute>
							<xsl:attribute name="Type"><xsl:value-of select="Street/@Type"/></xsl:attribute>
						</xsl:element>
					</xsl:if>
					<xsl:if test="Level1/@Value!=''">
						<xsl:element name="adrInp4:Level1">
							<xsl:attribute name="Value"><xsl:value-of select="Level1/@Value"/></xsl:attribute>
							<xsl:choose>
								<xsl:when test="Level1/@Type='вл'">
									<xsl:attribute name="Type"><xsl:value-of select="'влд'"/></xsl:attribute>
								</xsl:when>
								<xsl:when test="Level1/@Type='уч'">
										<xsl:attribute name="Type"><xsl:value-of select="'уч-к'"/></xsl:attribute>
								</xsl:when>
								<xsl:otherwise>
									<xsl:attribute name="Type"><xsl:value-of select="Level1/@Type"/></xsl:attribute>
								</xsl:otherwise>
							</xsl:choose>
						</xsl:element>
					</xsl:if>
					<xsl:if test="Level2/@Value">
						<xsl:element name="adrInp4:Level2">
							<xsl:attribute name="Value"><xsl:value-of select="Level2/@Value"/></xsl:attribute>
							<xsl:choose>
								<xsl:when test="Level2/@Type='корп'">
									<xsl:attribute name="Type"><xsl:value-of select="'к'"/></xsl:attribute>							
								</xsl:when>
								<xsl:when test="Level2/@Type='строение'">
									<xsl:attribute name="Type"><xsl:value-of select="'стр'"/></xsl:attribute>							
								</xsl:when>
							<xsl:otherwise>
								<xsl:attribute name="Type"><xsl:value-of select="Level2/@Type"/></xsl:attribute>
							</xsl:otherwise>
							</xsl:choose>	
						</xsl:element>
					</xsl:if>
					<xsl:if test="Note!=''">
						<xsl:element name="adrInp4:Note">
							<xsl:value-of select="Note"/>
						</xsl:element>
					</xsl:if>
					<!--<xsl:if test="Other!=''">
					  <xsl:element name="adrInp4:Other">
					      <xsl:value-of select="Other"/>
					  </xsl:element>
					</xsl:if>-->
				</xsl:element>
			</xsl:if>
		</xsl:for-each>
	</xsl:template>
	<!--  Техплан 3 версии -->
	<xsl:template match="TP">
		<xsl:variable name="vCodeType" select="name(node())"/>
		<StatementGKN>
			<xsl:attribute name="GUID"><xsl:value-of select="$GUId_TP"/></xsl:attribute>
			<xsl:attribute name="NameSoftware">Федеральная служба государственной регистрации кадастра и картографии</xsl:attribute>
			<xsl:attribute name="VersionSoftware">1.0.9</xsl:attribute>
			<xsl:call-template name="BuildTitle">
				<xsl:with-param name="pCode" select="$vCodeType"/>
			</xsl:call-template>
			<xsl:element name="Statement">
				<xsl:variable name="_name" select="name(node()/node()/node())"/>
				<xsl:choose>
					<xsl:when test="substring($_name,0,6) ='Exist'">
						<xsl:call-template name="_StatementGKU">
							<xsl:with-param name="pTypeProj" select="$vTP"/>
							<xsl:with-param name="pTypeAction" select="$vExist"/>
						</xsl:call-template>
					</xsl:when>
					<xsl:otherwise>
						<xsl:call-template name="_StatementGKU">
							<xsl:with-param name="pTypeProj" select="$vTP"/>
							<xsl:with-param name="pTypeAction" select="$vNew"/>
						</xsl:call-template>
					</xsl:otherwise>
				</xsl:choose>
			</xsl:element>
			<xsl:call-template name="BuildDeclarentAsPerson">
				<xsl:with-param name="pSurname" select="$Surname"/>
				<xsl:with-param name="pFirst" select="$First"/>
				<xsl:with-param name="pPatronumic" select="$Patronumic"/>
				<xsl:with-param name="pPasSerial" select="$Serial"/>
				<xsl:with-param name="pPasNumber" select="$Number"/>
				<xsl:with-param name="pPasDate" select="$DateDoc"/>
				<xsl:with-param name="pPasIssuqe" select="$Issuqe"/>
				<xsl:with-param name="pRegion" select="$DocRegion"/>
				<xsl:with-param name="pLocation" select="$DocNote"/>
				<xsl:with-param name="pEmail" select="$Email"/>
				<xsl:with-param name="pSnils" select="$Snils"/>
			</xsl:call-template>
			<xsl:call-template name="BuildAppiliedDocuments">
				<xsl:with-param name="pCode" select="$vCodeType"/>
			</xsl:call-template>
			<xsl:element name="Consent">
				<xsl:element name="ProcessingPersonalData">Подтверждаю свое согласие, а также согласие представляемого мною лица, на обработку персональных данных  (сбор,  систематизацию,  накопление,  хранение,   уточнение   (обновление,   изменение), использование, распространение (в том числе передачу), обезличивание, блокирование,  уничтожение персональных данных, а также иных действий, необходимых  для  обработки  персональных   данных в рамках предоставления органами кадастрового учета, в соответствии с законодательством Российской Федерации государственных услуг), в том числе  в  автоматизированном  режиме,  включая  принятие решений на их основе органом кадастрового учета, в целях предоставления государственной услуги</xsl:element>
				<xsl:element name="ConfirmationReliability">Настоящим подтверждаю: сведения, включенные в заявление, относящиеся к моей личности и представляемому мною лицу, а также внесенные мною ниже, достоверны. Документы (копии документов), приложенные к заявлению, соответствуют требованиям,  установленным законодательством  Российской  Федерации,  на  момент  представления  заявления  эти   документы действительны и содержат достоверные сведения</xsl:element>
			</xsl:element>
			<!--/xsl:element-->
		</StatementGKN>
	</xsl:template>
	<!--*********ЗАЯВЛЕНИЕ МЕЖЕВОГО ПЛАНА*********-->
	<!--замечания-->
	<!--*21.07.2016 в 1 уровне адреса, пребразуется "вл" в "влд" (из 6 версии МП в заявление 1й версии) -->
	<!--*29.06.15 для обозначений образуемых ЗУ необходимо использовать краткую форму вида ":ЗУ1" а не полное обозначение с кад. номером. исходного ЗУ -->
	<!--*26.06.15 В случае уточнения сведений ВСЕГДА в списке изменений один единственный код - "общие свдения об об...." - это не совем правильно-->
	<!--*26.06.15 В качестве ID объектов используються их кадастровые номера-->
	<!--*26.06.15 нерешена проблема с повторяющимися смежниками в заявлении-->
	<!--*26.06.15 не формируется заявление на образрование ЧЗУ-->
	<!--26.06.15 иные ненайденные косяки :)-->
	<!--MP_V04-->
	<xsl:template match="MP" mode="MP">
		<xsl:variable name="vCodeType" select="name(node()/node())"/>
		<StatementGKN>
			<xsl:attribute name="GUID"><xsl:value-of select="$GUId_MP"/></xsl:attribute>
			<xsl:attribute name="NameSoftware">Федеральная служба государственной регистрации кадастра и картографии</xsl:attribute>
			<xsl:attribute name="VersionSoftware">1.0.9</xsl:attribute>
			<xsl:text>
				<!-- 82000000000
  -->
			</xsl:text>
			<xsl:call-template name="BuildTitle">
				<xsl:with-param name="pCode" select="$vCodeType"/>
			</xsl:call-template>
			<xsl:element name="Statement">
				<xsl:choose>
					<xsl:when test="$vCodeType=$vFormParcel">
						<xsl:call-template name="_StatementGKU">
							<xsl:with-param name="pTypeAction" select="$vNew"/>
							<xsl:with-param name="pTypeProj" select="$vMP"/>
						</xsl:call-template>
					</xsl:when>
					<xsl:otherwise>
						<xsl:call-template name="_StatementGKU">
							<xsl:with-param name="pTypeAction" select="$vExist"/>
							<xsl:with-param name="pTypeProj" select="$vMP"/>
						</xsl:call-template>
					</xsl:otherwise>
				</xsl:choose>
			</xsl:element>
			<xsl:call-template name="BuildDeclarentAsPerson">
				<xsl:with-param name="pSurname" select="$Surname"/>
				<xsl:with-param name="pFirst" select="$First"/>
				<xsl:with-param name="pPatronumic" select="$Patronumic"/>
				<xsl:with-param name="pPasSerial" select="$Serial"/>
				<xsl:with-param name="pPasNumber" select="$Number"/>
				<xsl:with-param name="pPasDate" select="$DateDoc"/>
				<xsl:with-param name="pPasIssuqe" select="$Issuqe"/>
				<xsl:with-param name="pRegion" select="$DocRegion"/>
				<xsl:with-param name="pLocation" select="$DocNote"/>
				<xsl:with-param name="pEmail" select="$Email"/>
				<xsl:with-param name="pSnils" select="$Snils"/>
			</xsl:call-template>
			<xsl:call-template name="BuildAppiliedDocuments">
				<xsl:with-param name="pCode" select="$vCodeType"/>
			</xsl:call-template>
			<xsl:element name="Consent">
				<xsl:element name="ProcessingPersonalData">Подтверждаю свое согласие, а также согласие представляемого мною лица, на обработку персональных данных  (сбор,  систематизацию,  накопление,  хранение,   уточнение   (обновление,   изменение), использование, распространение (в том числе передачу), обезличивание, блокирование,  уничтожение персональных данных, а также иных действий, необходимых  для  обработки  персональных   данных в рамках предоставления органами кадастрового учета, в соответствии с законодательством Российской Федерации государственных услуг), в том числе  в  автоматизированном  режиме,  включая  принятие решений на их основе органом кадастрового учета, в целях предоставления государственной услуги</xsl:element>
				<xsl:element name="ConfirmationReliability">Настоящим подтверждаю: сведения, включенные в заявление, относящиеся к моей личности и представляемому мною лицу, а также внесенные мною ниже, достоверны. Документы (копии документов), приложенные к заявлению, соответствуют требованиям,  установленным законодательством  Российской  Федерации,  на  момент  представления  заявления  эти   документы действительны и содержат достоверные сведения</xsl:element>
			</xsl:element>
			<!--/xsl:element-->
		</StatementGKN>
	</xsl:template>
	<!--титул документа-->
	<xsl:template name="BuildTitle">
		<xsl:element name="Title">
			<xsl:element name="RecipientName">
				<xsl:value-of select="$Region_doc"/>
			</xsl:element>
			<xsl:element name="RecipientType">Орган кадастрового учёта</xsl:element>
		</xsl:element>
	</xsl:template>
	<!--заявитель - физическое лицо-->
	<xsl:template name="BuildDeclarentAsPerson">
		<xsl:param name="pSurname"/>
		<xsl:param name="pFirst"/>
		<xsl:param name="pPatronumic"/>
		<xsl:param name="pPasSerial"/>
		<xsl:param name="pPasNumber"/>
		<xsl:param name="pPasDate"/>
		<xsl:param name="pPasIssuqe"/>
		<xsl:param name="pRegion"/>
		<xsl:param name="pLocation"/>
		<xsl:param name="pEmail"/>
		<xsl:param name="pSnils"/>
		<xsl:element name="Declarants">
			<xsl:element name="Declarant">
				<xsl:attribute name="declarantKind">357001000000</xsl:attribute>
				<xsl:element name="gen:Person">
					<xsl:element name="fio:FamilyName">
						<xsl:value-of select="$pSurname"/>
					</xsl:element>
					<xsl:element name="fio:FirstName">
						<xsl:value-of select="$pFirst"/>
					</xsl:element>
					<xsl:element name="fio:Patronymic">
						<xsl:value-of select="$pPatronumic"/>
					</xsl:element>
					<xsl:element name="fl2:Document">
						<xsl:element name="fl2:CodeDocument">008001001000</xsl:element>
						<xsl:element name="fl2:Series">
							<xsl:value-of select="$pPasSerial"/>
						</xsl:element>
						<xsl:element name="fl2:Number">
							<xsl:value-of select="$pPasNumber"/>
						</xsl:element>
						<xsl:element name="fl2:Date">
							<xsl:value-of select="$pPasDate"/>
						</xsl:element>
						<xsl:element name="fl2:IssueOrgan">
							<xsl:value-of select="$pPasIssuqe"/>
						</xsl:element>
					</xsl:element>
					<xsl:element name="fl2:Telephone"/>
					<xsl:element name="fl2:SNILS">
						<xsl:value-of select="$pSnils"/>
					</xsl:element>
					<!--TODO : проверить дату  -->
				</xsl:element>
				<xsl:element name="gen:IncomingDate">
					<xsl:value-of select="$Curr_Date"/>
				</xsl:element>
			</xsl:element>
		</xsl:element>
	</xsl:template>
	<!--объекты заявки-->
	<xsl:template name="BuildObject">
		<xsl:param name="pCode"/>
		<xsl:element name="Object">
			<xsl:element name="Parcels">
				<xsl:choose>
					<!--учточнение-->
					<xsl:when test="$pCode=$vSpecifyParcel">
						<xsl:apply-templates select="Package/SpecifyParcel" mode="parcel"/>
					</xsl:when>
					<!--образование-->
					<xsl:when test="$pCode=$vFormParcel">
						<xsl:apply-templates select="Package/FormParcels" mode="parcel"/>
					</xsl:when>
				</xsl:choose>
			</xsl:element>
		</xsl:element>
	</xsl:template>
	<xsl:template name="WriteParcel">
		<xsl:param name="pID"/>
		<xsl:param name="pExist"/>
		<!--		<xsl:param name="pGUID"/>-->
		<xsl:param name="pCadNum"/>
		<xsl:param name="pDesignation"/>
		<xsl:param name="pLocation"/>
		<xsl:element name="Parcel">
			<xsl:attribute name="temp_id"><xsl:value-of select="$pID"/></xsl:attribute>
			<xsl:attribute name="exist"><xsl:value-of select="$pExist"/></xsl:attribute>
			<xsl:attribute name="GUID"><xsl:value-of select="usr:generateGUID()"/></xsl:attribute>
			<xsl:if test="$pCadNum">
				<xsl:element name="CadastralNumber">
					<xsl:value-of select="$pCadNum"/>
				</xsl:element>
			</xsl:if>
			<xsl:if test="$pLocation">
				<xsl:copy-of select="$pLocation"/>
			</xsl:if>
			<xsl:element name="Obj_Kind">002001001000<!--Земельный участок-->
			</xsl:element>
			<xsl:element name="Designation">
				<xsl:value-of select="$pDesignation"/>
			</xsl:element>
		</xsl:element>
	</xsl:template>
	<!--приложенные документы-->
	<xsl:template name="BuildAppiliedDocuments">
		<xsl:param name="pCode"/>
		<xsl:element name="AppliedDocuments">
			<xsl:if test="$PDFIS = '1'">
				<xsl:call-template name="WriteAppiliedDocumentPDF">
					<xsl:with-param name="pCode" select="'558401000000'"/>
					<xsl:with-param name="pName" select="'Доверенность'"/>
					<xsl:with-param name="pNumber" select="$PdfNumber"/>
					<xsl:with-param name="pDate" select="$PDFDate"/>
					<xsl:with-param name="pIssueOrgan" select="$PDFIssueQue"/>
					<xsl:with-param name="pAppliedFile" select="$PDFName"/>
				</xsl:call-template>
			</xsl:if>
			<xsl:choose>
				<xsl:when test="$pCode=$vSpecifyParcel">
					<!--межевой план-->
					<xsl:call-template name="WriteAppiliedDocument">
						<xsl:with-param name="pCode" select="'558203000000'"/>
						<xsl:with-param name="pName" select="'Межевой план'"/>
						<xsl:with-param name="pDate" select="$Curr_Date"/>
						<xsl:with-param name="pIssueOrgan"/>
						<xsl:with-param name="pAppliedFile" select="concat('GKUZU_',$NameMp,'.zip')"/>
					</xsl:call-template>
				</xsl:when>
				<xsl:when test="$pCode='SubParcels'">
					<!--межевой план-->
					<xsl:call-template name="WriteAppiliedDocument">
						<xsl:with-param name="pCode" select="'558203000000'"/>
						<xsl:with-param name="pName" select="'Межевой план'"/>
						<xsl:with-param name="pDate" select="$Curr_Date"/>
						<xsl:with-param name="pIssueOrgan"/>
						<xsl:with-param name="pAppliedFile" select="concat('GKUZU_',$NameMp,'.zip')"/>
					</xsl:call-template>
				</xsl:when>
				<xsl:when test="$pCode=$vFormParcel">
					<!--межевой план-->
					<xsl:call-template name="WriteAppiliedDocument">
						<xsl:with-param name="pCode" select="'558203000000'"/>
						<xsl:with-param name="pName" select="'Межевой план'"/>
						<xsl:with-param name="pDate" select="$Curr_Date"/>
						<xsl:with-param name="pIssueOrgan" select="$Issuqe"/>
						<xsl:with-param name="pAppliedFile" select="concat('GKUZU_',$NameMp,'.zip')"/>
					</xsl:call-template>
				</xsl:when>
				<xsl:otherwise>
					<xsl:variable name="_name" select="name(node()/node()/node())"/>
					<xsl:choose>
						<xsl:when test="substring($_name,0,6) ='Exist'">
							<!--межевой план-->
							<xsl:call-template name="WriteAppiliedDocument">
								<xsl:with-param name="pCode" select="'558211010000'"/>
								<xsl:with-param name="pName" select="'Технический план'"/>
								<xsl:with-param name="pDate" select="$Curr_Date"/>
								<xsl:with-param name="pIssueOrgan"/>
								<xsl:with-param name="pAppliedFile" select="concat('GKUOKS_',$NameTP,'.zip')"/>
							</xsl:call-template>
						</xsl:when>
						<xsl:otherwise>
							<!--межевой план-->
							<xsl:call-template name="WriteAppiliedDocument">
								<xsl:with-param name="pCode" select="'558211010000'"/>
								<xsl:with-param name="pName" select="'Технический план'"/>
								<xsl:with-param name="pDate" select="$Curr_Date"/>
								<xsl:with-param name="pIssueOrgan" select="$Issuqe"/>
								<xsl:with-param name="pAppliedFile" select="concat('GKUOKS_',$NameTP,'.zip')"/>
							</xsl:call-template>
						</xsl:otherwise>
					</xsl:choose>
				</xsl:otherwise>
			</xsl:choose>
		</xsl:element>
	</xsl:template>
	<xsl:template name="WriteAppiliedDocumentPDF">
		<xsl:param name="pCode"/>
		<xsl:param name="pName"/>
		<xsl:param name="pNumber"/>
		<xsl:param name="pDate"/>
		<xsl:param name="pIssueOrgan"/>
		<xsl:param name="pOriginal" select="'1'"/>
		<xsl:param name="pOriginal_Sheet" select="'1'"/>
		<xsl:param name="pAppliedFileType" select="'02'"/>
		<xsl:param name="pAppliedFile"/>
		<xsl:element name="AppliedDocument">
			<xsl:element name="doc2:CodeDocument">
				<xsl:value-of select="$pCode"/>
			</xsl:element>
			<xsl:element name="doc2:Name">
				<xsl:value-of select="$pName"/>
			</xsl:element>
			<xsl:element name="doc2:Number">
				<xsl:value-of select="$pNumber"/>
			</xsl:element>
			<xsl:element name="doc2:Date">
				<xsl:value-of select="$pDate"/>
			</xsl:element>
			<xsl:element name="doc2:IssueOrgan">
				<xsl:value-of select="$pIssueOrgan"/>
			</xsl:element>
			<xsl:if test="$pAppliedFile">
				<xsl:element name="doc2:AppliedFile">
					<xsl:attribute name="Kind"><xsl:value-of select="$pAppliedFileType"/></xsl:attribute>
					<xsl:attribute name="Name"><xsl:value-of select="$pAppliedFile"/></xsl:attribute>
				</xsl:element>
			</xsl:if>
			<xsl:element name="Quantity">
				<xsl:element name="qua:Original">
					<xsl:attribute name="Quantity"><xsl:value-of select="$pOriginal"/></xsl:attribute>
					<xsl:attribute name="QuantitySheet"><xsl:value-of select="$pOriginal_Sheet"/></xsl:attribute>
				</xsl:element>
			</xsl:element>
		</xsl:element>
	</xsl:template>
	<xsl:template name="WriteAppiliedDocument">
		<xsl:param name="pCode"/>
		<xsl:param name="pName"/>
		<xsl:param name="pNumber" select="'1'"/>
		<xsl:param name="pDate"/>
		<xsl:param name="pIssueOrgan"/>
		<xsl:param name="pOriginal" select="'1'"/>
		<xsl:param name="pOriginal_Sheet" select="'1'"/>
		<xsl:param name="pAppliedFileType" select="'02'"/>
		<xsl:param name="pAppliedFile"/>
		<xsl:element name="AppliedDocument">
			<xsl:element name="doc2:CodeDocument">
				<xsl:value-of select="$pCode"/>
			</xsl:element>
			<xsl:element name="doc2:Name">
				<xsl:value-of select="$pName"/>
			</xsl:element>
			<xsl:element name="doc2:Number">
				<xsl:value-of select="$pNumber"/>
			</xsl:element>
			<xsl:element name="doc2:Date">
				<xsl:value-of select="$pDate"/>
			</xsl:element>
			<xsl:element name="doc2:IssueOrgan">-</xsl:element>
			<xsl:if test="$pAppliedFile">
				<xsl:element name="doc2:AppliedFile">
					<xsl:attribute name="Kind"><xsl:value-of select="$pAppliedFileType"/></xsl:attribute>
					<xsl:attribute name="Name"><xsl:value-of select="$pAppliedFile"/></xsl:attribute>
				</xsl:element>
			</xsl:if>
			<xsl:element name="Quantity">
				<xsl:element name="qua:Original">
					<xsl:attribute name="Quantity"><xsl:value-of select="$pOriginal"/></xsl:attribute>
					<xsl:attribute name="QuantitySheet"><xsl:value-of select="$pOriginal_Sheet"/></xsl:attribute>
				</xsl:element>
			</xsl:element>
		</xsl:element>
	</xsl:template>
	<xsl:template name="BuildStatement">
		<xsl:param name="pCode"/>
		<xsl:element name="StatementGKU">
			<xsl:choose>
				<xsl:when test="$pCode =$vSpecifyParcel">
					<xsl:call-template name="BuildGKUIS">
						<xsl:with-param name="pCadNum" select="$vSpecifyParcel"/>
					</xsl:call-template>
				</xsl:when>
				<xsl:when test="$pCode= $vExistBuilding">
					<xsl:call-template name="BuildGKUIS">
						<xsl:with-param name="pCadNum" select="..//..//../@CadastralNumber"/>
					</xsl:call-template>
				</xsl:when>
			</xsl:choose>
			<xsl:element name="Delivery">
				<xsl:element name="LinkEmail">
					<xsl:value-of select="$Email"/>
				</xsl:element>
			</xsl:element>
		</xsl:element>
	</xsl:template>
	<xsl:template name="BuildGKUIS">
		<xsl:param name="pCadNum"/>
		<xsl:element name="GKUIS">
			<xsl:element name="CadastralNumber">
				<xsl:value-of select="$pCadNum"/>
			</xsl:element>
		</xsl:element>
		<xsl:call-template name="BuildConstruction"/>
	</xsl:template>
	<xsl:template name="BuildConstruction">
		<xsl:choose>
			<xsl:when test="$pIsUpdate = chAddress">
				<xsl:element name="chAddress">true</xsl:element>
			</xsl:when>
			<xsl:when test="$pIsUpdate = chAssignation">
				<xsl:element name="chAssignation">true</xsl:element>
			</xsl:when>
			<xsl:when test="$pIsUpdate = chLevel">
				<xsl:element name="chLevel">true</xsl:element>
			</xsl:when>
			<xsl:when test="$pIsUpdate = chCharacteristic">
				<xsl:element name="chCharacteristic">true</xsl:element>
			</xsl:when>
			<xsl:when test="$pIsUpdate = chName">
				<xsl:element name="chName">true</xsl:element>
			</xsl:when>
		</xsl:choose>
	</xsl:template>
</xsl:stylesheet>
