<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:fn="http://www.w3.org/2006/xpath-functions" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xalan="xalan://" extension-element-prefixes="xalan" xmlns:guid="clitype:System.Guid?partialname=mscorlib" exclude-result-prefixes="guid" xmlns:ms="urn:schemas-microsoft-com:xslt" xmlns:usr="urn:the-xml-files:xslt">
	<xsl:output method="xml" indent="yes"/>
	<ms:script language="C#" implements-prefix="usr"><![CDATA[
public string generateGUID() { return Guid.NewGuid().ToString();}
]]></ms:script>
	<xsl:strip-space elements="*"/>
	<!--параметры для заполнения-->
	<xsl:param name="Curr_Date">2015-06-18</xsl:param>
	<xsl:param name="Curr_Time">18:10:43</xsl:param>
	<xsl:param name="_GUID_DO"/>
	
		<xsl:param name="CadNum"></xsl:param>
    	<xsl:param name="Other"></xsl:param>
	
	<xsl:template match="/">
		<xsl:apply-templates select="PropsDeclarent"/>
	</xsl:template>
	<xsl:template match="PropsDeclarent">
		<RequestGKN>
			<xsl:element name="eDocument">
					<xsl:attribute name="Version">2</xsl:attribute>
					<xsl:attribute name="GUID"><xsl:value-of select="$_GUID_DO"/></xsl:attribute>
    				<Recipient Name=""/>
				</xsl:element>
			<Request>
				<Declarant declarant_kind="357099000000">
					<Person>
						<FIO>
							<Surname><xsl:value-of select="zFIO"/></Surname>
							<First><xsl:value-of select="zName"/></First>
						</FIO>
						<Document>
							<Code_Document>008001001000</Code_Document>
							<Number><xsl:value-of select="zNumber"/></Number>
							<Date><xsl:value-of select="zDATEDoc"/>+04:00</Date>
						</Document>
						<E-mail><xsl:value-of select="zEmail"/></E-mail>
					</Person>
				</Declarant>
				<RequiredData>
					<KPT>
						<CadastralNumber><xsl:value-of select="$CadNum"/></CadastralNumber>
						<Orient><xsl:value-of select="$Other"/></Orient>
					</KPT>
				</RequiredData>
				<Delivery>
					<LinkE_mail><xsl:value-of select="zEmail"/></LinkE_mail>
				</Delivery>
				<Applied_Documents>
					<Applied_Document>
						<Code_Document>558101010000</Code_Document>
						<Number><xsl:value-of select="$_GUID_DO"/></Number>
						<Date><xsl:value-of select="$Curr_Date"/>+03:00</Date>
						<Quantity>
							<Original Quantity="1" Quantity_Sheet="1"/>
						</Quantity>
					</Applied_Document>
				</Applied_Documents>
			</Request>
		</RequestGKN>
	</xsl:template>
	</xsl:stylesheet>
