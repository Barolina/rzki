﻿using SQLiteHelperTestApp;
using System;
using System.Collections.Generic;
using System.Data.SqlTypes;
using System.IO;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Xml;
using System.Xml.Serialization;

#region Класс по работе с локальными настройками приложения
#endregion

namespace PortalRosreestr
{
    class SettHelper
    {
        public static Props props = new Props(); //экземпляр класса с настройками 
    }

    [Serializable]
    public class PropsSettField
    {
        //Путь до файла настроек
        public String XMLFileName = Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData) + @"\PortalRosreestr\PortalRosreestrSettings.xml";
        public String PatchBD = Application.StartupPath + @"\Data\Portal.db"; 
        public Boolean isFilterEmail = false;
        public Boolean isFilterYes = false;
        public Boolean isFilterError = false;
        public Boolean isFilterActiv = false;
    }

    public class PropsDeclarent
    {
        //данные заявителя для заказа сведений 
        public String XMLFileName = Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData) + @"\PortalRosreestr\PortalRosreestrDeclarent.xml";
        public String zFIO = "";
        public String zName = "";
        public String zDATEDoc = "";
        public String zNumber = "";
        public String zEmail = "";
    }

    [Serializable]
    public class GeneralSettings
    {
        public String  XMLFileName = Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData) + @"\PortalRosreestr\PortalSettings.xml";      
        public Boolean isShowSettings = true; //показ форму настроек к базе 
        public Boolean isShowSettCert = true; // показ формы настроек кадастрового энеженера
    }

    [Serializable]
    //Класс определяющий какие настройки есть в программе
    public class PropsCertFields
    {        
        // public String XMLFileName = Environment.CommandLine + "\\PortalRosreestrSettings.xml";
        //Чтобы добавить настройку в программу просто добавьте суда строку вида -
        //public ТИП ИМЯ_ПЕРЕМЕННОЙ = значение_переменной_по_умолчанию;
        public String TextValue = @"File Settings";
        public String EmailPerson = "test@email.ru";

        public Boolean isDublicatPack = false;
        public String FIO = "";
        public String Name = "";
        public String SurName = "";
        public String Snils = "";
        public String DateDOC = "";
        public String Seria = "";
        public String Number = "";
        public String Issuque = "";
        public String Addres = "";
        public String Telefon = "";
        public String Region = "1";

        public String PDFNumber = "";
        public String PDfDate = "";
        public String PdfIssuque = "";
        
        public void Init()
        {
          TextValue = @"File Settings";
          EmailPerson = "test@email.ru";
          isDublicatPack = false;
          FIO = "";
          Name = "";
          SurName = "";
          Snils = "";
          DateDOC = "";
          Seria = "";
          Number = "";
          Issuque = "";
          Addres = "";
          Telefon = "";
          Region = "1";
        }
    }


    //Класс работы с настройками
    public class Props
    {
        public PropsSettField  SettFields;
        public PropsCertFields CertFields;
        public GeneralSettings GeneralSett;
        public PropsDeclarent DeclarentFields;
        public Props()
        {
            SettFields = new PropsSettField();
            CertFields = new PropsCertFields();
            GeneralSett = new GeneralSettings();
            DeclarentFields = new PropsDeclarent();

        }
        //Запист настроек в файл        

        public void WriteXml()
        {
            XmlSerializer ser = new XmlSerializer(typeof(PropsSettField));
          //  ThreadExceptionHandler.logger.Info("Path " + Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData) + @"\PortalRosreestr\PortalRosreestrSettings.xml");
            TextWriter writer = new StreamWriter(Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData) + @"\PortalRosreestr\PortalRosreestrSettings.xml");
            ser.Serialize(writer, SettFields);
            writer.Close();
        }
        public void WrireDeclarentXMl()
        {
            XmlSerializer ser = new XmlSerializer(typeof(PropsDeclarent));
            TextWriter writer = new StreamWriter(Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData) + @"\PortalRosreestr\PortalRosreestrDeclarent.xml");
            ser.Serialize(writer, DeclarentFields);
            writer.Close();

        }

        public void WriteXMlGeneral ()
        {
            XmlSerializer ser = new XmlSerializer(typeof(GeneralSettings));
            TextWriter writer = new StreamWriter(Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData) + @"\PortalRosreestr\PortalSettings.xml");
            ser.Serialize(writer, GeneralSett);
            writer.Close();        
        }
        public void WriteBD()
        {
                using (var outputStream = new MemoryStream())
                {
                    using (var writer = XmlWriter.Create(outputStream))
                    {
                        var serializer = new XmlSerializer(typeof(PropsCertFields));
                        serializer.Serialize(writer, CertFields);
                    }
                    outputStream.Position = 0;
                    using (var reader = XmlReader.Create(outputStream))
                    {
                        var xml = new SqlXml(reader);
                        Dictionary<string, object> dic = new Dictionary<string, object>();
                        dic["nameXML"] = xml.Value;
                        dic["idCERT"] = CurrentCert.SerialNumber;
                        if (!FrPortalRosreestr.sqlStruct.FindCertByID())        
                             FrPortalRosreestr.sqlStruct.InsertEngener(dic);
                        else
                             FrPortalRosreestr.sqlStruct.UpdateEngener(dic);
                    }        
                }
        }

        //Чтение настроек из файла
        public void ReadXml()
        {
            if (File.Exists(SettFields.XMLFileName))
            {
                XmlSerializer ser = new XmlSerializer(typeof(PropsSettField));
                TextReader reader = new StreamReader(Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData) + @"\PortalRosreestr\PortalRosreestrSettings.xml");
                SettFields = ser.Deserialize(reader) as PropsSettField;
                reader.Close();
            }

        }
        public void ReadDeclarentXml()
        {
            if (File.Exists(DeclarentFields.XMLFileName))
            {
                XmlSerializer ser = new XmlSerializer(typeof(PropsDeclarent));
                TextReader reader = new StreamReader(Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData) + @"\PortalRosreestr\PortalRosreestrDeclarent.xml");
                DeclarentFields = ser.Deserialize(reader) as PropsDeclarent;
                reader.Close();
            }

        }

        public void ReadXMlGeneral()
        {
            if (File.Exists(GeneralSett.XMLFileName))
            {
                XmlSerializer ser = new XmlSerializer(typeof(GeneralSettings));
                TextReader reader = new StreamReader(Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData) + @"\PortalRosreestr\PortalSettings.xml");
                GeneralSett = ser.Deserialize(reader) as GeneralSettings;
                reader.Close();
            }
        }
        public void ReadBD()
        {
            byte[] xml = FrPortalRosreestr.sqlStruct.CertXMlbyID();
            if (xml == null)
            {
                CertFields.Init();
                return;
            }
            using (var outputStream = new MemoryStream())
            {
                MemoryStream memory = new MemoryStream(xml);
                memory.Position = 0;
                using (var reader = XmlReader.Create(memory))
                {
                    var serializer = new XmlSerializer(typeof(PropsCertFields));
                    CertFields = serializer.Deserialize(reader) as PropsCertFields;
                }
            }
        }

        public void writeSetting()
        {
                SettHelper.props.GeneralSett.isShowSettings = FrPortalRosreestr.frSettings.chkShow.Checked;
                SettHelper.props.GeneralSett.isShowSettCert = FrPortalRosreestr.frSettCert.chkShow.Checked; 
                SettHelper.props.WriteXMlGeneral();
        }

        //Чтение настроек
        public void readSetting()
        {
            try
            {
                SettHelper.props.ReadXMlGeneral();
                FrPortalRosreestr.frSettings.chkShow.Checked = SettHelper.props.GeneralSett.isShowSettings;
                FrPortalRosreestr.frSettCert.chkShow.Checked = SettHelper.props.GeneralSett.isShowSettCert;
            }
            catch (Exception e)
            {
                
            }
        }
    }
}
