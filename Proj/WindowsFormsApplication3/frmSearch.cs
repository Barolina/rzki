/*
 * Author: Iv�n Loire www.dotneat.net
 * Use this code at your own risk for whatever you want, with either comercial purposes or not.
 */

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using PortalRosreestr;
using System.Collections;

namespace PortalRosreestr
{
    public partial class frmSearch : Form
    {
        private TextBox txtStatus;
        private Label label5;
        private TextBox txtEmail;
        private Label label4;
        private TextBox txtRequestNumber;
        private Label label3;
        private TextBox txtNamePack;
        private Label label1;
        //Search context event frmGrid wil subscribe to in order to recieve "filter changed" notifications dynamically.
        internal event SearchContextChangedHandler TextChanged;



        /// <summary>
        /// Build controls based on fields collection
        /// </summary>
        /// <param name="fields"></param>
        private void BuildControls(List<field> fields) {
            txtNamePack.Tag = "NamePack";
            txtRequestNumber.Tag = "RequestNumber";
            txtStatus.Tag = "Status";
            txtEmail.Tag = "Email";
            //dtmPack.Tag = "DateDispath";
        }



        /// <summary>
        /// Constructor, we build the controls just once.
        /// </summary>
        /// <param name="fields"></param>
        public frmSearch(List<field> fields)
        {
            InitializeComponent();
            BuildControls(fields);
        }



        /// <summary>
        /// Returns field collection with filter values
        /// </summary>
        /// <returns></returns>
        private List<field> GetFilterValues() {
            List<field> fields=new List<field>();
            
            foreach(Control ctrl in this.Controls){
                if (ctrl.GetType() == typeof(TextBox))
                {
                    field f=new field();
                    f.Field=ctrl.Tag.ToString();
                    f.Value=ctrl.Text;
                    fields.Add(f);                    
                }
                else if (ctrl.GetType()==typeof(DateTimePicker))
                {
                  //  field f = new field();
                  //  f.Field = ctrl.Tag.ToString();
                  //  f.Value = dtmPack.Value.ToShortDateString();
                  //  fields.Add(f);                          
                }
            }
            return fields;
        }


        
        // Raise event to parent form if textbox content changes
        private void textBox_TextChanged(object sender, EventArgs e)
        {
            if (TextChanged != null)
                TextChanged(GetFilterValues());
        }


        #region Shortcuts
        private void frmSearch_KeyDown(object sender, KeyEventArgs e)
        {
            if ((e.KeyCode == Keys.Escape) || (e.KeyCode == Keys.Enter))
                Close();
        }
        #endregion

        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmSearch));
            this.txtStatus = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.txtEmail = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.txtRequestNumber = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.txtNamePack = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // txtStatus
            // 
            this.txtStatus.Location = new System.Drawing.Point(12, 148);
            this.txtStatus.Name = "txtStatus";
            this.txtStatus.Size = new System.Drawing.Size(354, 20);
            this.txtStatus.TabIndex = 19;
            this.txtStatus.TextChanged += new System.EventHandler(this.textBox_TextChanged);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label5.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(1)))), ((int)(((byte)(85)))), ((int)(((byte)(177)))));
            this.label5.Location = new System.Drawing.Point(9, 132);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(41, 13);
            this.label5.TabIndex = 18;
            this.label5.Text = "������";
            // 
            // txtEmail
            // 
            this.txtEmail.Location = new System.Drawing.Point(12, 109);
            this.txtEmail.Name = "txtEmail";
            this.txtEmail.Size = new System.Drawing.Size(354, 20);
            this.txtEmail.TabIndex = 17;
            this.txtEmail.TextChanged += new System.EventHandler(this.textBox_TextChanged);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label4.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(1)))), ((int)(((byte)(85)))), ((int)(((byte)(177)))));
            this.label4.Location = new System.Drawing.Point(9, 93);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(35, 13);
            this.label4.TabIndex = 16;
            this.label4.Text = "E-mail";
            // 
            // txtRequestNumber
            // 
            this.txtRequestNumber.Location = new System.Drawing.Point(12, 31);
            this.txtRequestNumber.Name = "txtRequestNumber";
            this.txtRequestNumber.Size = new System.Drawing.Size(354, 20);
            this.txtRequestNumber.TabIndex = 15;
            this.txtRequestNumber.TextChanged += new System.EventHandler(this.textBox_TextChanged);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label3.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(1)))), ((int)(((byte)(85)))), ((int)(((byte)(177)))));
            this.label3.Location = new System.Drawing.Point(12, 15);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(80, 13);
            this.label3.TabIndex = 14;
            this.label3.Text = "����� ������";
            // 
            // txtNamePack
            // 
            this.txtNamePack.Location = new System.Drawing.Point(12, 70);
            this.txtNamePack.Name = "txtNamePack";
            this.txtNamePack.Size = new System.Drawing.Size(354, 20);
            this.txtNamePack.TabIndex = 11;
            this.txtNamePack.TextChanged += new System.EventHandler(this.textBox_TextChanged);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label1.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(1)))), ((int)(((byte)(85)))), ((int)(((byte)(177)))));
            this.label1.Location = new System.Drawing.Point(9, 54);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(95, 13);
            this.label1.TabIndex = 10;
            this.label1.Text = "�������� ������";
            // 
            // frmSearch
            // 
            this.ClientSize = new System.Drawing.Size(377, 177);
            this.Controls.Add(this.txtStatus);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.txtEmail);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.txtRequestNumber);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.txtNamePack);
            this.Controls.Add(this.label1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "frmSearch";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.KeyDown += new System.Windows.Forms.KeyEventHandler(this.frmSearch_KeyDown_1);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        private void frmSearch_KeyDown_1(object sender, KeyEventArgs e)
        {
            if ((e.KeyCode == Keys.Escape) || (e.KeyCode == Keys.Enter))
                Close(); 
        }

        private void button1_Click(object sender, EventArgs e)
        {
            
        }

    }
}