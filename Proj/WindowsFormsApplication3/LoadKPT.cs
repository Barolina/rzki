﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.IO;
using Ionic.Zip;


using PortalRosreestr;

namespace PortalRosreestr
{
    class LoadKPT
    {
         public string  KodRegionByCadNum(string cadNum)
         {
             if (cadNum == "") return "";
             return cadNum.Substring(0, 2);
         }
         
        private string RegionByCadNum(string cadNum)
         {
             var items = FrPortalRosreestr.portalRosreestr.ReadXsd(Application.StartupPath + @"\\DATA\\adresCod.xsd"); ///преобразование в читаемый вид
             var match = items.Where(val => val.Key == KodRegionByCadNum(cadNum));
             if (match.Count() > 0) return match.First().Value.ToString().Substring(3);
             else return match.ToString().Substring(3);
         }

        /// <summary>
        ///   Формируем заявления  - ипользуя преобразования xslt и xml данные заявителя
        /// </summary>
        public string createReqKPT(string CadNum)
        {
       
            try
            {
                string xmlDeclarent = SettHelper.props.DeclarentFields.XMLFileName;
                string filesXSl = Environment.CurrentDirectory + Consts.DIRECT_INFO+  Consts.xsltKPT;

                string newGuid = System.Guid.NewGuid().ToString();
                
               
                string req_tempDir = Path.GetTempPath() + "req_" + newGuid;
                if (!Directory.Exists(req_tempDir)) Directory.CreateDirectory(req_tempDir);


                string fileREs = req_tempDir + "\\" + "req_" + newGuid + ".xml";

                if (File.Exists(fileREs)) { File.Delete(fileREs); };
                FrPortalRosreestr.portalRosreestr.XmlTransformReqKPT(xmlDeclarent, fileREs, filesXSl, newGuid,CadNum,RegionByCadNum(CadNum));///

                // архивируем и отправляем в папку reQ_/////
                FrPortalRosreestr.portalRosreestr.CompressPack(Path.GetFileName(req_tempDir) + ".zip", req_tempDir, Path.GetTempPath());
                if (Directory.Exists(req_tempDir)) Directory.Delete(req_tempDir, true);
                return Path.GetTempPath() + "\\" + Path.GetFileName(req_tempDir) + ".zip";
            }
            catch (Exception ex)
            {
                ThreadExceptionHandler.ShowEventDialog("Сформировать пакет не удалось!",ex);
                return "";
            }
        }
    }
}
