﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.SQLite;
using SQLiteHelperTestApp;
using System.Windows.Forms;
using System.Data;
using System.IO;

#region Класс для  работы с базой данных SQLLITE 
#endregion

namespace PortalRosreestr
{
    public partial class sqlStructurecs
    {
        static SQLiteConnectionStringBuilder connBuilder;


         void ReadConfiguration()
        {
            config.DatabaseFile = SettHelper.props.SettFields.PatchBD;

            connBuilder = new SQLiteConnectionStringBuilder();
            connBuilder.DataSource = config.DatabaseFile;

            connBuilder.Version = 3; //эти настройки были
            //Set page size to NTFS cluster size = 4096 bytes
            connBuilder.PageSize = 4096;
            connBuilder.CacheSize = 10000;
            connBuilder.JournalMode = SQLiteJournalModeEnum.Off;
            connBuilder.Pooling = true;
            connBuilder.LegacyFormat = false;
            connBuilder.DefaultTimeout = 500;
        }

         void  OpenBD()
        {
            ReadConfiguration();
            if (!File.Exists(config.DatabaseFile)) CreateDatabase(config.DatabaseFile);
            try
            {
                using (SQLiteConnection conn = new SQLiteConnection(connBuilder.ToString() + ";synchronous = OFF"))
                {
                    conn.Open();
                    conn.Close();
                }
            }
            catch (Exception ex)
            {
                ThreadExceptionHandler.logger.Error(ex.ToString() + " " + ex.StackTrace.ToString());
                MessageBox.Show(ex.Message.ToString());
            }
        }
        /// <summary>
        /// Подключенеи к базе
        /// </summary>
        public sqlStructurecs()
        {
            OpenBD();
        }

        public bool BD_DeletePack(string RequestNumber)
        {
            using (SQLiteConnection conn = new SQLiteConnection(connBuilder.ToString()))
            {
                using (SQLiteCommand cmd = new SQLiteCommand())
                {
                    if (conn.State == ConnectionState.Closed) conn.Open();
                    cmd.Connection = conn;
                    cmd.CommandText = "delete from OutRequest where requestNumber = '" + RequestNumber + "'";
                    try /// удялем сначало ответы по данной заявки
                    {
                        string res = Convert.ToString(cmd.ExecuteScalar());
                    }
                    catch (Exception ex)
                    {
                        ThreadExceptionHandler.logger.Error("Сбой при удалении заявки OUTPack  " + RequestNumber + ex.Message.ToString() + " " + ex.StackTrace.ToString());
                    };
                    cmd.CommandText = "delete from request where  RequestNumber = '" + RequestNumber + "' and Certificate = '" + CurrentCert.SerialNumber + "'; ";
                    try
                    {

                        string res = Convert.ToString(cmd.ExecuteScalar());
                        return true;
                    }
                    catch (Exception ex)
                    {
                        ThreadExceptionHandler.logger.Error("Сбой при удалении заявки  " + RequestNumber + ex.Message.ToString() + " " + ex.StackTrace.ToString());
                        return false;
                    }
                    finally
                    {
                        cmd.Dispose();
                        conn.Close();
                    }
                }
            }
        }

        


        /// <summary>
        ///  ID последней заявки добавленной в базу
        /// </summary>
        /// <returns></returns>
        public byte[] CertXMlbyID()
        {
            using (SQLiteConnection conn = new SQLiteConnection(connBuilder.ToString()))
            {
                using (SQLiteCommand cmd = new SQLiteCommand())
                {
                    if (conn.State == ConnectionState.Closed) conn.Open();
                    cmd.Connection = conn;
                    SQLiteHelper sh = new SQLiteHelper(cmd);
                    try
                    {
                        cmd.CommandText = "select eng.nameXML from EngenersCert eng  where eng.idCERT= '" + CurrentCert.SerialNumber + "' ";
                        byte[] res = (byte[])cmd.ExecuteScalar();
                        return res;

                    }
                    catch (Exception ex)
                    {
                        ThreadExceptionHandler.logger.Info(ex.Message + " " + ex.StackTrace.ToString());
                        return null;
                    }
                    finally
                    {
                        cmd.Dispose();
                        conn.Close();
                    }
                }
            }
        }

        public  void CreateDatabase(string path)
        {
            try
            {
                using (SQLiteConnection conn = new SQLiteConnection(connBuilder.ToString() + ";synchronous = OFF"))
                {
                    FileInfo file = new FileInfo(Path.GetDirectoryName(Application.ExecutablePath) + "\\Data\\sqlPortal.sql");
                    string script = file.OpenText().ReadToEnd();
                    SQLiteCommand cmd = null; ;
                    try
                    {
                        conn.Open();
                        cmd = new SQLiteCommand();
                        cmd.Connection = conn;
                        cmd.CommandText = script;
                        cmd.ExecuteNonQuery();
                       // MessageBox.Show("DB Uploaded.");
                    }
                    finally
                    {
                        cmd.Dispose();
                        conn.Close();
                        conn.Dispose();
                        file.OpenText().Close();
                    }                  
                }
            }
            catch (Exception ex)
            {
                ThreadExceptionHandler.logger.Error(ex.ToString() + " " + ex.StackTrace.ToString());
                MessageBox.Show(ex.Message.ToString());
            }
        }
        /// <summary>
        ///   Получение EventID по номеру заявки
        /// </summary>
        /// <param name="RequestNumber"></param>
        /// <param name="Date"></param>
        /// <returns></returns>
        public string EventIDByRequest(string requestNumber)
        {
            using (SQLiteConnection conn = new SQLiteConnection(connBuilder.ToString()))
            {
                using (SQLiteCommand cmd = new SQLiteCommand())
                {
                    if (conn.State == ConnectionState.Closed) conn.Open();
                    cmd.Connection = conn;
                    SQLiteHelper sh = new SQLiteHelper(cmd);
                    DataTable dt = null;
                    try
                    {
                        dt = sh.Select("Select guid from Request where RequestNumber = '" + requestNumber + "' and  Certificate = '" + CurrentCert.SerialNumber + "' ");
                        if (dt.Rows.Count <= 0) return "";
                        return dt.Select().First()[0].ToString();
                    }
                    catch (Exception ex)
                    {
                        ThreadExceptionHandler.logger.Info(ex.Message + " " + ex.StackTrace.ToString());
                        return "";
                    }
                    finally
                    {
                        dt.Dispose();
                        cmd.Dispose();
                        conn.Close();
                    }

                }
            }
        }

        public string StatusByRequest(string requestNumber)
        {
            using (SQLiteConnection conn = new SQLiteConnection(connBuilder.ToString()))
            {
                using (SQLiteCommand cmd = new SQLiteCommand())
                {
                    if (conn.State == ConnectionState.Closed) conn.Open();
                    cmd.Connection = conn;
                    SQLiteHelper sh = new SQLiteHelper(cmd);
                    DataTable dt = null;
                    try
                    {
                        dt = sh.Select("Select status from Request where RequestNumber = '" + requestNumber + "' and  Certificate = '" + CurrentCert.SerialNumber + "' ");
                        if (dt.Rows.Count <= 0) return "";
                        return dt.Select().First()[0].ToString();
                    }
                    catch (Exception ex)
                    {
                        ThreadExceptionHandler.logger.Info(ex.Message + " " + ex.StackTrace.ToString());
                        return "";
                    }
                    finally
                    {
                        dt.Dispose();
                        cmd.Dispose();
                        conn.Close();
                    }

                }
            }
        }


        public string FileNameByRequestNumber(string RequestNumber)
        {
            using (SQLiteConnection conn = new SQLiteConnection(connBuilder.ToString()))
            {
                using (SQLiteCommand cmd = new SQLiteCommand())
                {
                    if (conn.State == ConnectionState.Closed) conn.Open();
                    cmd.Connection = conn;
                    SQLiteHelper sh = new SQLiteHelper(cmd);
                    DataTable dt = null;
                    try
                    {
                        dt = sh.Select("Select fileNameReq from Request where RequestNumber = '" + RequestNumber + "';");
                        if (dt.Rows.Count <= 0) return "Пакет отправлен не нами или удален!";
                        return dt.Select().First()[0].ToString();
                    }
                    catch (Exception ex)
                    {
                        ThreadExceptionHandler.logger.Info(ex.Message + " " + ex.StackTrace.ToString());
                        return "";
                    }
                    finally
                    {
                        dt.Dispose();
                        cmd.Dispose();
                        conn.Close();
                    }
                }
            }
        }

        /// <summary>
        ///  ID последней заявки добавленной в базу
        /// </summary>
        /// <returns></returns>
        public bool FindCertByID()
        {
            using (SQLiteConnection conn = new SQLiteConnection(connBuilder.ToString()))
            {
                using (SQLiteCommand cmd = new SQLiteCommand())
                {
                    if (conn.State == ConnectionState.Closed) conn.Open();

                    cmd.Connection = conn;
                    cmd.CommandText = "select count(eng.nameXML) from EngenersCert eng  where eng.idCERT= '" + CurrentCert.SerialNumber + "' ";
                    try
                    {
                        long res = (long)cmd.ExecuteScalar();
                        if ((res != null) && (res > 0)) return true;
                        else return false;
                    }
                    catch (Exception ex)
                    {
                        ThreadExceptionHandler.logger.Info(ex.Message + " " + ex.StackTrace.ToString());
                        return false;
                    }
                    finally
                    {
                        cmd.Dispose();
                        conn.Close();
                    }
                }
            }
        }

        /// <summary>
        /// Получить писок заявок из базы
        /// </summary>
        /// <param name="Certificate">Номер кадастрового инженера</param>
        /// <returns></returns>
        public DataTable GetEvents(string Certificate)
        {
            using (SQLiteConnection conn = new SQLiteConnection(connBuilder.ToString()))
            {
                using (SQLiteCommand cmd = new SQLiteCommand())
                {
                    try
                    {
                        if (conn.State == ConnectionState.Closed) conn.Open();
                        cmd.Connection = conn;
                        string sql = "Select IDRosreestr,DateDispath,RequestNumber,NamePack,Status,Email, ";
                        sql = sql + "(select count(*) from OutRequest where r.ID = OutRequest.idRostreestr) countOutFile, data,Region from Request r where ";
                        sql = sql + " Certificate = '" + Certificate + "' and ((isArchive  is null)) ";
                        if (SettHelper.props.SettFields.isFilterEmail)
                            sql = sql + " and Email = '" + SettHelper.props.CertFields.EmailPerson + "' ";
                        sql = sql + "ORDER by IDRosreestr desc;";
                        cmd.CommandText = sql;
                        SQLiteDataAdapter ad = new SQLiteDataAdapter(cmd);
                        DataTable table = null; ;
                        try
                        {
                            table = new DataTable();
                            ad.Fill(table);
                            return table;
                        }
                        finally
                        {
                            ad.Dispose();
                            table.Dispose();
                        }
                    }
                    finally
                    {
                        cmd.Dispose();
                        conn.Close();
                    }
                }

            }
        }

        public DataTable LoadKodyPLay(string Certificate)
        {
            using (SQLiteConnection conn = new SQLiteConnection(connBuilder.ToString()))
            {
                using (SQLiteCommand cmd = new SQLiteCommand())
                {
                    try
                    {
                        if (conn.State == ConnectionState.Closed) conn.Open();
                        cmd.Connection = conn;
                        string sql = "Select NamePack,substr(Status,88 ,19) oplata, Status,RequestNumber ";
                        sql = sql + " from Request r where Status like '%Ожидание оплаты%' and ";
                        sql = sql + " Certificate = '" + Certificate + "' and ((isArchive  is null)) ";
                        sql = sql + "ORDER by IDRosreestr desc;";
                        cmd.CommandText = sql;
                        SQLiteDataAdapter ad = new SQLiteDataAdapter(cmd);
                        DataTable table = null; ;
                        try
                        {
                            table = new DataTable();
                            ad.Fill(table);
                            return table;
                        }
                        finally
                        {
                            ad.Dispose();
                            table.Dispose();
                        }
                    }
                    finally
                    {
                        cmd.Dispose();
                        conn.Close();
                    }
                }

            }
        }

        /// <summary>
        /// Поиск по номеру заявки 
        /// </summary>
        /// <param name="requestNumber"></param>
        /// <returns>ID</returns>
        public int IdByRequestNumber(string requestNumber)
        {
            using (SQLiteConnection conn = new SQLiteConnection(connBuilder.ToString()))
            {
                using (SQLiteCommand cmd = new SQLiteCommand())
                {
                    if (conn.State == ConnectionState.Closed) conn.Open();
                    cmd.Connection = conn;
                    SQLiteHelper sh = new SQLiteHelper(cmd);
                    DataTable dt = null;
                    try
                    {
                        dt = sh.Select("Select id from Request where RequestNumber = '" + requestNumber + "' and  Certificate = '" + CurrentCert.SerialNumber + "' ");
                        if (dt.Rows.Count <= 0) return -1;
                        return Convert.ToInt32(dt.Select().First()[0].ToString());
                    }
                    catch (Exception ex)
                    {
                        ThreadExceptionHandler.logger.Info(ex.Message + " " + ex.StackTrace.ToString());
                        return -1;
                    }
                    finally
                    {
                        dt.Dispose();
                        cmd.Dispose();
                        conn.Close();
                    }

                }
            }
        }

        public long IDRosreestrByRequestNumber(string requestNumber)
        {
            using (SQLiteConnection conn = new SQLiteConnection(connBuilder.ToString()))
            {
                using (SQLiteCommand cmd = new SQLiteCommand())
                {
                    if (conn.State == ConnectionState.Closed) conn.Open();
                    cmd.Connection = conn;
                    SQLiteHelper sh = new SQLiteHelper(cmd);
                     DataTable dt = null;
                    try
                    {
                        dt = sh.Select("Select IDRosreestr from Request where RequestNumber = '" + requestNumber + "' and  Certificate = '" + CurrentCert.SerialNumber + "' ");
                        if (dt.Rows.Count <= 0) return -1;
                        return Convert.ToInt64(dt.Select().First()[0]);
                    }
                    catch (Exception ex)
                    {
                        ThreadExceptionHandler.logger.Info(ex.Message + " " + ex.StackTrace.ToString());
                        return -1;
                    }
                    finally
                    {
                        dt.Dispose();
                        cmd.Dispose();
                        conn.Close();
                    }
                }
            }
        }
        //insert into  EngenersCert (nameXML) VALUES('sdf')
        public string InsertEngener(Dictionary<string, object> dic)
        {
            using (SQLiteConnection conn = new SQLiteConnection(connBuilder.ToString()))
            {
                using (SQLiteCommand cmd = new SQLiteCommand())
                {
                    if (conn.State == ConnectionState.Closed) conn.Open();
                    cmd.Connection = conn;
                    SQLiteHelper sh = new SQLiteHelper(cmd);
                    sh.BeginTransaction();
                    try
                    {
                        sh.Insert("EngenersCert", dic);
                        sh.Commit();
                        return " ";
                    }
                    catch (Exception ex)
                    {
                        ThreadExceptionHandler.logger.Error("Произошел откат изменений " + ex.Message.ToString() + " " + ex.StackTrace.ToString());
                        return ex.Message;
                    }
                    finally
                    {
                        cmd.Dispose();
                        conn.Close();
                    }
                }
            }
        }

        public string InsertOutData(Dictionary<string, object> dic)
        {
            using (SQLiteConnection conn = new SQLiteConnection(connBuilder.ToString()))
            {
                using (SQLiteCommand cmd = new SQLiteCommand())
                {
                    if (conn.State == ConnectionState.Closed) conn.Open();
                    cmd.Connection = conn;
                    SQLiteHelper sh = new SQLiteHelper(cmd);
                    sh.BeginTransaction();
                    try
                    {
                        sh.Insert("OutRequest", dic);
                        sh.Commit();
                        return " ";
                    }
                    catch (Exception ex)
                    {
                        ThreadExceptionHandler.logger.Error("Произошел откат изменений " + ex.Message.ToString() + " " + ex.StackTrace.ToString());
                        // sh.Rollback();
                        return ex.Message;
                    }
                    finally
                    {
                        cmd.Dispose();
                        conn.Close();
                    }
                }
            }
        }

        /// <summary>
        ///  Добавить данные заявки в базу 
        /// </summary>
        /// <param name="dic">Список значение </param>
        /// <returns>ничего</returns>
        public string InsertRequest(Dictionary<string, object> dic)
        {
            using (SQLiteConnection conn = new SQLiteConnection(connBuilder.ToString()))
            {
                using (SQLiteCommand cmd = new SQLiteCommand())
                {
                    if (conn.State == ConnectionState.Closed) conn.Open();
                    cmd.Connection = conn;
                    SQLiteHelper sh = new SQLiteHelper(cmd);
                    sh.BeginTransaction();
                    try
                    {
                        sh.Insert("Request", dic);
                        sh.Commit();
                        return "";
                    }
                    catch (Exception ex)
                    {
                        ThreadExceptionHandler.logger.Error("Произошел откат изменений " + ex.Message.ToString() + " " + ex.StackTrace.ToString());
                        //sh.Rollback();
                        return ex.Message;
                    }
                    finally
                    {
                        cmd.Dispose();
                        conn.Close();
                    }
                }
            }
        }
        public bool IsExistOutPack(string RequestNumber, string Date)
        {
            using (SQLiteConnection conn = new SQLiteConnection(connBuilder.ToString()))
            {
                using (SQLiteCommand cmd = new SQLiteCommand())
                {
                    if (conn.State == ConnectionState.Closed) conn.Open();
                    cmd.Connection = conn;
                    cmd.CommandText = "select count(Date) from OutRequest ot where ot.Date = '" + Date + "' and ot.requestNumber = '" + RequestNumber + "';";
                    try
                    {
                        int res = Convert.ToInt32(cmd.ExecuteScalar());
                        if ((res != null) && (res > 0)) return true;
                        else return false;
                    }
                    catch (Exception ex)
                    {
                        ThreadExceptionHandler.logger.Info(ex.Message + " " + ex.StackTrace.ToString());
                        return false;
                    }
                    finally
                    {
                        cmd.Dispose();
                        conn.Close();
                    }
                }
            }
        }

        /// <summary>
        ///    Проверка заявки - отправлялась ли она через нашу программу или нет 
        ///   
        /// </summary>
        /// <param name="RequestNumber"></param>
        /// <returns></returns>
        public bool IsMainRequest(string RequestNumber)
        {
            using (SQLiteConnection conn = new SQLiteConnection(connBuilder.ToString()))
            {
                using (SQLiteCommand cmd = new SQLiteCommand())
                {
                    if (conn.State == ConnectionState.Closed) conn.Open();
                    cmd.Connection = conn;
                    cmd.CommandText = "Select Region from Request where RequestNumber = '" + RequestNumber + "';";
                    try
                    {
                        long res = (long)cmd.ExecuteScalar();
                        if ((res != null)&&(res != -1)) return true;
                        else return false;
                    }
                    catch (Exception ex)
                    {
                       // ThreadExceptionHandler.logger.Info(ex.Message + " " + ex.StackTrace.ToString() + " " + ex.InnerException.Message + " " + ex.Source.ToString());                        
                        return false;                     
                    }
                    finally
                    {
                        cmd.Dispose();
                        conn.Close();
                    }
                }
            }
        }

        /// <summary>
        ///  ID последней заявки добавленной в базу
        /// </summary>
        /// <returns></returns>
        public string LastEventID()
        {
            using (SQLiteConnection conn = new SQLiteConnection(connBuilder.ToString()))
            {
                using (SQLiteCommand cmd = new SQLiteCommand())
                {
                    if (conn.State == ConnectionState.Closed) conn.Open();
                    cmd.Connection = conn;
                    SQLiteHelper sh = new SQLiteHelper(cmd);
                     DataTable dt  = null;
                    try
                    {
                        dt = sh.Select("SELECT max(IDRosreestr) FROM Request where Certificate = '" + CurrentCert.SerialNumber + "' ");
                        string ch = dt.Select().First()[0].ToString();
                        if (ch == "") return "0";
                        else return ch;                                                
                    }
                    catch (Exception ex)
                    {
                        ThreadExceptionHandler.logger.Info(ex.Message + " " + ex.StackTrace.ToString());
                        return "0";
                    }
                    finally
                    {
                        dt.Dispose();
                        cmd.Dispose();
                        conn.Close();
                    }
                }
            }
        }
        public byte[] LoadFileByOutFile(string RequestNumber)
        {
            using (SQLiteConnection conn = new SQLiteConnection(connBuilder.ToString()))
            {
                using (SQLiteCommand cmd = new SQLiteCommand())
                {
                    if (conn.State == ConnectionState.Closed) conn.Open();
                    cmd.Connection = conn;
                    cmd.CommandText = "Select file from OutRequest ot where RequestNumber = '" + RequestNumber + "'  order by ot.id desc;";
                    try
                    {
                        byte[] res = (byte[])cmd.ExecuteScalar();
                        return res;
                    }
                    catch 
                    {                        
                        return null;
                    }
                    finally
                    {
                        cmd.Dispose();
                        conn.Close();
                    }
                }
            }
        }

        public byte[] LoadFileByRequestNumber(string RequestNumber)
        {
            using (SQLiteConnection conn = new SQLiteConnection(connBuilder.ToString()))
            {
                using (SQLiteCommand cmd = new SQLiteCommand())
                {
                    if (conn.State == ConnectionState.Closed) conn.Open();
                    cmd.Connection = conn;
                    cmd.CommandText = "Select FILE from Request where RequestNumber = '" + RequestNumber + "';";
                    try
                    {
                        byte[] res = (byte[])cmd.ExecuteScalar();
                        return res;
                    }
                    catch
                    {
                        return null;
                    }
                    finally
                    {
                        cmd.Dispose();
                        conn.Close();
                    }
                }
            }
        }

        /// <summary>
        ///  Получить список платежей
        /// </summary>
        /// <returns></returns>
        public string[] GetListPlata()
        {
            using (SQLiteConnection conn = new SQLiteConnection(connBuilder.ToString()))
            {
                using (SQLiteCommand cmd = new SQLiteCommand())
                {
                    if (conn.State == ConnectionState.Closed) conn.Open();
                    cmd.Connection = conn;
                    cmd.CommandText = "Select NamePack, Status,RequestNumber from Request where Status like '%Ожидание оплаты%'  and Certificate = '" + CurrentCert.SerialNumber + "';";
                    try
                    {
                        string[] res = (string[])cmd.ExecuteScalar();
                        return res;
                    }
                    catch
                    {
                        return null;
                    }
                    finally
                    {
                        cmd.Dispose();
                        conn.Close();
                    }
                }
            }
        }

        public bool UpdateDateByGUID(Dictionary<string, object> dic, string id)
        {
            using (SQLiteConnection conn = new SQLiteConnection(connBuilder.ToString()))
            {
                using (SQLiteCommand cmd = new SQLiteCommand())
                {
                    if (conn.State == ConnectionState.Closed) conn.Open();
                    cmd.Connection = conn;
                    SQLiteHelper sh = new SQLiteHelper(cmd);
                    sh.BeginTransaction();
                    try
                    {
                        sh.Update("Request", dic, "ID", id);
                        sh.Commit();
                        return true;
                    }
                    catch (Exception ex)
                    {
                        ThreadExceptionHandler.logger.Info(ex.Message + " Поэтому и произошел откат изменений!");
                       // sh.Rollback();
                        return false;
                    }
                    finally
                    {
                        cmd.Dispose();
                        conn.Close();
                    }
                }
            }
        }

        /// <summary>
        ///  Изменение данных по ID
        /// </summary>
        /// <param name="dic"></param>
        /// <returns></returns>
        public bool UpdateDateByID(Dictionary<string, object> dic, string id)
        {
            using (SQLiteConnection conn = new SQLiteConnection(connBuilder.ToString()))
            {
                using (SQLiteCommand cmd = new SQLiteCommand())
                {
                    if (conn.State == ConnectionState.Closed) conn.Open();
                    cmd.Connection = conn;
                    SQLiteHelper sh = new SQLiteHelper(cmd);
                    sh.BeginTransaction();
                    try
                    {
                        sh.Update("Request", dic, "ID", id);
                        sh.Commit();
                        return true;
                    }
                    catch (Exception ex)
                    {
                        ThreadExceptionHandler.logger.Error("Произошел откат изменений " + ex.Message.ToString() + " " + ex.StackTrace.ToString());                        
                       // sh.Rollback();
                        return false;
                    }
                    finally
                    {
                        cmd.Dispose();
                        conn.Close();
                    }
                }
            }
        }
        public string UpdateEngener(Dictionary<string, object> dic)
        {
            using (SQLiteConnection conn = new SQLiteConnection(connBuilder.ToString()))
            {
                using (SQLiteCommand cmd = new SQLiteCommand())
                {
                    if (conn.State == ConnectionState.Closed) conn.Open();
                    cmd.Connection = conn;
                    SQLiteHelper sh = new SQLiteHelper(cmd);
                    sh.BeginTransaction();
                    try
                    {
                        sh.Update("EngenersCert", dic, "idCERT", CurrentCert.SerialNumber);
                        sh.Commit();
                        return " ";
                    }
                    catch (Exception ex)
                    {
                        ThreadExceptionHandler.logger.Error("Произошел откат изменений " + ex.Message.ToString() + " " + ex.StackTrace.ToString());
                        return ex.Message;
                    }
                    finally
                    {
                        cmd.Dispose();
                        conn.Close();
                    }
                }
            }
        }

        public bool UpdateFileByRequestNumber(string requestNumber, byte[] file)
        {
            using (SQLiteConnection conn = new SQLiteConnection(connBuilder.ToString()))
            {
                using (SQLiteCommand cmd = new SQLiteCommand())
                {
                    if (conn.State == ConnectionState.Closed) conn.Open();
                    cmd.Connection = conn;
                    SQLiteHelper sh = new SQLiteHelper(cmd);
                    sh.BeginTransaction();
                    try
                    {
                        var dic = new Dictionary<string, object>();
                        dic["FILE"] = file;
                        sh.Update("Request", dic, "RequestNumber", requestNumber);
                        sh.Commit();
                        return true;
                    }
                    catch (Exception ex)
                    {
                        ThreadExceptionHandler.logger.Error("Произошел откат изменений " + ex.Message.ToString() + " " + ex.StackTrace.ToString());
                       // sh.Rollback();
                        return false;
                    }

                    finally
                    {
                        cmd.Dispose();
                        conn.Close();
                    }
                }
            }
        }

        public bool UpdateStatusByRequestNumber(string requestumber)
        {
            using (SQLiteConnection conn = new SQLiteConnection(connBuilder.ToString()))
            {
                using (SQLiteCommand cmd = new SQLiteCommand())
                {
                    if (conn.State == ConnectionState.Closed) conn.Open();
                    cmd.Connection = conn;
                    SQLiteHelper sh = new SQLiteHelper(cmd);
                    sh.BeginTransaction();
                    try
                    {
                        var dic = new Dictionary<string, object>();
                        dic["isArchive"] = "true";
                        sh.Update("Request", dic, "RequestNumber", requestumber);
                        sh.Commit();
                        return true;
                    }
                    catch (Exception ex)
                    {
                        ThreadExceptionHandler.logger.Error("Произошел откат изменений " + ex.Message.ToString() + " " + ex.StackTrace.ToString());                        
                       // sh.Rollback();
                        return false;
                    }
                    finally
                    {
                        cmd.Dispose();
                        conn.Close();
                    }
                }
            }
        }
    }
}

