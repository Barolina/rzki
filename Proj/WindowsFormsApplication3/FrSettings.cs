﻿using SQLiteHelperTestApp;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlTypes;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Runtime.Serialization.Formatters.Binary;
using System.Text;
using System.Windows.Forms;
using System.Xml;
using System.Xml.Serialization;

namespace PortalRosreestr
{

    public partial class FrSettings : Form
    {
      
        //Запись настроек
        private void writeSetting()
        {
            try
            {
                SettHelper.props.SettFields.PatchBD = txtBD.Text;
                SettHelper.props.CertFields.isDublicatPack = chkTOfile.Checked;

                SettHelper.props.WriteXml();
            }
            catch (Exception e)
            {
                MessageBox.Show(e.StackTrace);
            }
        }

        //Чтение настроек
        public void readSetting()
        {
            try
            {
                SettHelper.props.ReadXml();
                txtBD.Text = SettHelper.props.SettFields.PatchBD;                
                chkTOfile.Checked = SettHelper.props.CertFields.isDublicatPack;
                checkedListBox1.SetItemChecked(0, SettHelper.props.SettFields.isFilterEmail);                
            }
            catch(Exception e)
            {
                MessageBox.Show(e.StackTrace);
            }
        }
        public FrSettings()
        {
            InitializeComponent();           
        }
      
        private void btnSelBD_Click(object sender, EventArgs e)
        {
            txtBD.Text = "";
            
            if (openFileDialog1.ShowDialog() == DialogResult.OK)
            {
                txtBD.Text = openFileDialog1.InitialDirectory + openFileDialog1.FileName;
            }
        }


        private void btnOk_Click_1(object sender, EventArgs e)
        {
            try
            {
                writeSetting();
                readSetting();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
                return;
            }
           
        }

        private void FrSettings_Shown(object sender, EventArgs e)
        {
            readSetting();
        }

        private void chkShow_CheckedChanged(object sender, EventArgs e)
        {
            SettHelper.props.writeSetting();
        }

        
    }

}
