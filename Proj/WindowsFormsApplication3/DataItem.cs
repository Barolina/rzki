﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Text;

namespace WindowsFormsDataGrid
{
    public class DataItem:INotifyPropertyChanged
    {
        private int id;
        //Если не присваивать дефолтный параметор то бедет показвать 01.01.0001 
        private DateTime date=DateTime.Now;
        private string name;
        private string number;

        [DisplayName("ID")]
        public int Id
        {
            get
            {
                return this.id;
            }
            set
            {
                this.id = value;
                this.OnPropertyChanged("Id");
            }
        }
        [DisplayName("Путь файла (вдойной клик мыши)")]
        public string Name
        {
            get
            {
                return this.name;
            }
            set
            {
                this.name = value;
                this.OnPropertyChanged("Name");
            }
        }
        [DisplayName("Дата")]
        public DateTime Date
        {
            get
            {
                return this.date;
            }
            set
            {
                this.date= value;
                this.OnPropertyChanged("Date");
            }
        }
        
        [DisplayName("Номер документа")]
        public string Number
        {
            get
            {
                return this.number;
            }
            set
            {
                this.number = value;
                this.OnPropertyChanged("Number");
            }
        }
        public static DataItem Create(int id, DateTime date, string name, string number)
        {
            DataItem di = new DataItem();
            di.id = id;
            di.date = date;
            di.name = name;
            di.number = number;
            return di;
        }

        #region Члены INotifyPropertyChanged

        public event PropertyChangedEventHandler PropertyChanged;
        private void OnPropertyChanged(String propertyName)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
            }
        }

        #endregion
    }
}
