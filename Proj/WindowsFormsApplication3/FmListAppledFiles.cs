﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace PortalRosreestr
{
    public partial class FmListAppledFiles : Form
    {
     
        DateTimePicker _date; 
        public FmListAppledFiles()
        {
            InitializeComponent();
        }

        private void txtBoxFile_DoubleClick(object sender, EventArgs e)
        {
            oFDLGZIP.Multiselect = true;
            if (oFDLGZIP.ShowDialog() == DialogResult.OK)
            {
                if (oFDLGZIP.FileNames.Count() == 1)
                    this.txtBoxFile.Text = oFDLGZIP.InitialDirectory + oFDLGZIP.FileName;
                else this.txtBoxFile.Text = "Выбранно : " + oFDLGZIP.FileNames.Count().ToString() + "файлов!";

            }
            oFDLGZIP.Multiselect = false;
        }

        private void txtPDF_DoubleClick(object sender, EventArgs e)
        {
            if (FrPortalRosreestr.portalRosreestr.frmPDfDocDev.ShowDialog() == DialogResult.OK)
            {
                txtPDF.Text = FrPortalRosreestr.portalRosreestr.frmPDfDocDev.txtPDF.Text;
            }

        }

        private void drag_frop_file(object sender, DataGridViewCellEventArgs e)
        {
            try
            {
                if ((e.ColumnIndex == 0))  //выбор файла
                {
                    if (openFileDialog1.ShowDialog() == DialogResult.OK)
                    {

                        DataGridViewRow rw = dataGridView.Rows[e.RowIndex];
                        rw.Cells[0].Value = openFileDialog1.InitialDirectory + openFileDialog1.FileName;
                    }

                }
            }
            catch (Exception ex)
            {
                ThreadExceptionHandler.ShowEventDialog("", ex);
            }

        }

        private void dataGridView_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
        }

        private void bindingSourceDataList_CurrentChanged_1(object sender, EventArgs e)
        {

        }

        /// <summary>
        /// Кинуть  файл в таблицу
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void dataGridView_DragDrop(object sender, DragEventArgs e)
        {
            if (e.Data.GetDataPresent(DataFormats.FileDrop))
            {
                string[] filePaths = (string[])(e.Data.GetData(DataFormats.FileDrop));
                foreach (string fileLoc in filePaths)
                {
                    // Code to read the contents of the text file
                    if (File.Exists(fileLoc))
                    {                        
                        var index = this.dataGridView.Rows.Add();
                        this.dataGridView.Rows[index].Cells[0].Value = fileLoc;
                        this.dataGridView.Rows[index].Cells[2].Value = "б/н";
                        this.dataGridView.Rows[index].Cells[1].Value = DateTime.Now;
                    }
                }
            }            
        }

        private void bindingSourceDataList_CurrentChanged(object sender, EventArgs e)
        {

        }

        private void dataGridView1_DragEnter(object sender, DragEventArgs e)
        {
            if (e.Data.GetDataPresent(DataFormats.FileDrop))
            {
                e.Effect = DragDropEffects.All;
            }
            else
            {
                e.Effect = DragDropEffects.None;
            }
        }

        private void dataGridView1_DragDrop(object sender, DragEventArgs e)
        {
            if (e.Data.GetDataPresent(DataFormats.FileDrop))
            {
                string[] filePaths = (string[])(e.Data.GetData(DataFormats.FileDrop));
                foreach (string fileLoc in filePaths)
                {
                    // Code to read the contents of the text file
                    if (File.Exists(fileLoc))
                    {
                        using (TextReader tr = new StreamReader(fileLoc))
                        {
                            MessageBox.Show(tr.ReadToEnd());
                        }
                    }

                }
            }
        }

        private void dataGridView_DragEnter(object sender, DragEventArgs e)
        {
            if (e.Data.GetDataPresent(DataFormats.FileDrop))
            {
                e.Effect = DragDropEffects.All;
            }
            else
            {
                e.Effect = DragDropEffects.None;
            }
        }

        private void dataGridView_CellContentDoubleClick(object sender, DataGridViewCellEventArgs e)
        {

            this.drag_frop_file(sender, e);
        }

        private void label2_Click(object sender, EventArgs e)
        {

        }

        private void button2_Click(object sender, EventArgs e)
        {
           if (!this.dataGridView.Visible)
            {
                this.dataGridView.Rows.Clear();
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {

        }
    }
}
