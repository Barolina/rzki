﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Security.Cryptography.X509Certificates;

using System.Security.Cryptography;
using System.IO;
using PortalRosreestr.ru.rosreestr.portal;
using System.Windows.Forms;
using System.Security.Cryptography.Pkcs;
using Ionic.Zip;
using System.Xml.Linq;
using SQLiteHelperTestApp;
using System.Xml;
using System.Xml.Xsl;
using System.Xml.XPath;
using NLog;
using System.Threading;
using System.Xml.Schema;
using GotDotNet.Exslt;



namespace PortalRosreestr
{
   
    public  class Rostreestr
    {
        public FrOutEvents frOutEvents;  //портал росреестра
        public FrmPDFDOC frmPDfDocDev;

        /// <summary>
        /// Выбор сертификата
        /// </summary>
        public Rostreestr()
        {
            frOutEvents = new FrOutEvents();
            frmPDfDocDev = new FrmPDFDOC(); ///если необходимо подгрузить доверенность
            if (!SetCertificat()) Application.Exit();
        }

        #region Выбран ли сертификат
        public bool isConnect()
        {
            return (Certificate.cert2 != null);
        }
        #endregion

        #region  Получение серийнного  номера сертификата
        /// <summary>
        ///   ВЫБОР СЕРТИФИКАТА, по серийному номеру
        /// </summary>
        /// <returns>true|false</returns>
       

        public bool  SetCertificat()
        {
            Certificate.cert2I = Certificate.GetCert();
            try
            {
                CurrentCert.SerialNumber = Certificate.cert2.SerialNumber.ToString();
                if (frOutEvents.externalWS2.ClientCertificates.Count >= 1)
                    frOutEvents.externalWS2.ClientCertificates.RemoveAt(0);
                frOutEvents.externalWS2.ClientCertificates.Add(Certificate.cert2);
                ThreadExceptionHandler.logger.Info("Сертификат " + CurrentCert.SerialNumber);
                return true;
            }
            catch (Exception ex)
            {
                string  err = "Нет так нет";
                ThreadExceptionHandler.logger.Error(err + " "  +ex.StackTrace+ " " + ex.Message);
                MessageBox.Show(err);
                return false;
            }                      
        }

        #endregion
        
        #region Архивация пакета
        /// <summary>
        /// Архивация пакета          
        /// Для req пришлось добавить кодировку - так как там могут встречаться файлы с русскими наименованияим
        /// </summary>
        /// <param name="NameZip">как назвать</param>
        /// <param name="InDir">что</param>
        /// <param name="OutDir">куда</param>
       
        public void CompressPack(string NameZip, string InDir, string OutDir)
        {
            if (InDir.IndexOf("req_") != -1)
            {
                using (ZipFile zp = new ZipFile(Encoding.GetEncoding(866)))
                {
                    zp.AddDirectory(InDir);
                    zp.Save((OutDir + "\\" + NameZip));
                }
            }
            else
            {
                using (ZipFile zp = new ZipFile())
                {
                    zp.AddDirectory(InDir);
                    zp.Save((OutDir + "\\" + NameZip));
                }
            }
        }

        #endregion

        #region Получить кол-во заявок на портале , для обновления
        /// <summary>
        ///  кол-во заявок на портале
        /// </summary>
        /// <returns>count|-1</returns>
        
        public int CountPortalEvents()
        {
            getEventsOut outcl = null;
            getEventsIn incl = null;
            try
            {
                incl = new getEventsIn();
                outcl = frOutEvents.externalWS2.getEvents(incl);
                if (outcl.events == null) return -1;
                else
                    return outcl.events.Count();
            }
            catch (Exception e)
            {
                return -1;
            }
        }
        #endregion

        #region Проверка сформирванного заявления относительно схемы
        /// <summary>
        ///   Проверка сформирванного заявления относительно схемы
        /// </summary>
        /// <param name="pathReqXML"></param>
        /// <returns></returns>
        private bool IsValidateReq(string pathReqXML, string pathXSD)
        {
            XDocument xDoc = new XDocument();
            xDoc = XDocument.Load(pathReqXML);
            XmlSchemaSet sh = new XmlSchemaSet();
            sh.Add("", pathXSD);
            bool res=  true;
            xDoc.Validate(sh, (sender, e) =>
            {
                ThreadExceptionHandler.logger.Error(e.Message);
                res = false;
            });

            return res;
        }
        #endregion

        #region Генерирует GUID
        /// <summary>
        ///   Генерирует GUID
        /// </summary>
        /// <returns></returns>
        protected string _GenerateGUID()
        {
            string guidGUOKS = Guid.NewGuid().ToString();
            int idnex_ = guidGUOKS.IndexOf("_");
            guidGUOKS = guidGUOKS.Substring(idnex_ + 1);
            return guidGUOKS;
        }
        #endregion

        #region Создание директории
        private void GenerateDir(string Path)
        {
            if (!Directory.Exists(Path)) Directory.CreateDirectory(Path);
            else
            {
                Directory.Delete(Path, true);
                Directory.CreateDirectory(Path);
            }            
        }
#endregion

        #region Распаковать  межевой иди техплан
        /// <summary>
        ///   Распаковать  межевой иди техплан
        /// </summary>
        /// <param name="path"></param>
        private string DecompressGKHGP(string InFileName)
        {
            try
            {
                string gk_tempDir = Path.GetTempPath() + System.Guid.NewGuid().ToString(); /// распаковываем пакет
                ZipFile zip = new ZipFile(InFileName);
                if (Directory.Exists(gk_tempDir)) Directory.Delete(gk_tempDir, true);
                zip.ExtractAll(gk_tempDir);
                zip.Dispose();
                return gk_tempDir;
            }
            catch (Exception ex)
            {
                ThreadExceptionHandler.ShowEventDialog("", ex);
                return "";
            }

        }
        #endregion

        #region Формирование пакета Req_......zip
        /// <summary>
        ///   Формирование пакета Req_......zip
        /// </summary>
        /// <param name="pathMP_TP_REG">путь к архиву G.....zip</param>
        /// <param name="codReq">код заявления - находится в свойстве Tag = TabControl</param>
        public string CreatePackReq(string pathMP_TP_REG, string codReq, bool isfixerror=false)
        {
            try
            {
                string guidGUOKS = _GenerateGUID();///получение гуида пакета                
                string req_tempDir = Path.GetTempPath() + "req_" + guidGUOKS;
                GenerateDir(req_tempDir);//создание временной директории

                //bool isNotExistXML = Path.GetExtension(pathMP_TP_REG.ToLower()) == ".PDf".ToLower();
                bool isNotExistXML = ((this.frOutEvents.path_MP_TP_Req == ""));

                string fileXMl = "";        
                string _GKZU_tempDir ="";

                if (!isNotExistXML) ///если есть  какой межевой или техплан
                {
                    _GKZU_tempDir = DecompressGKHGP(pathMP_TP_REG);
                    string[] fales = Directory.GetFiles(_GKZU_tempDir, "*.xml"); ///получаем xml пакета
                    foreach (string fale in fales)
                    {
                        fileXMl = fale + Environment.NewLine;
                    }
                }

                string filesXSl = AppDomain.CurrentDomain.BaseDirectory + "DATA\\";

                switch (codReq)
                {
                    case Consts.CODE_DOP     : filesXSl = filesXSl + Consts.xsltConversionDopReq; 
                                               if (isNotExistXML)
                                               {
                                                   filesXSl = AppDomain.CurrentDomain.BaseDirectory + "DATA\\" + Consts.xsltConversionDopReqPDF;
                                                   fileXMl = AppDomain.CurrentDomain.BaseDirectory + "DATA\\" + Consts.xsltTempXML;  //так как xml file обязательный то берем по умолчанию                                                       
                                               }

                                               break;
                    case Consts.CODE_TECHNICAL_ERROR:
                        fileXMl = AppDomain.CurrentDomain.BaseDirectory + "DATA\\" + Consts.xsltTempXML;  //так как xml file обязательный то берем по умолчанию                                                              
                        filesXSl = filesXSl + Consts.xsltTechnicalError;
                            break;
                    default                  :
                        {
                            if (isfixerror)
                                filesXSl = filesXSl + Consts.xsltFixError;
                            else
                                filesXSl = filesXSl + Consts.xsltGeneral;
                            break;
                        }
                        
                }

                string fileREs = req_tempDir + "\\" + "req_" + guidGUOKS + ".xml";

                if (File.Exists(fileREs)) { File.Delete(fileREs); };
                

                FrPortalRosreestr.portalRosreestr.XmlTransformReq(fileXMl, fileREs, filesXSl, guidGUOKS);///

                ///проверка по схеме 
               // if (IsValidateReq(fileREs, @"D:\projects\PortalRoss\Proj\WindowsFormsApplication3\bin\x86\Debug\DATA\V17_CR_ZC_REQ_Request\CR_ZC_REQ_Reqest.xsd"))
               //     MessageBox.Show("es");
               // else
               //     MessageBox.Show("net");

                // архивируем и отправляем в папку reQ_/////
                if (!isNotExistXML)
                {
                    FrPortalRosreestr.portalRosreestr.CompressPack(Path.GetFileName(pathMP_TP_REG), _GKZU_tempDir, req_tempDir);
                    if (Directory.Exists(_GKZU_tempDir)) Directory.Delete(_GKZU_tempDir, true);
                }

                if ((this.frOutEvents.countDopPDf != null) && (this.frOutEvents.countDopPDf >0 ))
                {// копируем подгруженные pdf
                    DirCopyPDF(pathMP_TP_REG, "dop", req_tempDir);
                }

                if (pathMP_TP_REG == "")//на случай если межевой план не выбран а отправляем допником только pdf одну или несколько
                {
                    string selectedPath = "";
                    var t = new Thread((ThreadStart)(() =>
                    {
                        FolderBrowserDialog fbd = new FolderBrowserDialog();
                        fbd.RootFolder = System.Environment.SpecialFolder.MyComputer;
                        fbd.Description = "Выберите каталог для хранения файла заявления!";
                        fbd.ShowNewFolderButton = true;
                        if (fbd.ShowDialog() == DialogResult.Cancel)
                            return;

                        selectedPath = fbd.SelectedPath;
                    }));

                    t.SetApartmentState(ApartmentState.STA);
                    t.Start();
                    t.Join();

                    pathMP_TP_REG = selectedPath+"\\";
                }

                FrPortalRosreestr.portalRosreestr.CompressPack(@Path.GetFileName(req_tempDir) + ".zip", @req_tempDir, @Path.GetDirectoryName(pathMP_TP_REG));                
                if (Directory.Exists(req_tempDir)) Directory.Delete(req_tempDir, true);
                return Path.GetDirectoryName(pathMP_TP_REG)+"\\"+Path.GetFileName(req_tempDir) + ".zip";
            }
            catch (Exception ex)
            {
                ThreadExceptionHandler.ShowEventDialog("Сформировать пакет не удалось!",ex);
                return "";
            }
        }

        #endregion

        #region Скопировать PDf в директорию заявления
        private void DirCopyPDF(string pathFile, string newDir, string reqDir)
        {
            if (Directory.Exists(reqDir + "\\" + newDir))
                Directory.Delete(reqDir + "\\" + newDir, true);
            Directory.CreateDirectory(reqDir + "\\" + newDir + "\\");
            int[] intAr = new int[this.frOutEvents.countDopPDf];
            
                for (int i = 0; i < this.frOutEvents.countDopPDf; i++)
                {
                    string fl = this.frOutEvents.fmPack.dataGridView.Rows[i].Cells[0].Value.ToString();
                    if (File.Exists(fl))
                    {
                        File.Copy(@fl, @reqDir + "\\" + newDir + "\\RFile"+(i+1).ToString()+".pdf", true);      
                    }
                }
                /*if (File.Exists(pathFile))
                File.Copy(@pathFile, @reqDir + "\\" + newDir + "\\RFile1.pdf", true);            */
        }
        #endregion

        #region Получить список заявок с портала
        public  List<eventStruct> ListEvents()
        {

            getEventsOut outcl = null;
            getEventsIn incl = null;
            #region connect rosreestr
            try
            {

                incl = new getEventsIn();
                outcl = frOutEvents.externalWS2.getEvents(incl);
            }
            catch (Exception ex)
            {
                ThreadExceptionHandler.logger.Debug(ex.Message + " " + ex.StackTrace.ToString());
                MessageBox.Show(ex.Message);
                return null;
            }
            #endregion

            if (outcl.events == null) return null;  ///на портале ничего нет            

            Int64 lastEventID = Convert.ToInt64(FrPortalRosreestr.sqlStruct.LastEventID());
            List<eventStruct> lstEventsOrbr = (from elem in outcl.events
                                               orderby elem.eventDate //ascending 
                                               select
                                                        new eventStruct()
                                                        {
                                                            eventID = elem.eventID,
                                                            eventDate = elem.eventDate,
                                                            eventType = elem.eventType,
                                                            requestNumber = elem.requestNumber                                                                   
                                                        }).ToList();
            return lstEventsOrbr;
        }
        #endregion

        #region Формирование  заявления по заказу КПТ
        public void XmlTransformReqKPT(string xmlFile, string reqName, string pathXslt, string guid, string CadNum, string Other)
        {
            XslTransform xslTR = new XslTransform();
            xslTR.Load(pathXslt);
            XPathDocument xpathDoc = new XPathDocument(xmlFile);
            Encoding utf8 = new UTF8Encoding(false, true);
            XmlTextWriter xmlWrite = new XmlTextWriter(reqName, null);
            //передача параметров в xslt
            XsltArgumentList argList = new XsltArgumentList();

            argList.AddParam("Curr_Date", "", DateTime.Today.ToString("yyyy-MM-dd"));
            argList.AddParam("_GUID_DO", "", guid);
            argList.AddParam("CadNum", "", CadNum);
            argList.AddParam("Other", "",Other);

            var st = new MemoryStream();

            xslTR.Transform(xpathDoc, argList, xmlWrite, null);
            xmlWrite.Close();

            StreamReader reader = new StreamReader(reqName, Encoding.UTF8);
            string str = reader.ReadToEnd();
            reader.Dispose();
            StreamWriter writer = new StreamWriter(reqName, false, utf8);
            writer.Write("<?xml version=\"1.0\" encoding=\"UTF-8\"?> " + str);
            writer.Dispose();
        }

        #endregion


        public string ListNumbersPDF()
        {
            var lstNumbers = String.Join(";", this.frOutEvents.fmPack.dataGridView.Rows.OfType<DataGridViewRow>()
                    .Where(x => x.Cells[2].Value != null)
                    .Select(x => x.Cells[2].Value.ToString()).ToArray());
            return lstNumbers;
        }

        private static void ConvertToDateTime(string value)
        {
            DateTime convertedDate;
            try
            {
                convertedDate = Convert.ToDateTime(value);
                Console.WriteLine("'{0}' converts to {1} {2} time.",
                                  value, convertedDate,
                                  convertedDate.Kind.ToString());
            }
            catch (FormatException)
            {
                Console.WriteLine("'{0}' is not in the proper format.", value);
            }
        }
        public string ListDatePDF()
        {
            var lstDate = String.Join(";", this.frOutEvents.fmPack.dataGridView.Rows.OfType<DataGridViewRow>()
                    .Where(x => x.Cells[1].Value != null)
                    .Select(x => Convert.ToDateTime(x.Cells[1].Value.ToString()).ToString("yyyy-MM-dd")).ToArray());
            return lstDate;
        }

        /// <summary>
        ///   Формирование файла заявления - req_GUID
        /// </summary>
        /// <param name="xmlFile"></param>
        /// <param name="reqName"></param>
        /// <param name="pathXslt"></param>
        /// <param name="guid"></param>
        /// <param name="requestNumber"></param>
        public void XmlTransformReq(string xmlFile, string reqName, string pathXslt, string guid, string  requestNumber ="")
        {
            XslTransform xslTR = new XslTransform();
            xslTR.Load(pathXslt);            
            XPathDocument xpathDoc = new XPathDocument(xmlFile);            
            Encoding utf8 = new UTF8Encoding(false, true);          
            XmlTextWriter xmlWrite = new XmlTextWriter(reqName,null);
            //передача параметров в xslt
            XsltArgumentList argList = new XsltArgumentList();
            
            // обязательные данные для  передачи  в заявлние 
            argList.AddParam(Consts.xslCurr_Date, "", DateTime.Today.ToString("yyyy-MM-dd"));
            argList.AddParam(Consts.xslCurr_Time, "", "18:10:43"); /// ды пусть пока так 
            argList.AddParam(Consts.xslSerial, "", SettHelper.props.CertFields.Seria);
            argList.AddParam(Consts.xslNumber, "", SettHelper.props.CertFields.Number);
            argList.AddParam(Consts.xslDateDoc, "", SettHelper.props.CertFields.DateDOC);
            argList.AddParam(Consts.xslIssuqe, "", SettHelper.props.CertFields.Issuque);
            argList.AddParam(Consts.xslDocRegion, "", SettHelper.props.CertFields.Region);
            argList.AddParam(Consts.xslDocNote, "", SettHelper.props.CertFields.Addres);


            if ((pathXslt.IndexOf("DOP_ST_REQ") != -1) || ((pathXslt.IndexOf("DOP_PDF") != -1))) argList.AddParam(Consts.xslSnils, "", SettHelper.props.CertFields.Snils);
            else argList.AddParam(Consts.xslSnils, "", SettHelper.props.CertFields.Snils.Replace(" ", "").Replace("-", ""));

            //если  уточнение  изменение  где нужно указать в связи с чем :
            TabPage selectTab = null;
            frOutEvents.cbbUpdate.Invoke((MethodInvoker)delegate
            {
                frOutEvents.tabControl2.Invoke((MethodInvoker)delegate
                {
                    
                   
                       selectTab = frOutEvents.tabControl2.SelectedTab;
                   

                    if (selectTab ==frOutEvents.tb_Exist)
                    {
                        argList.AddParam(Consts.xslvTypeUpdate, "", frOutEvents.cbbUpdate.SelectedValue.ToString());                        
                    }
                    else if (selectTab == frOutEvents.tb_Fix_Error) 
                    {
                        argList.AddParam(Consts.xslvTypeUpdate, "", frOutEvents.cbb_fix_error.SelectedValue.ToString());
                    }
                });
            });

            // данные о заявителе
            argList.AddParam(Consts.xslEmail, "", SettHelper.props.CertFields.EmailPerson);
            argList.AddParam(Consts.xslSurname, "", SettHelper.props.CertFields.FIO);
            argList.AddParam(Consts.xslFirst, "", SettHelper.props.CertFields.Name);
            argList.AddParam(Consts.xslPatronumic, "", SettHelper.props.CertFields.SurName);

            frOutEvents.cbbRegion.Invoke((MethodInvoker)delegate
            {
                argList.AddParam(Consts.xslRegion_doc, "", frOutEvents.cbbRegion.Text);
            });

          
            argList.AddParam(Consts.xslFIO_Cadastr, "", SettHelper.props.CertFields.FIO + " " + SettHelper.props.CertFields.Name + " " + SettHelper.props.CertFields.SurName);
            argList.AddParam("_GUID", "", guid);
            argList.AddParam(Consts.xslRequestNumber, "", frOutEvents.txtRequetsNumber.Text);

            // в случае  если отправляются доверенность 
            if (frOutEvents.dovPDF != "")
            {
                argList.AddParam(Consts.xslPDFIS, "", "1");
                argList.AddParam(Consts.xslPDFNumber, "", frmPDfDocDev.txtNumber.Text);
                argList.AddParam(Consts.xslPDFDate, "", frmPDfDocDev.dateTimePicker1.Value.ToString("yyyy-MM-dd"));
                argList.AddParam(Consts.xslPDFIssueQue, "", frmPDfDocDev.txtIssuque.Text);
                argList.AddParam(Consts.xslPDFName, "",  "dov\\"+Path.GetFileName(frmPDfDocDev.txtPDF.Text));
                Directory.CreateDirectory(Path.GetDirectoryName(reqName) + "\\dov\\");
                if (File.Exists(frmPDfDocDev.txtPDF.Text))
                    File.Copy(@frmPDfDocDev.txtPDF.Text, @Path.GetDirectoryName(reqName) + "\\dov\\" +@Path.GetFileName(frmPDfDocDev.txtPDF.Text),true);
                if (File.Exists(frmPDfDocDev.txtPDF.Text+".sig"))
                    File.Copy(@frmPDfDocDev.txtPDF.Text + ".sig", @Path.GetDirectoryName(reqName) + "\\dov\\" +@Path.GetFileName(frmPDfDocDev.txtPDF.Text)+".sig",true);
            }
            else
                argList.AddParam(Consts.xslPDFIS, "", "0");



            argList.AddParam(Consts.xslCountFilePDf, "",frOutEvents.countDopPDf);

            ///список файлов передавать не обязатель -  по  мы их вставим в xslt автоматически по порядковому номеру
            if (frOutEvents.countDopPDf > 0)
            {                
               argList.AddParam(Consts.xslNumbersPDF, "", ListNumbersPDF());
               argList.AddParam(Consts.xslDatesPDF, "", ListDatePDF());
              // argList.AddParam(Consts.xslDatesPDF, "", ListDatePDF());
            }
           
         

            if (selectTab == frOutEvents.tbTexError)
            {
                argList.AddParam(Consts.xslTechCadNum, "", frOutEvents.frTechicalError.txt_cadNumber.Text);
                argList.AddParam(Consts.xslTechindocs, "", frOutEvents.frTechicalError.txt_docs.Text);
                argList.AddParam(Consts.xslTechinGKn, "", frOutEvents.frTechicalError.txt_kadastr.Text);
               // frOutEvents.frTechicalError.cbb_type_error.Invoke((MethodInvoker)delegate
               // {
                    argList.AddParam(Consts.xslTechinType, "", frOutEvents.frTechicalError.cbb_type_error.SelectedValue.ToString());
                //});
                if (frOutEvents.frTechicalError.rbCadNum.Checked)
                {
                    argList.AddParam(Consts.xslTechTypeParcel, "", "Parcel");
                }
                else if (frOutEvents.frTechicalError.rbBuilding.Checked)
                {
                    argList.AddParam(Consts.xslTechTypeParcel, "", "Building");
                }
                else if (frOutEvents.frTechicalError.rbStroy.Checked)
                {
                    argList.AddParam(Consts.xslTechTypeParcel, "", "Construction");
                }
                else if (frOutEvents.frTechicalError.rbPomech.Checked)
                {
                    argList.AddParam(Consts.xslTechTypeParcel, "", "Flat");
                }
                else if (frOutEvents.frTechicalError.rb_NotStroy.Checked)
                {
                    argList.AddParam(Consts.xslTechTypeParcel, "", "Uncompleted");
                }
            }

            var st = new MemoryStream();
            argList.AddExtensionObject("http://exslt.org/dates-and-times", new ExsltDatesAndTimes());
            xslTR.Transform(xpathDoc, argList, xmlWrite, null);
            xmlWrite.Close();

            StreamReader reader = new StreamReader(reqName,Encoding.UTF8);
            string str = reader.ReadToEnd();
            reader.Dispose();
            StreamWriter writer = new StreamWriter(reqName, false,utf8);
            writer.Write("<?xml version=\"1.0\" encoding=\"UTF-8\"?> "+str); 
            writer.Dispose();
        }

        #region Плучение из строки "C1EAFD2F-C72F-4371-99AD-E087E7E1F446/STATUS/01015/1431584190000"  последней цифры
        /// <summary>
        /// Плучение из строки "C1EAFD2F-C72F-4371-99AD-E087E7E1F446/STATUS/01015/1431584190000"
        /// 1431584190000
        /// </summary>
        /// <param name="EventID"></param>
        /// <returns>1431584190000</returns>
        private Int64 GetID(string EventID )
        {
            try
            {
                if (EventID == "") return 0;
                if ((EventID.IndexOf("out") != -1) || (EventID.IndexOf("RECEIPT") != -1)) return 0;
                string iD = EventID;
                int pos = iD.LastIndexOf(@"/");
                if (pos == -1) return 0;
                else
                {
                    iD = iD.Substring(pos + 1);
                    return Convert.ToInt64(iD);
                }
            }
            catch
            {
                return 0;
            }
        }

        #endregion

        #region  Добавляются  ответные документы только те что есть у нас в таблице Request
        /// <summary>
        ///   Добавляются  ответные документы только те что есть у нас в таблице Request
        /// </summary>
        /// <param name="el"></param>
        public void InsertOutDataRosreestr(eventStruct el)
        {
            int idRequestNumber = FrPortalRosreestr.sqlStruct.IdByRequestNumber(el.requestNumber);
            if (idRequestNumber == -1) return;


            if (!FrPortalRosreestr.sqlStruct.IsExistOutPack(el.requestNumber, el.eventDate.ToString()))
            {
                var outRec = new Dictionary<string, object>();
                outRec["idRostreestr"] = idRequestNumber.ToString();
                outRec["requestNumber"] = el.requestNumber;
                outRec["Date"] = el.eventDate.ToString();

                loadEventDetailsIn lIn = new loadEventDetailsIn();
                lIn.eventID = el.eventID;
                loadEventDetailsOut lout = frOutEvents.externalWS2.loadEventDetails(lIn);

                outRec["File"] = lout.binary;
                FrPortalRosreestr.sqlStruct.InsertOutData(outRec);
                
                if (FrPortalRosreestr.sqlStruct.IsMainRequest(el.requestNumber))
                {
                    ThreadExceptionHandler.logger.Info(el.requestNumber);
                    DeleteEvent(el.eventID);
                }
            }
            else ///пакет такой у нас есть 
            {
                ///ToDO : подумать  что бы не было  лишних удалений
                if (FrPortalRosreestr.sqlStruct.IsMainRequest(el.requestNumber))
                {
                    ThreadExceptionHandler.logger.Info(el.requestNumber);
                    DeleteEvent(el.eventID);
                }
            }

        }
        #endregion


        #region  Измнение статуса заявки в базе
        /// <summary>
        ///  Измнение статуса заявки в базе 
        /// </summary>
        /// <param name="el"></param>
        public void UpdateStatus(eventStruct el)
        {
                var dic = new Dictionary<string, object>();
                string dst = "";
            
                int idRequestNumber = FrPortalRosreestr.sqlStruct.IdByRequestNumber(el.requestNumber);//поиск по базе  
                ThreadExceptionHandler.logger.Info("Поиск по базе  " + idRequestNumber.ToString());               
                if (idRequestNumber != -1)
                {
                    long _sqlIDRostrestr = FrPortalRosreestr.sqlStruct.IDRosreestrByRequestNumber(el.requestNumber);
                    long _eventID = GetID(el.eventID);
                    if (_sqlIDRostrestr < _eventID)
                    {
                        string plata = "";
                        string _status = GetPortalStatus(el.eventID, out dst, out plata);
                        if (dst == "") dst = DateTime.Today.ToString();
                        el.eventDate = DateTime.Parse(dst);

                
                        dic["GUID"] = el.eventID.ToString();
                        dic["IDRosreestr"] = GetID(el.eventID).ToString();
                        dic["DateDispath"] = el.eventDate.ToString();
                        dic["Status"] = _status;

                        if (FrPortalRosreestr.sqlStruct.IsMainRequest(el.requestNumber))
                        {
                            string _guid = FrPortalRosreestr.sqlStruct.EventIDByRequest(el.requestNumber); /// прежде чем обновить статус - удаляем с портала предыдущий статус заявки
                            if (_guid != "")
                            {
                                ThreadExceptionHandler.logger.Info("Заявки " + el.requestNumber);
                                DeleteEvent(_guid);
                            }
                        }
                        if(  FrPortalRosreestr.sqlStruct.UpdateDateByID(dic, idRequestNumber.ToString()))
                           DeleteEvent(el.eventID.ToString());
                    }
                    else  /// ну если меньше то влюбом случае удалять
                    {
                        if (_sqlIDRostrestr != _eventID)
                        {
                            if (FrPortalRosreestr.sqlStruct.IsMainRequest(el.requestNumber))
                            {
                                ThreadExceptionHandler.logger.Info("Заявки " + el.requestNumber);
                                DeleteEvent(el.eventID);
                            }
                                
                        }
                        else
                        {
                            if (_sqlIDRostrestr == _eventID)
                            {
                                string st = FrPortalRosreestr.sqlStruct.StatusByRequest(el.requestNumber);
                                if ((st.ToString().IndexOf("Ответ") != -1) ||
                                                (st.ToString().IndexOf("Выполнена") != -1)
                                                )
                                {
                                    string gd = FrPortalRosreestr.sqlStruct.EventIDByRequest(el.requestNumber);
                                    DeleteEvent(gd);
                                }
                            }
                        }

                    }
                    
                }
                else ///значит еще откуда то пришла
                {
                    ThreadExceptionHandler.logger.Info("Добавим не нашу заявку "+ el.requestNumber);
                    dic["GUID"] = el.eventID.ToString();
                    dic["IDRosreestr"] = GetID(el.eventID).ToString();
                    dic["DateDispath"] = el.eventDate.ToString();
                    string plata= "";
                    dic["Status"] = GetPortalStatus(el.eventID, out dst, out plata);
                    dic["RequestNumber"] = el.requestNumber;
                    dic["NamePack"] = "Not comment!";
                    dic["Region"] = "-1";
                    dic["Email"] = "";
                    dic["Certificate"] = CurrentCert.SerialNumber;
                    dic["Data"] = el.eventDate.ToString();
                    FrPortalRosreestr.sqlStruct.InsertRequest(dic);                    
                }
        }

        #endregion
    
        #region Сохранение ответа с росреестра
        /// <summary>
        ///   Сохранить результат
        /// </summary>
        /// <param name="EvenetID">номер завки для детализации</param>
        public void LoadFileR(string EvenetID)
        {
            try
            {
                if ((EvenetID.IndexOf("out") == -1) && (EvenetID.IndexOf("RECEIPT") == -1)) return;
                loadEventDetailsIn lIn = new loadEventDetailsIn();
                lIn.eventID = EvenetID;
                loadEventDetailsOut lout = frOutEvents.externalWS2.loadEventDetails(lIn);

                string pathFile = frOutEvents.folderBrowserDialog1.SelectedPath + "\\OUTDOC" + RandomNumberGenerator.Create().ToString() + ".zip";
                if (frOutEvents.folderBrowserDialog1.ShowDialog() == DialogResult.OK)
                {
                    File.WriteAllBytes(pathFile, lout.binary);
                }
                MessageBox.Show("Результат готов! " + pathFile);
            }
            catch(Exception ex)
            {
                MessageBox.Show("Ошибка при сохранении результата!" + ex.ToString()+ex.Message);
            }
        }
        #endregion

        #region  Получение более  подробного описания заявки -Получение статуса заявки

        /// <summary>
        /// Получение статуса заявки
        /// </summary>
        /// <param name="EventID">ID заявки</param>
        /// <param name="date">Получаем из детализации дату (так как она отличается)</param>
        /// <returns>Дата и текстовое описание заявки</returns>
        public string GetPortalStatus(string EventID, out string date,out string kodOplaty)
        {
            try
            {
                if (EventID.IndexOf("STATUS") == -1)
                {
                    date = "";
                    kodOplaty = "";
                    return null;
                }
               
                loadEventDetailsIn lIn = new loadEventDetailsIn(); ///--обращенеи к порталу
                lIn.eventID = EventID;
                loadEventDetailsOut lout = frOutEvents.externalWS2.loadEventDetails(lIn);

                if (lout.detailsXML == "") { date = ""; kodOplaty = ""; return null; } ///чтение xml 
                string codeStatus = "";
                var messageStatus = System.Xml.Linq.XElement.Parse(lout.detailsXML);

                XElement _code = messageStatus.Element(Consts.XML_ELM_CODE);
                if (_code != null) codeStatus = _code.Value.ToString();
                XElement _message = messageStatus.Element(Consts.XML_ELM_MESSAGE);
                string codeOplata = "";
                if (_message != null) codeOplata = _message.Value.ToString();
                kodOplaty = codeOplata;
                     
                date = XmlConvert.ToDateTime(messageStatus.Element(Consts.XML_ELM_DATE).Value.ToString()).ToString();

                var items = ReadXsd(Application.StartupPath + @"\\DATA\\kody_rosreestr.xsd"); ///преобразование в читаемый вид
                var match = items.Where(val => val.Key == codeStatus);
                if (match.Count() > 0) return match.First().Value.ToString() + " | "+codeOplata;
                else return match.ToString() +  " | "+codeOplata;
            }
            catch(Exception ex )
            {
                ThreadExceptionHandler.logger.Debug(ex.Message +" " +ex.StackTrace.ToString());
                date = "";
                kodOplaty = "";
                return null;
            }
        }
        #endregion

        #region Удалить заявку
        /// <summary>
        ///  Удалить заявку из списка
        /// </summary>
        /// <param name="EventID"></param>
        /// <returns>успех или нет</returns>
        public Boolean DeleteEvent(string EventID)
        {
            try
            {
                String[] eventIDs = { EventID };
                deleteEventsOut delOut = new deleteEventsOut();
                delOut = frOutEvents.externalWS2.deleteEvents(eventIDs);
                ThreadExceptionHandler.logger.Info("Удаление  номера заявки с портала успешно завершилось! "+ EventID);
                return delOut.status.result;
            }
            catch(Exception ex)
            {
                ThreadExceptionHandler.logger.Debug(ex.Message);
                return false;
            }
        }
        #endregion

        #region Преобразование списка из xsd
        /// <summary>
        ///  Разбор xsd файла
        /// </summary>
        /// <param name="path">Путь к файлу</param>
        /// <returns> список ключ значение</returns>
        public List<KeyValuePair<string, string>> ReadXsd(string path)
        {
            try
            {
                XmlReader reader = XmlReader.Create(path);
                XDocument doc = XDocument.Load(reader);
                XmlNamespaceManager ns = new XmlNamespaceManager(reader.NameTable);
                ns.AddNamespace("", "http://tempuri.org/XMLSchema.xsd");
                XNamespace xs = "http://www.w3.org/2001/XMLSchema";
                try
                {
                    var items = new List<KeyValuePair<string, string>>();
                    foreach (var element in doc.Descendants(xs + "enumeration"))
                    {
                        items.Add(new KeyValuePair<string, string>(element.Attribute("value").Value, element.Value));
                    }
                    return items;
                }
                finally
                {
                    reader.Close();
                    doc = null;
                }
            }
            catch
            {
                return null;
            }
        }
        #endregion
    }

    #region КЛАСС EXCEPTIOnN

    internal class ThreadExceptionHandler
    {
        public static Logger logger = LogManager.GetCurrentClassLogger();
        public static void ShowEventDialog(string Message, Exception ex)
        {
            string errorMessage = Message + "\n" +
              "Unhandled Exception:\n" +
               ex.Message + "\n" +
               ex.GetType();

            logger.Error(errorMessage + "\nStack Trace:\n" + ex.StackTrace);

            MessageBox.Show(errorMessage,
            "Application Error",
            MessageBoxButtons.OK,
            MessageBoxIcon.Error);
        }

        /// 
        /// Handles the thread exception.
        /// 
        public  void Application_ThreadException(
            object sender, ThreadExceptionEventArgs e)
        {
            logger.Debug(e.ToString() + " " + e.Exception.Message.ToString());
            try
            {
                // Exit the program if the user clicks Abort.
                DialogResult result = ShowThreadExceptionDialog(
                    e.Exception);

                if (result == DialogResult.Abort)
                    Application.Exit();
            }
            catch
            {
                // Fatal error, terminate program
                try
                {
                    MessageBox.Show("Fatal Error",
                        "Fatal Error",
                        MessageBoxButtons.OK,
                        MessageBoxIcon.Stop);
                }
                finally
                {
                    Application.Exit();
                }
            }
        }

        /// 
        /// Creates and displays the error message.
        /// 
        private  DialogResult ShowThreadExceptionDialog(Exception ex)
        {
            string errorMessage =
                "Unhandled Exception:\n\n" +
                ex.Message + "\n\n" +
                ex.GetType() +
                "\n\nStack Trace:\n" +
                ex.StackTrace;

            return MessageBox.Show(errorMessage,
                "Application Error",
                MessageBoxButtons.AbortRetryIgnore,
                MessageBoxIcon.Stop);
        }

     

    } // End ThreadExceptionHandler

    #endregion


    public class UpperCaseUTF8Encoding : UTF8Encoding
    {
        // Code from a blog http://www.distribucon.com/blog/CategoryView,category,XML.aspx
        //
        // Dan Miser - Thoughts from Dan Miser
        // Tuesday, January 29, 2008 
        // He used the Reflector to understand the heirarchy of the encoding class
        //
        //      Back to Reflector, and I notice that the Encoding.WebName is the property used to
        //      write out the encoding string. I now create a descendant class of UTF8Encoding.
        //      The class is listed below. Now I just call XmlTextWriter, passing in
        //      UpperCaseUTF8Encoding.UpperCaseUTF8 for the Encoding type, and everything works
        //      perfectly. - Dan Miser

        public override string WebName
        {
            get { return base.WebName.ToUpper(); }
        }

        public static UpperCaseUTF8Encoding UpperCaseUTF8
        {
            get
            {
                if (upperCaseUtf8Encoding == null)
                {
                    upperCaseUtf8Encoding = new UpperCaseUTF8Encoding();
                }
                return upperCaseUtf8Encoding;
            }
        }

        private static UpperCaseUTF8Encoding upperCaseUtf8Encoding = null;
    }
}
