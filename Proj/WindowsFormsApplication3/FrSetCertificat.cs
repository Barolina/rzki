﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Xml;
using System.Xml.Serialization;


namespace PortalRosreestr
{
    public partial class FrSetCertificat : Form
    {
        //Запись настроек
        private void writeSetting()
        {
            try
            {
                SettHelper.props.CertFields.EmailPerson = txtEmailPerson.Text;
                SettHelper.props.CertFields.FIO = txtFIO.Text;
                SettHelper.props.CertFields.Name = txtName.Text;
                SettHelper.props.CertFields.SurName = txtSurName.Text;
                SettHelper.props.CertFields.Snils = txtSnils.Text;
                SettHelper.props.CertFields.DateDOC =  dtP.Value.ToString("yyyy-MM-dd");
                SettHelper.props.CertFields.Seria = txtSeria.Text;
                SettHelper.props.CertFields.Number = txtNumber.Text;
                SettHelper.props.CertFields.Issuque = txtIssque.Text;
                SettHelper.props.CertFields.Addres = txtAddress.Text;
                SettHelper.props.CertFields.Telefon = txtTel.Text;
                SettHelper.props.CertFields.Region = this.cbbRegion.SelectedValue.ToString();
                SettHelper.props.WriteBD();

                SettHelper.props.DeclarentFields.zFIO = textBoxzFIO.Text;
                SettHelper.props.DeclarentFields.zName = textBoxzName.Text;
                SettHelper.props.DeclarentFields.zDATEDoc = dateTimePickerzDate.Value.ToString("yyyy-MM-dd");
                SettHelper.props.DeclarentFields.zNumber = maskedTextBoxzNumber.Text;
                SettHelper.props.DeclarentFields.zEmail = textBoxzEmail.Text;
                SettHelper.props.WrireDeclarentXMl();
            }
            catch (Exception e)
            {
                MessageBox.Show(e.Message.ToString());
            }
        }


        //Чтение настроек
        public void readSetting()
        {
            try
            {
                SettHelper.props.ReadBD();
                txtEmailPerson.Text = SettHelper.props.CertFields.EmailPerson;
                txtFIO.Text = SettHelper.props.CertFields.FIO;
                txtName.Text = SettHelper.props.CertFields.Name;
                txtSurName.Text = SettHelper.props.CertFields.SurName;
                txtSnils.Text = SettHelper.props.CertFields.Snils;
                txtSeria.Text = SettHelper.props.CertFields.Seria;

                string dt = SettHelper.props.CertFields.DateDOC.ToString();
                if (dt == "") dtP.Value = DateTime.Today;
                else dtP.Value = Convert.ToDateTime(SettHelper.props.CertFields.DateDOC);
                
                //DateOut.Text = SettHelper.props.CertFields.DateDOC;
                //DateOut.Update();
                txtNumber.Text = SettHelper.props.CertFields.Number;
                txtIssque.Text = SettHelper.props.CertFields.Issuque;
                txtAddress.Text = SettHelper.props.CertFields.Addres;
                txtTel.Text = SettHelper.props.CertFields.Telefon;
                if (SettHelper.props.CertFields.Region == "") cbbRegion.SelectedValue = 1;
                else
                cbbRegion.SelectedValue = SettHelper.props.CertFields.Region;

                SettHelper.props.ReadDeclarentXml();
                textBoxzFIO.Text = SettHelper.props.DeclarentFields.zFIO;
                textBoxzName.Text = SettHelper.props.DeclarentFields.zName;
                maskedTextBoxzNumber.Text = SettHelper.props.DeclarentFields.zNumber;
                textBoxzEmail.Text = SettHelper.props.DeclarentFields.zEmail;

                string zdt = SettHelper.props.DeclarentFields.zDATEDoc.ToString();
                if (zdt == "") dateTimePickerzDate.Value = DateTime.Today;
                else dateTimePickerzDate.Value = Convert.ToDateTime(SettHelper.props.DeclarentFields.zDATEDoc);

            }
            catch (Exception e)
            {
                MessageBox.Show(e.Message.ToString());
            }
        }

        public FrSetCertificat()
        {
            InitializeComponent();
          
        }

      
        private void FrSetCertificat_Shown(object sender, EventArgs e)
        {
            var items = FrPortalRosreestr.portalRosreestr.ReadXsd(Application.StartupPath + @"\\DATA\\adresCod.xsd");
            cbbRegion.Sorted = false;
            cbbRegion.DataSource = items;
            cbbRegion.ValueMember = "Key";
            cbbRegion.DisplayMember = "Value";
            cbbRegion.SelectedValue = SettHelper.props.CertFields.Region.Trim();
            
            readSetting(); 
        }

        private void btnOk_Click(object sender, EventArgs e)
        {
            writeSetting();
          //  SettHelper.props.WriteBD();
          //  SettHelper.props.ReadBD();
        }

        private void chkShow_CheckedChanged(object sender, EventArgs e)
        {
            SettHelper.props.writeSetting();
        }

        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {

        }

        private void linkLabel1_Click(object sender, EventArgs e)
        {
            FrPortalRosreestr.portalRosreestr.SetCertificat();
            readSetting();
        }
    }
}
