CRYPT_SIGN_MESSAGE_PARA SigParams;
SigParams.cbSize = sizeof(SigParams);
SigParams.dwMsgEncodingType = PKCS_7_ASN_ENCODING | X509_ASN_ENCODING;
SigParams.pSigningCert = pCert;
SigParams.HashAlgorithm.pszObjId = "1.2.643.2.2.9";
SigParams.HashAlgorithm.Parameters.cbData = NULL;
SigParams.cMsgCert = 1;
SigParams.rgpMsgCert = &pCert;
SigParams.cAuthAttr = 0;
SigParams.dwInnerContentType = 0;
SigParams.cMsgCrl = 0;
SigParams.cUnauthAttr = 0;
SigParams.dwFlags = 0;
SigParams.pvHashAuxInfo = NULL;
SigParams.rgAuthAttr = NULL;

cbSignedMessageBlob = 0;
if (!(CryptSignMessage(&SigParams,
	FALSE,
	1,
	&pbBuffer,
	&dwBufferLen,
	NULL,
	&cbSignedMessageBlob)))
{
	HandleError("Error during CryptSignMessage.");
}

if (!(pbSignedMessageBlob = (BYTE*)malloc(cbSignedMessageBlob))) {
	HandleError("Error during CryptSignMessage.");
}

if (!(CryptSignMessage(&SigParams,
	FALSE,
	1,
	&pbBuffer,
	&dwBufferLen,
	pbSignedMessageBlob,
	&cbSignedMessageBlob)))
{
	HandleError("Error during CryptSignMessage.");
}