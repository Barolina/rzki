// ************************************************************************ //
// The types declared in this file were generated from data read from the
// WSDL File described below:
// WSDL     : https://portal.rosreestr.ru:4433/cxf/External?wsdl
//  >Import : https://portal.rosreestr.ru:4433/cxf/External?wsdl>0
// Encoding : UTF-8
// Version  : 1.0
// (29.01.2015 15:19:23 - - $Rev: 69934 $)
// ************************************************************************ //

unit External;

interface

uses Soap.InvokeRegistry, Soap.SOAPHTTPClient, System.Types, Soap.XSBuiltIns;

const
  IS_OPTN = $0001;
  IS_UNBD = $0002;
  IS_UNQL = $0008;


type

  // ************************************************************************ //
  // The following types, referred to in the WSDL document are not being represented
  // in this file. They are either aliases[@] of other types represented or were referred
  // to but never[!] declared in the document. The types from the latter category
  // typically map to predefined/known XML or Embarcadero types; however, they could also 
  // indicate incorrect WSDL documents that failed to declare or import a schema type.
  // ************************************************************************ //
  // !:dateTime        - "http://www.w3.org/2001/XMLSchema"[Gbl]
  // !:boolean         - "http://www.w3.org/2001/XMLSchema"[Gbl]
  // !:base64Binary    - "http://www.w3.org/2001/XMLSchema"[Gbl]
  // !:string          - "http://www.w3.org/2001/XMLSchema"[Gbl]

  createRequestIn      = class;                 { "urn:ws.request.pgu.sids.fccland.ru"[Lit][GblCplx] }
  createRequest        = class;                 { "urn:ws.request.pgu.sids.fccland.ru"[Lit][GblElm] }
  operationStatus      = class;                 { "urn:ws.request.pgu.sids.fccland.ru"[GblCplx] }
  createRequestOut     = class;                 { "urn:ws.request.pgu.sids.fccland.ru"[Lit][GblCplx] }
  createRequestResponse = class;                { "urn:ws.request.pgu.sids.fccland.ru"[Lit][GblElm] }
  loadEventDetailsOut  = class;                 { "urn:ws.request.pgu.sids.fccland.ru"[Lit][GblCplx] }
  loadEventDetailsResponse = class;             { "urn:ws.request.pgu.sids.fccland.ru"[Lit][GblElm] }
  getEventsIn          = class;                 { "urn:ws.request.pgu.sids.fccland.ru"[Lit][GblCplx] }
  getEvents            = class;                 { "urn:ws.request.pgu.sids.fccland.ru"[Lit][GblElm] }
  deleteEventsOut      = class;                 { "urn:ws.request.pgu.sids.fccland.ru"[Lit][GblCplx] }
  deleteEventsResponse = class;                 { "urn:ws.request.pgu.sids.fccland.ru"[Lit][GblElm] }
  loadEventDetailsIn   = class;                 { "urn:ws.request.pgu.sids.fccland.ru"[Lit][GblCplx] }
  loadEventDetails     = class;                 { "urn:ws.request.pgu.sids.fccland.ru"[Lit][GblElm] }
  getEventsOut         = class;                 { "urn:ws.request.pgu.sids.fccland.ru"[Lit][GblCplx] }
  getEventsResponse    = class;                 { "urn:ws.request.pgu.sids.fccland.ru"[Lit][GblElm] }
  eventStruct          = class;                 { "urn:ws.request.pgu.sids.fccland.ru"[GblCplx] }

  Array_Of_eventStruct = array of eventStruct;   { "urn:ws.request.pgu.sids.fccland.ru"[GblUbnd] }


  // ************************************************************************ //
  // XML       : createRequestIn, global, <complexType>
  // Namespace : urn:ws.request.pgu.sids.fccland.ru
  // Serializtn: [xoLiteralParam]
  // Info      : Wrapper
  // ************************************************************************ //
  createRequestIn = class(TRemotable)
  private
    Fregion: string;
    Fokato: string;
    Foktmo: string;
    FrequestData: TByteDynArray;
    FrequestType: string;
  public
    constructor Create; override;
  published
    property region:      string         Index (IS_UNQL) read Fregion write Fregion;
    property okato:       string         Index (IS_UNQL) read Fokato write Fokato;
    property oktmo:       string         Index (IS_UNQL) read Foktmo write Foktmo;
    property requestData: TByteDynArray  Index (IS_UNQL) read FrequestData write FrequestData;
    property requestType: string         Index (IS_UNQL) read FrequestType write FrequestType;
  end;



  // ************************************************************************ //
  // XML       : createRequest, global, <element>
  // Namespace : urn:ws.request.pgu.sids.fccland.ru
  // Info      : Wrapper
  // ************************************************************************ //
  createRequest = class(createRequestIn)
  private
  published
  end;



  // ************************************************************************ //
  // XML       : operationStatus, global, <complexType>
  // Namespace : urn:ws.request.pgu.sids.fccland.ru
  // ************************************************************************ //
  operationStatus = class(TRemotable)
  private
    Fresult: Boolean;
    Fmessage_: string;
    Fmessage__Specified: boolean;
    procedure Setmessage_(Index: Integer; const Astring: string);
    function  message__Specified(Index: Integer): boolean;
  published
    property result:   Boolean  Index (IS_UNQL) read Fresult write Fresult;
    property message_: string   Index (IS_OPTN or IS_UNQL) read Fmessage_ write Setmessage_ stored message__Specified;
  end;



  // ************************************************************************ //
  // XML       : createRequestOut, global, <complexType>
  // Namespace : urn:ws.request.pgu.sids.fccland.ru
  // Serializtn: [xoLiteralParam]
  // Info      : Wrapper
  // ************************************************************************ //
  createRequestOut = class(TRemotable)
  private
    FrequestNumber: string;
    Fstatus: operationStatus;
  public
    constructor Create; override;
    destructor Destroy; override;
  published
    property requestNumber: string           Index (IS_UNQL) read FrequestNumber write FrequestNumber;
    property status:        operationStatus  Index (IS_UNQL) read Fstatus write Fstatus;
  end;



  // ************************************************************************ //
  // XML       : createRequestResponse, global, <element>
  // Namespace : urn:ws.request.pgu.sids.fccland.ru
  // Info      : Wrapper
  // ************************************************************************ //
  createRequestResponse = class(createRequestOut)
  private
  published
  end;



  // ************************************************************************ //
  // XML       : loadEventDetailsOut, global, <complexType>
  // Namespace : urn:ws.request.pgu.sids.fccland.ru
  // Serializtn: [xoLiteralParam]
  // Info      : Wrapper
  // ************************************************************************ //
  loadEventDetailsOut = class(TRemotable)
  private
    FdetailsXML: string;
    Fbinary: TByteDynArray;
    Fbinary_Specified: boolean;
    Fstatus: operationStatus;
    procedure Setbinary(Index: Integer; const ATByteDynArray: TByteDynArray);
    function  binary_Specified(Index: Integer): boolean;
  public
    constructor Create; override;
    destructor Destroy; override;
  published
    property detailsXML: string           Index (IS_UNQL) read FdetailsXML write FdetailsXML;
    property binary:     TByteDynArray    Index (IS_OPTN or IS_UNQL) read Fbinary write Setbinary stored binary_Specified;
    property status:     operationStatus  Index (IS_UNQL) read Fstatus write Fstatus;
  end;



  // ************************************************************************ //
  // XML       : loadEventDetailsResponse, global, <element>
  // Namespace : urn:ws.request.pgu.sids.fccland.ru
  // Info      : Wrapper
  // ************************************************************************ //
  loadEventDetailsResponse = class(loadEventDetailsOut)
  private
  published
  end;



  // ************************************************************************ //
  // XML       : getEventsIn, global, <complexType>
  // Namespace : urn:ws.request.pgu.sids.fccland.ru
  // Serializtn: [xoLiteralParam]
  // Info      : Wrapper
  // ************************************************************************ //
  getEventsIn = class(TRemotable)
  private
    FlastEventID: string;
  public
    constructor Create; override;
  published
    property lastEventID: string  Index (IS_UNQL) read FlastEventID write FlastEventID;
  end;



  // ************************************************************************ //
  // XML       : getEvents, global, <element>
  // Namespace : urn:ws.request.pgu.sids.fccland.ru
  // Info      : Wrapper
  // ************************************************************************ //
  getEvents = class(getEventsIn)
  private
  published
  end;



  // ************************************************************************ //
  // XML       : deleteEventsOut, global, <complexType>
  // Namespace : urn:ws.request.pgu.sids.fccland.ru
  // Serializtn: [xoLiteralParam]
  // Info      : Wrapper
  // ************************************************************************ //
  deleteEventsOut = class(TRemotable)
  private
    Fstatus: operationStatus;
  public
    constructor Create; override;
    destructor Destroy; override;
  published
    property status: operationStatus  Index (IS_UNQL) read Fstatus write Fstatus;
  end;



  // ************************************************************************ //
  // XML       : deleteEventsResponse, global, <element>
  // Namespace : urn:ws.request.pgu.sids.fccland.ru
  // Info      : Wrapper
  // ************************************************************************ //
  deleteEventsResponse = class(deleteEventsOut)
  private
  published
  end;



  // ************************************************************************ //
  // XML       : loadEventDetailsIn, global, <complexType>
  // Namespace : urn:ws.request.pgu.sids.fccland.ru
  // Serializtn: [xoLiteralParam]
  // Info      : Wrapper
  // ************************************************************************ //
  loadEventDetailsIn = class(TRemotable)
  private
    FeventID: string;
  public
    constructor Create; override;
  published
    property eventID: string  Index (IS_UNQL) read FeventID write FeventID;
  end;



  // ************************************************************************ //
  // XML       : loadEventDetails, global, <element>
  // Namespace : urn:ws.request.pgu.sids.fccland.ru
  // Info      : Wrapper
  // ************************************************************************ //
  loadEventDetails = class(loadEventDetailsIn)
  private
  published
  end;

  deleteEventsIn = array of string;             { "urn:ws.request.pgu.sids.fccland.ru"[Lit][GblCplx] }
  deleteEvents    =  type deleteEventsIn;      { "urn:ws.request.pgu.sids.fccland.ru"[Lit][GblElm] }


  // ************************************************************************ //
  // XML       : getEventsOut, global, <complexType>
  // Namespace : urn:ws.request.pgu.sids.fccland.ru
  // Serializtn: [xoLiteralParam]
  // Info      : Wrapper
  // ************************************************************************ //
  getEventsOut = class(TRemotable)
  private
    Fevents: Array_Of_eventStruct;
    Fevents_Specified: boolean;
    Fstatus: operationStatus;
    procedure Setevents(Index: Integer; const AArray_Of_eventStruct: Array_Of_eventStruct);
    function  events_Specified(Index: Integer): boolean;
  public
    constructor Create; override;
    destructor Destroy; override;
  published
    property events: Array_Of_eventStruct  Index (IS_OPTN or IS_UNBD or IS_UNQL) read Fevents write Setevents stored events_Specified;
    property status: operationStatus       Index (IS_UNQL) read Fstatus write Fstatus;
  end;



  // ************************************************************************ //
  // XML       : getEventsResponse, global, <element>
  // Namespace : urn:ws.request.pgu.sids.fccland.ru
  // Info      : Wrapper
  // ************************************************************************ //
  getEventsResponse = class(getEventsOut)
  private
  published
  end;



  // ************************************************************************ //
  // XML       : eventStruct, global, <complexType>
  // Namespace : urn:ws.request.pgu.sids.fccland.ru
  // ************************************************************************ //
  eventStruct = class(TRemotable)
  private
    FeventID: string;
    FeventType: string;
    FeventDate: TXSDateTime;
    FrequestNumber: string;
  public
    destructor Destroy; override;
  published
    property eventID:       string       Index (IS_UNQL) read FeventID write FeventID;
    property eventType:     string       Index (IS_UNQL) read FeventType write FeventType;
    property eventDate:     TXSDateTime  Index (IS_UNQL) read FeventDate write FeventDate;
    property requestNumber: string       Index (IS_UNQL) read FrequestNumber write FrequestNumber;
  end;


  // ************************************************************************ //
  // Namespace : urn:ws.request.pgu.sids.fccland.ru
  // soapAction: urn:ws.request.pgu.sids.fccland.ru/%operationName%
  // transport : http://schemas.xmlsoap.org/soap/http
  // style     : document
  // use       : literal
  // binding   : soap1.1Binding
  // service   : ExternalWS
  // port      : ExternalWSSoap1.1Port
  // URL       : http://10.129.224.45:8080/cxf/External
  // ************************************************************************ //
  ExternalSEI = interface(IInvokable)
  ['{46B06FBB-C4AC-6371-C162-A86266D54FEF}']

    // Cannot unwrap: 
    //     - More than one strictly out element was found
    function  createRequest(const params: createRequest): createRequestResponse; stdcall;

    // Cannot unwrap: 
    //     - More than one strictly out element was found
    function  getEvents(const params: getEvents): getEventsResponse; stdcall;
    function  deleteEvents(const params: deleteEvents): deleteEventsResponse; stdcall;

    // Cannot unwrap: 
    //     - More than one strictly out element was found
    function  loadEventDetails(const params: loadEventDetails): loadEventDetailsResponse; stdcall;
  end;

function GetExternalSEI(UseWSDL: Boolean=System.False; Addr: string=''; HTTPRIO: THTTPRIO = nil): ExternalSEI;


implementation
  uses System.SysUtils;

function GetExternalSEI(UseWSDL: Boolean; Addr: string; HTTPRIO: THTTPRIO): ExternalSEI;
const
  defWSDL = 'https://portal.rosreestr.ru:4433/cxf/External?wsdl';
  defURL  = 'https://portal.rosreestr.ru:4433/cxf/External';
  defSvc  = 'ExternalWS';
  defPrt  = 'ExternalWSSoap1.1Port';
var
  RIO: THTTPRIO;
begin
  Result := nil;
  if (Addr = '') then
  begin
    if UseWSDL then
      Addr := defWSDL
    else
      Addr := defURL;
  end;
  if HTTPRIO = nil then
    RIO := THTTPRIO.Create(nil)
  else
    RIO := HTTPRIO;
  try
    Result := (RIO as ExternalSEI);
    if UseWSDL then
    begin
      RIO.WSDLLocation := Addr;
      RIO.Service := defSvc;
      RIO.Port := defPrt;
    end else
      RIO.URL := Addr;
  finally
    if (Result = nil) and (HTTPRIO = nil) then
      RIO.Free;
  end;
end;


constructor createRequestIn.Create;
begin
  inherited Create;
  FSerializationOptions := [xoLiteralParam];
end;

procedure operationStatus.Setmessage_(Index: Integer; const Astring: string);
begin
  Fmessage_ := Astring;
  Fmessage__Specified := True;
end;

function operationStatus.message__Specified(Index: Integer): boolean;
begin
  Result := Fmessage__Specified;
end;

constructor createRequestOut.Create;
begin
  inherited Create;
  FSerializationOptions := [xoLiteralParam];
end;

destructor createRequestOut.Destroy;
begin
  System.SysUtils.FreeAndNil(Fstatus);
  inherited Destroy;
end;

constructor loadEventDetailsOut.Create;
begin
  inherited Create;
  FSerializationOptions := [xoLiteralParam];
end;

destructor loadEventDetailsOut.Destroy;
begin
  System.SysUtils.FreeAndNil(Fstatus);
  inherited Destroy;
end;

procedure loadEventDetailsOut.Setbinary(Index: Integer; const ATByteDynArray: TByteDynArray);
begin
  Fbinary := ATByteDynArray;
  Fbinary_Specified := True;
end;

function loadEventDetailsOut.binary_Specified(Index: Integer): boolean;
begin
  Result := Fbinary_Specified;
end;

constructor getEventsIn.Create;
begin
  inherited Create;
  FSerializationOptions := [xoLiteralParam];
end;

constructor deleteEventsOut.Create;
begin
  inherited Create;
  FSerializationOptions := [xoLiteralParam];
end;

destructor deleteEventsOut.Destroy;
begin
  System.SysUtils.FreeAndNil(Fstatus);
  inherited Destroy;
end;

constructor loadEventDetailsIn.Create;
begin
  inherited Create;
  FSerializationOptions := [xoLiteralParam];
end;

constructor getEventsOut.Create;
begin
  inherited Create;
  FSerializationOptions := [xoLiteralParam];
end;

destructor getEventsOut.Destroy;
var
  I: Integer;
begin
  for I := 0 to System.Length(Fevents)-1 do
    System.SysUtils.FreeAndNil(Fevents[I]);
  System.SetLength(Fevents, 0);
  System.SysUtils.FreeAndNil(Fstatus);
  inherited Destroy;
end;

procedure getEventsOut.Setevents(Index: Integer; const AArray_Of_eventStruct: Array_Of_eventStruct);
begin
  Fevents := AArray_Of_eventStruct;
  Fevents_Specified := True;
end;

function getEventsOut.events_Specified(Index: Integer): boolean;
begin
  Result := Fevents_Specified;
end;

destructor eventStruct.Destroy;
begin
  System.SysUtils.FreeAndNil(FeventDate);
  inherited Destroy;
end;

initialization
  { ExternalSEI }
  InvRegistry.RegisterInterface(TypeInfo(ExternalSEI), 'urn:ws.request.pgu.sids.fccland.ru', 'UTF-8');
  InvRegistry.RegisterDefaultSOAPAction(TypeInfo(ExternalSEI), 'urn:ws.request.pgu.sids.fccland.ru/%operationName%');
  InvRegistry.RegisterInvokeOptions(TypeInfo(ExternalSEI), ioDocument);
  InvRegistry.RegisterInvokeOptions(TypeInfo(ExternalSEI), ioLiteral);
  RemClassRegistry.RegisterXSInfo(TypeInfo(Array_Of_eventStruct), 'urn:ws.request.pgu.sids.fccland.ru', 'Array_Of_eventStruct');
  RemClassRegistry.RegisterXSClass(createRequestIn, 'urn:ws.request.pgu.sids.fccland.ru', 'createRequestIn');
  RemClassRegistry.RegisterSerializeOptions(createRequestIn, [xoLiteralParam]);
  RemClassRegistry.RegisterXSClass(createRequest, 'urn:ws.request.pgu.sids.fccland.ru', 'createRequest');
  RemClassRegistry.RegisterXSClass(operationStatus, 'urn:ws.request.pgu.sids.fccland.ru', 'operationStatus');
  RemClassRegistry.RegisterExternalPropName(TypeInfo(operationStatus), 'message_', '[ExtName="message"]');
  RemClassRegistry.RegisterXSClass(createRequestOut, 'urn:ws.request.pgu.sids.fccland.ru', 'createRequestOut');
  RemClassRegistry.RegisterSerializeOptions(createRequestOut, [xoLiteralParam]);
  RemClassRegistry.RegisterXSClass(createRequestResponse, 'urn:ws.request.pgu.sids.fccland.ru', 'createRequestResponse');
  RemClassRegistry.RegisterXSClass(loadEventDetailsOut, 'urn:ws.request.pgu.sids.fccland.ru', 'loadEventDetailsOut');
  RemClassRegistry.RegisterSerializeOptions(loadEventDetailsOut, [xoLiteralParam]);
  RemClassRegistry.RegisterXSClass(loadEventDetailsResponse, 'urn:ws.request.pgu.sids.fccland.ru', 'loadEventDetailsResponse');
  RemClassRegistry.RegisterXSClass(getEventsIn, 'urn:ws.request.pgu.sids.fccland.ru', 'getEventsIn');
  RemClassRegistry.RegisterSerializeOptions(getEventsIn, [xoLiteralParam]);
  RemClassRegistry.RegisterXSClass(getEvents, 'urn:ws.request.pgu.sids.fccland.ru', 'getEvents');
  RemClassRegistry.RegisterXSClass(deleteEventsOut, 'urn:ws.request.pgu.sids.fccland.ru', 'deleteEventsOut');
  RemClassRegistry.RegisterSerializeOptions(deleteEventsOut, [xoLiteralParam]);
  RemClassRegistry.RegisterXSClass(deleteEventsResponse, 'urn:ws.request.pgu.sids.fccland.ru', 'deleteEventsResponse');
  RemClassRegistry.RegisterXSClass(loadEventDetailsIn, 'urn:ws.request.pgu.sids.fccland.ru', 'loadEventDetailsIn');
  RemClassRegistry.RegisterSerializeOptions(loadEventDetailsIn, [xoLiteralParam]);
  RemClassRegistry.RegisterXSClass(loadEventDetails, 'urn:ws.request.pgu.sids.fccland.ru', 'loadEventDetails');
  RemClassRegistry.RegisterXSInfo(TypeInfo(deleteEventsIn), 'urn:ws.request.pgu.sids.fccland.ru', 'deleteEventsIn');
  RemClassRegistry.RegisterSerializeOptions(TypeInfo(deleteEventsIn), [xoLiteralParam]);
  RemClassRegistry.RegisterXSInfo(TypeInfo(deleteEvents), 'urn:ws.request.pgu.sids.fccland.ru', 'deleteEvents');
  RemClassRegistry.RegisterXSClass(getEventsOut, 'urn:ws.request.pgu.sids.fccland.ru', 'getEventsOut');
  RemClassRegistry.RegisterSerializeOptions(getEventsOut, [xoLiteralParam]);
  RemClassRegistry.RegisterXSClass(getEventsResponse, 'urn:ws.request.pgu.sids.fccland.ru', 'getEventsResponse');
  RemClassRegistry.RegisterXSClass(eventStruct, 'urn:ws.request.pgu.sids.fccland.ru', 'eventStruct');

end.