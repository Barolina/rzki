object Form1: TForm1
  Left = 0
  Top = 0
  Caption = 'Form1'
  ClientHeight = 357
  ClientWidth = 327
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  PixelsPerInch = 96
  TextHeight = 13
  object Memo1: TMemo
    Left = 24
    Top = 8
    Width = 201
    Height = 324
    Lines.Strings = (
      'Memo1')
    TabOrder = 0
  end
  object btnStart: TButton
    Left = 231
    Top = 307
    Width = 75
    Height = 25
    Caption = 'btnStart'
    TabOrder = 1
    OnClick = btnStartClick
  end
  object HTTPRIO1: THTTPRIO
    WSDLLocation = 'https://portal.rosreestr.ru:4433/cxf/External?wsdl'
    Service = 'ExternalWS'
    HTTPWebNode.UseUTF8InHeader = True
    HTTPWebNode.InvokeOptions = [soIgnoreInvalidCerts, soPickFirstClientCertificate]
    HTTPWebNode.WebNodeOptions = []
    Converter.Options = [soSendMultiRefObj, soTryAllSchema, soRootRefNodesToBody, soCacheMimeResponse, soUTF8EncodeXML]
    Left = 264
    Top = 224
  end
end
